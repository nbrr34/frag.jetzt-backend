FROM maven:3 AS DEV

ARG USER='1000:1000'
ARG CACHEDIR=/cache
ARG APPDIR=/app

RUN mkdir -p ${CACHEDIR} ${APPDIR} && chown -R ${USER} ${CACHEDIR} ${APPDIR}

USER ${USER}
ENV HOME=${CACHEDIR}

VOLUME ${CACHEDIR}
VOLUME ${APPDIR}
WORKDIR ${APPDIR}

ENTRYPOINT ["mvn"]
CMD ["-Dmaven.repo.local=/cache", "spring-boot:run"]

FROM maven:3.6-jdk-8-slim AS BUILD

ENV MAVEN_OPTS="-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Dorg.slf4j.simpleLogger.dateTimeFormat=HH:mm:ss.SSS -Djava.awt.headless=true"
ENV MAVEN_CLI_OPTS="--batch-mode --fail-at-end --show-version"

WORKDIR /app
COPY . .

RUN mvn clean
RUN mvn $MAVEN_CLI_OPTS test-compile
RUN mvn $MAVEN_CLI_OPTS jacoco:prepare-agent surefire:test jacoco:report
RUN mvn $MAVEN_CLI_OPTS package


FROM openjdk:8-jdk-alpine AS SERVE
COPY --from=BUILD /app/target/frag.jetzt-backend-*.jar app.jar
EXPOSE 8888
ENTRYPOINT ["java","-jar","/app.jar"]
