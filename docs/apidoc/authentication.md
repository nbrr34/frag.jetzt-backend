# Authentication Routes


## Check login status and Refresh JSON Web Token
```http
POST /auth/login
```

**Request params**:

| Field   | Type    | Description                    |
|---------|---------|--------------------------------|
| refresh | boolean | Should the token be refreshed? |

**Success 200**:
The result is an array of user objects with the following attributes:

| Field     | Type      | Description     |
|-----------|-----------|-----------------|
| id        |      UUID | ID of the user  |
| email     |    String | Email           |
| lastLogin | Timestamp | last login time |


**Error 4xx**:

| Field        | Description                          |
|--------------|--------------------------------------|
| Unauthorized | Access denied when no JWT is present |


---
## Login with an registered non guest user
```http
POST /auth/login/registered
```

**Request body**:

| Field     | Type   | Description                    |
|-----------|--------|--------------------------------|
| loginId   | UUID   | email of the requested user    |
| password  | String | password of the requested user |

**Success 200**:

| Field         | Type                      | Description           |
|---------------|---------------------------|-----------------------|
| accountId     | UUID                      | ID of the user        |
| credentials   | String                    | ID of the user        |
| principal     | String                    | ID of the user        |
| type          | String                    | guest or registered   |
| roomAccesses  | Array of RoomAccess       | list of access rights |
| authenticated | boolean                   | is user logged in     |
| authorities   | Array of authorities      | not used              |
| details       | String                    | JSON Web Token        |
| name          | String                    | email of user         |



**Error 4xx**:

| Field        | Description                                      |
|--------------|--------------------------------------------------|
| Unauthorized | passwords doesn´t match or account was not found |
| Forbidden    | User not activated                               |


---
## Create a new guest account
```http
POST /auth/login/guest
```

**Success 200**:

| Field         | Type                      | Description           |
|---------------|---------------------------|-----------------------|
| accountId     | UUID                      | ID of the user        |
| credentials   | String                    | ID of the user        |
| principal     | String                    | ID of the user        |
| type          | String                    | guest or registered   |
| roomAccesses  | Array of RoomAccess       | list of access rights |
| authenticated | boolean                   | is user logged in     |
| authorities   | Array of authorities      | not used              |
| details       | String                    | JSON Web Token        |
| name          | String                    | email of user         |
