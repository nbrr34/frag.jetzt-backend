# User Routes


## Get multiple User
```http
GET /user?ids=[]
```

**Needed rights:** valid JWT for authorization

**Request params**:

| Field    | Type          | Description          |
|----------|---------------|----------------------|
| ids      | Array of UUID | List of IDs to query |

**Success 200**:
The result is an array of user objects with the following attributes:

| Field     | Type      | Description     |
|-----------|-----------|-----------------|
| id        |      UUID | ID of the user  |
| email     |    String | Email           |
| lastLogin | Timestamp | last login time |


---
## Get User by ID
```http
GET /user/{id}
```

**Needed rights:** valid JWT for authorization

**Path variables**:

| Field    | Type   | Description                |
|----------|--------|----------------------------|
| id       | UUID   | ID of the specific user    |

**Success 200**:

| Field     | Type      | Description     |
|-----------|-----------|-----------------|
| id        |      UUID | ID of the user  |
| email     |    String | Email           |
| lastLogin | Timestamp | last login time |


---
## Register a new User
```http
POST /user/register
```

**Request body**:

| Field    | Type   | Description                   |
|----------|--------|-------------------------------|
| loginid  | String | Email address of the new user |
| password | String | Password of the new user      |

**Success 200**:
Empty Response

**Error 4xx**:

| Field     | Description         |
|-----------|---------------------|
| Forbidden | User already exists |


---
## Activate newly created user
```http
POST /user/~{alias}/activate?key=key
```

**Request params**:

| Field    | Type   | Description                        |
|----------|--------|------------------------------------|
| alias    | String | Email address of the specific user |
| key      | String | Activationkey for verification     |

**Success 200**:
Empty Response

**Error 4xx**:

| Field     | Description          |
|-----------|----------------------|
| Forbidden | Wrong activation key |


---
## Reset activation and receive a new key by mail
```http
POST /user/~{alias}/resetactivation
```
**Path variables**:

| Field    | Type   | Description                |
|----------|--------|----------------------------|
| alias    | UUID   | Email of the specific user |

**Success 200**:
Empty Response


---
## Initiate and execute password reset
```http
POST /user/{id}/resetpassword
```
These Route combines two different functions. If a key for the passwortreset is provided and is correct, the new password will be set. If not, a new password reset key will be send by mail.

**Path variables**:

| Field | Type   | Description             |
|-------|--------|-------------------------|
| id    | UUID   | ID of the specific user |

**Optional request body**:

| Field    | Type   | Description                        |
|----------|--------|------------------------------------|
| key      | String | Passwordresetkey for verification  |
| password | String | The new password                   |

**Success 200**:
Empty Response

**Error 4xx**:

| Field     | Description          |
|-----------|----------------------|
| Forbidden | Wrong key |
| BadRequest | If a key is provided, but no password is set. If no key is provided, there is already a reset key |


---
## Get the accessrights of a User
```http
GET /user/{id}/roomAccess
```

**Needed rights:** valid JWT for authorization

**Path variables**:

| Field | Type   | Description             |
|-------|--------|-------------------------|
| id    | UUID   | ID of the specific user |

**Success 200**:
A List of following object:

| Field     | Type      | Description                                           |
|-----------|-----------|-------------------------------------------------------|
| id        |      UUID | ID of the right                                       |
| roomId    |      UUID | ID of the room where the rights match to              |
| accountId |      UUID | ID of the account where the rights match to           |
| role      | Role      | PARTICIPANT, EDITING_MODERATOR or EXECUTIVE_MODERATOR |
| lastVisit | Timestamp | Date of the last visit of the room                    |


---
## Update an user
```http
PUT /user/{id}
```

**Needed rights:** valid JWT for authorization. Only the user itself can modify.

**Path variables**:

| Field | Type   | Description             |
|-------|--------|-------------------------|
| id    | UUID   | ID of the specific user |

**Request body**:

| Field     | Type      | Description     |
|-----------|-----------|-----------------|
| id        |      UUID | ID of the user  |
| email     |    String | Email           |
| lastLogin | Timestamp | last login time |

**Success 200**:

| Field     | Type      | Description     |
|-----------|-----------|-----------------|
| id        |      UUID | ID of the user  |
| email     |    String | Email           |
| lastLogin | Timestamp | last login time |

**Error 4xx**:

| Field     | Description                          |
|-----------|--------------------------------------|
| Forbidden | You can only modify your own account |


---
## Create an user
```http
POST /user/
```

**Needed rights:** valid JWT for authorization.

**Request body**:

| Field     | Type      | Description     |
|-----------|-----------|-----------------|
| id        |      UUID | ID of the user  |
| email     |    String | Email           |
| lastLogin | Timestamp | last login time |

**Success 200**:

| Field     | Type      | Description     |
|-----------|-----------|-----------------|
| id        |      UUID | ID of the user  |
| email     |    String | Email           |
| lastLogin | Timestamp | last login time |


---
## Delete an user
```http
DELETE /user/{id}
```

**Needed rights:** valid JWT for authorization. Only the user itself can delete its account.

**Path variables**:

| Field | Type   | Description             |
|-------|--------|-------------------------|
| id    | UUID   | ID of the specific user |

**Request body**:

| Field     | Type      | Description     |
|-----------|-----------|-----------------|
| id        |      UUID | ID of the user  |
| email     |    String | Email           |
| lastLogin | Timestamp | last login time |

**Success 200**:
Empty Response

**Error 4xx**:

| Field     | Description                          |
|-----------|--------------------------------------|
| Forbidden | You can only delete your own account |


---
## Find user by email
```http
POST /user/find
```

**Needed rights:** valid JWT for authorization.

**Request body**:

| Field     | Type               | Description                                |
|-----------|--------------------|--------------------------------------------|
| findQuery | FindQuery<Account> | Query information like the provided email  |

FindQuery Object:

| Field      | Type      | Description                                           |
|------------|-----------|-------------------------------------------------------|
| properties | Account   | Account details to query for. Here this is the email  |


**Success 200**:

The result is an array of user objects with the following attributes:

| Field     | Type      | Description     |
|-----------|-----------|-----------------|
| id        |      UUID | ID of the user  |
| email     |    String | Email           |
| lastLogin | Timestamp | last login time |


---
## Create a new history entry for an user
```http
POST /user/{id}/roomHistory
```

**Needed rights:** valid JWT for authorization.

**Path variables**:

| Field | Type   | Description             |
|-------|--------|-------------------------|
| id    | UUID   | ID of the specific user |

**Request body**:

| Field     | Type      | Description                                           |
|-----------|-----------|-------------------------------------------------------|
| id        |      UUID | ID of the right                                       |
| roomId    |      UUID | ID of the room where the rights match to              |
| accountId |      UUID | ID of the account where the rights match to           |
| role      | Role      | PARTICIPANT, EDITING_MODERATOR or EXECUTIVE_MODERATOR |
| lastVisit | Timestamp | Date of the last visit of the room                    |

**Success 200**:

| Field     | Type      | Description                                           |
|-----------|-----------|-------------------------------------------------------|
| id        |      UUID | ID of the right                                       |
| roomId    |      UUID | ID of the room where the rights match to              |
| accountId |      UUID | ID of the account where the rights match to           |
| role      | Role      | PARTICIPANT, EDITING_MODERATOR or EXECUTIVE_MODERATOR |
| lastVisit | Timestamp | Date of the last visit of the room                    |

---
## Delete a roomhistory Entry
```http
DELETE /user/{id}/roomHistory/{roomId}
```

**Needed rights:** valid JWT for authorization. Only the user itself can delete its rights and history.

**Path variables**:

| Field  | Type   | Description             |
|--------|--------|-------------------------|
| id     | UUID   | ID of the specific user |
| roomId | UUID   | ID of the specific room |

**Success 200**:
empty Response

**Error 4xx**:

| Field        | Description                               |
|--------------|-------------------------------------------|
| Unauthorized | Deletion only allowed for the user itself |