variables:
  OUTPUT_DIR: target
  JAR_FILE: $OUTPUT_DIR/frag.jetzt-backend-*.jar

stages:
  - codestyle
  - build
  - test
  - package
  - push
  - deploy


.maven:
  tags:
    - maven
  variables:
    MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Dorg.slf4j.simpleLogger.dateTimeFormat=HH:mm:ss.SSS -Djava.awt.headless=true"
    MAVEN_CLI_OPTS: "--batch-mode --fail-at-end --show-version"
  cache:
    key: maven-repository
    paths:
      - .m2/repository

sonar:
  extends: .maven
  stage: codestyle
  only:
    - staging
  script:
    - mvn $MAVEN_CLI_OPTS clean verify sonar:sonar -Dsonar.login=$SONAR_TOKEN -Dsonar.host.url=https://scm.thm.de/sonar/

compile:
  extends: .maven
  stage: build
  artifacts:
    paths:
      - $OUTPUT_DIR
  script:
    - mvn $MAVEN_CLI_OPTS test-compile

unit_test:
  extends: .maven
  stage: test
  needs:
    - compile
  artifacts:
    paths:
      - $OUTPUT_DIR
    reports:
      junit: $OUTPUT_DIR/surefire-reports/TEST-*.xml
  coverage: '/Code coverage: \d+\.\d+/'
  script:
    - mvn $MAVEN_CLI_OPTS jacoco:prepare-agent surefire:test jacoco:report
    - awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print "Instructions covered:", covered, "/", instructions; print "Code coverage:", 100 * covered / instructions "%" }' "$OUTPUT_DIR/site/jacoco/jacoco.csv"

package:
  extends: .maven
  stage: package
  needs:
    - compile
  artifacts:
    name: package
    paths:
      - $JAR_FILE
  script:
     - mvn $MAVEN_CLI_OPTS package

docker_hub:
  stage: push
  only:
    - staging
    - master
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  tags:
    - kaniko
  dependencies:
    - package
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$DOCKER_REGISTRY_URL\":{\"auth\":\"$DOCKER_REGISTRY_TOKEN\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor
      --context     "$CI_PROJECT_DIR"
      --dockerfile  "$CI_PROJECT_DIR/Dockerfile.kaniko"
      --build-arg   "JAR_FILE=$JAR_FILE"
      --destination "arsnova/fragjetzt-backend:$CI_COMMIT_REF_SLUG"

gitlab_registry:
  stage: push
  only:
    - staging
    - master
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  tags:
    - kaniko
  dependencies:
    - package
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor
      --context     "$CI_PROJECT_DIR"
      --dockerfile  "$CI_PROJECT_DIR/Dockerfile.kaniko"
      --build-arg   "JAR_FILE=$JAR_FILE"
      --destination "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"

deploy_frag_thm:
  stage: deploy
  only:
    - master
  tags:
    - ssh
  script:
    - eval $(ssh-agent -s)
    - mkdir ~/.ssh
    - ssh-keyscan "$SSH_FRAG_THM_SERVER_URL" >> ~/.ssh/known_hosts
    - ssh-add <(echo "$SSH_FRAG_THM_PRIVATE_KEY")
    - ssh "$SSH_FRAG_THM_USER"

deploy_staging:
  stage: deploy
  only:
    - staging
  tags:
    - ssh
  script:
    - eval $(ssh-agent -s)
    - mkdir ~/.ssh
    - ssh-keyscan "$STAGING_SERVER_URL" >> ~/.ssh/known_hosts
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - ssh $STAGING_SSH_CMD "touch ~/deploy"

deploy_production:
  stage: deploy
  only:
    - master
  tags:
    - ssh
  script:
    - eval $(ssh-agent -s)
    - mkdir ~/.ssh
    - ssh-keyscan "$SERVER_URL" >> ~/.ssh/known_hosts
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - ssh $SSH_CMD "touch ~/deploy"
