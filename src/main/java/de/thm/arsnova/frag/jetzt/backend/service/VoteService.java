package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.*;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.DownvoteRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.UpvoteRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@Service
public class VoteService {

    private final AuthorizationHelper authorizationHelper;
    private final UpvoteRepository upvoteRepository;
    private final DownvoteRepository downvoteRepository;
    private final AccountService accountService;
    private final CommentService commentService;
    private final RoomRepository roomRepository;
    private final CommentChangeService commentChangeService;

    @Autowired
    public VoteService(
            AuthorizationHelper authorizationHelper,
            UpvoteRepository upvoteRepository,
            DownvoteRepository downvoteRepository,
            AccountService accountService,
            CommentService commentService,
            RoomRepository roomRepository,
            CommentChangeService commentChangeService
    ) {
        this.authorizationHelper = authorizationHelper;
        this.upvoteRepository = upvoteRepository;
        this.downvoteRepository = downvoteRepository;
        this.accountService = accountService;
        this.commentService = commentService;
        this.roomRepository = roomRepository;
        this.commentChangeService = commentChangeService;
    }

    public Mono<AbstractVote> get(UUID id) {
        return upvoteRepository.findById(id)
                .map(upvote -> (AbstractVote) upvote)
                .switchIfEmpty(downvoteRepository.findById(id));
    }

    public Flux<AbstractVote> get(List<UUID> ids) {
        return upvoteRepository.findAllById(ids)
                .map(upvote -> (AbstractVote) upvote)
                .switchIfEmpty(downvoteRepository.findAllById(ids));
    }

    public Mono<AbstractVote> get(UUID commentId, UUID accountId) {
        return upvoteRepository.findByCommentIdAndAccountId(commentId, accountId)
                .map(upvote -> (AbstractVote) upvote)
                .switchIfEmpty(downvoteRepository.findByCommentIdAndAccountId(commentId, accountId));
    }

    public Mono<AbstractVote> getForCommentAndUser(UUID commentId, UUID userId) {
        return upvoteRepository.findByCommentIdAndAccountId(commentId, userId)
                .map(upvote -> (AbstractVote) upvote)
                .switchIfEmpty(downvoteRepository.findByCommentIdAndAccountId(commentId, userId));
    }

    public Flux<AbstractVote> getForCommentsAndUser(List<UUID> commentIds, UUID accountId) {
        return upvoteRepository.findAllByAccountIdAndCommentIdIn(accountId, commentIds)
                .map(upvote -> (AbstractVote) upvote)
                .switchIfEmpty(downvoteRepository.findAllByAccountIdAndCommentIdIn(accountId, commentIds));
    }

    public Mono<Upvote> createUpvote(Upvote upvote) {
        return authorizationHelper.getCurrentUser()
                .flatMap(user -> accountService.refreshLastActive(user.getAccountId(), Mono.just(user)))
                .flatMap(user -> Mono.zip(
                        Mono.just(user),
                        upvoteRepository.findByCommentIdAndAccountId(upvote.getCommentId(), user.getAccountId())
                                .switchIfEmpty(Mono.just(upvote).map(currentUpvote -> {
                                    currentUpvote.setId(null);
                                    currentUpvote.setAccountId(user.getAccountId());
                                    return currentUpvote;
                                })),
                        commentService.get(upvote.getCommentId())
                ))
                .flatMap(tuple3 -> downvoteRepository
                        .deleteByAccountIdAndCommentId(tuple3.getT1().getAccountId(), tuple3.getT3().getId())
                        .flatMap(deleteComplete -> upvoteRepository.save(tuple3.getT2()))
                        .switchIfEmpty(upvoteRepository.save(tuple3.getT2()))
                        .flatMap(upvote1 -> Mono.zip(
                                Mono.just(upvote1),
                                Mono.just(tuple3.getT1()),
                                Mono.just(tuple3.getT3()),
                                commentService.get(tuple3.getT3().getId()),
                                roomRepository.findById(tuple3.getT3().getRoomId()),
                                commentService.getUserInformation(tuple3.getT3().getRoomId(), tuple3.getT1())
                        )))
                .flatMap(tuple6 -> {
                    Comment previous = tuple6.getT3();
                    Comment newComment = tuple6.getT4();
                    return commentChangeService.create(new CommentChange(
                            newComment.getId(),
                            newComment.getRoomId(),
                            CommentChange.CommentChangeType.CHANGE_SCORE,
                            previous.getScore() + "/" + previous.getUpvotes() + "/" + previous.getDownvotes(),
                            newComment.getScore() + "/" + newComment.getUpvotes() + "/" + newComment.getDownvotes(),
                            tuple6.getT2().getAccountId(),
                            tuple6.getT6()
                    )).map(change -> {
                        commentService.sendAmqpMessage(change, tuple6.getT4(), tuple6.getT5());
                        return tuple6.getT1();
                    });
                });
    }

    public Mono<Downvote> createDownvote(Downvote downvote) {
        return authorizationHelper.getCurrentUser()
                .flatMap(user -> accountService.refreshLastActive(user.getAccountId(), Mono.just(user)))
                .flatMap(user -> Mono.zip(
                        Mono.just(user),
                        downvoteRepository.findByCommentIdAndAccountId(downvote.getCommentId(), user.getAccountId())
                                .switchIfEmpty(Mono.just(downvote).map(currentDownvote -> {
                                    currentDownvote.setId(null);
                                    currentDownvote.setAccountId(user.getAccountId());
                                    return currentDownvote;
                                })),
                        commentService.get(downvote.getCommentId())
                ))
                .flatMap(tuple3 -> upvoteRepository
                        .deleteByAccountIdAndCommentId(tuple3.getT1().getAccountId(), tuple3.getT3().getId())
                        .flatMap(deleteComplete -> downvoteRepository.save(tuple3.getT2()))
                        .switchIfEmpty(downvoteRepository.save(tuple3.getT2()))
                        .flatMap(downvote1 -> Mono.zip(
                                Mono.just(downvote1),
                                Mono.just(tuple3.getT1()),
                                Mono.just(tuple3.getT3()),
                                commentService.get(tuple3.getT3().getId()),
                                roomRepository.findById(tuple3.getT3().getRoomId()),
                                commentService.getUserInformation(tuple3.getT3().getRoomId(), tuple3.getT1())
                        )))
                .flatMap(tuple6 -> {
                    Comment previous = tuple6.getT3();
                    Comment newComment = tuple6.getT4();
                    return commentChangeService.create(new CommentChange(
                            newComment.getId(),
                            newComment.getRoomId(),
                            CommentChange.CommentChangeType.CHANGE_SCORE,
                            previous.getScore() + "/" + previous.getUpvotes() + "/" + previous.getDownvotes(),
                            newComment.getScore() + "/" + newComment.getUpvotes() + "/" + newComment.getDownvotes(),
                            tuple6.getT2().getAccountId(),
                            tuple6.getT6()
                    )).map(change -> {
                        commentService.sendAmqpMessage(change, tuple6.getT4(), tuple6.getT5());
                        return tuple6.getT1();
                    });
                });
    }

    public Mono<Void> deleteUpvote(Upvote upvote) {
        return authorizationHelper.getCurrentUser()
                .filter(authenticatedUser -> authenticatedUser.getAccountId().equals(upvote.getAccountId()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(authenticatedUser -> upvoteRepository.delete(upvote));
    }

    public Mono<Void> deleteDownvote(Downvote downvote) {
        return authorizationHelper.getCurrentUser()
                .filter(authenticatedUser -> authenticatedUser.getAccountId().equals(downvote.getAccountId()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(authenticatedUser -> downvoteRepository.delete(downvote));
    }

    public Mono<Void> resetVote(UUID commentId, UUID userId) {
        return authorizationHelper.getCurrentUser()
                .filter(authenticatedUser -> authenticatedUser.getAccountId().equals(userId))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(authenticatedUser -> Mono.zip(
                        accountService.refreshLastActive(authenticatedUser.getAccountId(), Mono.just(authenticatedUser)),
                        commentService.get(commentId)
                ))
                .flatMap(tuple2 -> Mono.zip(
                        Mono.just(tuple2.getT1()),
                        Mono.just(tuple2.getT2()),
                        upvoteRepository.deleteByAccountIdAndCommentId(userId, commentId)
                                .switchIfEmpty(downvoteRepository.deleteByAccountIdAndCommentId(userId, commentId))
                                .map(d -> "")
                                .switchIfEmpty(Mono.just("")),
                        roomRepository.findById(tuple2.getT2().getRoomId()),
                        commentService.get(commentId),
                        commentService.getUserInformation(tuple2.getT2().getRoomId(), tuple2.getT1())
                ))
                .flatMap(tuple6 -> {
                    Comment previous = tuple6.getT2();
                    Comment newComment = tuple6.getT5();
                    return commentChangeService.create(new CommentChange(
                            newComment.getId(),
                            newComment.getRoomId(),
                            CommentChange.CommentChangeType.CHANGE_SCORE,
                            previous.getScore() + "/" + previous.getUpvotes() + "/" + previous.getDownvotes(),
                            newComment.getScore() + "/" + newComment.getUpvotes() + "/" + newComment.getDownvotes(),
                            tuple6.getT1().getAccountId(),
                            tuple6.getT6()
                    )).flatMap(change -> {
                        commentService.sendAmqpMessage(change, tuple6.getT5(), tuple6.getT4());
                        return Mono.empty();
                    });
                });
    }
}
