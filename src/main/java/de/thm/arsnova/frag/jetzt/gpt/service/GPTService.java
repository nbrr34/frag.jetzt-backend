package de.thm.arsnova.frag.jetzt.gpt.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.thm.arsnova.frag.jetzt.backend.model.Room;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.InternalServerErrorException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig.GPTActivationCode;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTCounters;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTCompletionModerationMarker;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTGlobalAccessInfo;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTModerationWrapped;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTQuotaUnit;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTRoomAccessInfo;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRequestStatistic;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTUser;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionChoice;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionDeltaChoice;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionMessage;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTCompletionDoneMarker;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTCompletionResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModerationRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModerationResponse.GPTModerationResponseResult;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTTextCompletionChoice;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTTextCompletionRequest;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTTextCompletionResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.IGPTStreamObject;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTUserRepository;
import de.thm.arsnova.frag.jetzt.gpt.util.GPTModel;
import de.thm.arsnova.frag.jetzt.gpt.util.STRateLimiter;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class GPTService {

  private static final Logger logger = LoggerFactory.getLogger(
    GPTService.class
  );
  public static final String STAGE_0 = "0 - Build Info";
  public static final String STAGE_1 = "1 - Quota Reservation";
  public static final String STAGE_2 = "2 - Profanity detection";
  public static final String STAGE_3 = "3 - Request";
  public static final String STAGE_4 = "4 - Moderation verify";

  public static final class ApiPrompt {

    public final String promptText;
    public final int promptTokens;
    public final int requestedTokens;
    public Object completion;
    public String completionText;
    public int completionTokens = 0;
    public Instant lastTokenGenerated = null;

    public ApiPrompt(String promptText, int promptTokens, int requestedTokens) {
      this.promptText = promptText;
      this.promptTokens = promptTokens;
      this.requestedTokens = requestedTokens;
    }
  }

  public static final class ApiRequestInfo {

    public final AuthenticatedUser account;
    public final GPTUser user;
    public final Room room;
    public final String apiKey;
    public final String apiOrganization;
    public final GPTGlobalAccessInfo globalAccess;
    public final GPTRoomAccessInfo roomAccess;
    public final GPTModel model;
    public final List<ApiPrompt> prompts = new ArrayList<>();

    public ApiRequestInfo(
      AuthenticatedUser account,
      GPTUser user,
      Room room,
      String apiKey,
      String apiOrganization,
      GPTGlobalAccessInfo globalAccess,
      GPTRoomAccessInfo roomAccess,
      GPTModel model
    ) {
      this.account = account;
      this.user = user;
      this.room = room;
      this.apiKey = apiKey;
      this.apiOrganization = apiOrganization;
      this.globalAccess = globalAccess;
      this.roomAccess = roomAccess;
      this.model = model;
    }

    public GPTQuotaUnit getRequestedCost() {
      GPTQuotaUnit sum = new GPTQuotaUnit(0, 8);
      for (ApiPrompt p : prompts) {
        sum =
          sum.add(
            model
              .getCostPerCompletionToken()
              .multiply(p.requestedTokens)
              .add(model.getCostPerPromptToken().multiply(p.promptTokens))
          );
      }
      return sum;
    }

    public GPTQuotaUnit getActualCost() {
      GPTQuotaUnit sum = new GPTQuotaUnit(0, 8);
      for (ApiPrompt p : prompts) {
        sum =
          sum.add(
            model
              .getCostPerCompletionToken()
              .multiply(p.completionTokens)
              .add(model.getCostPerPromptToken().multiply(p.promptTokens))
          );
      }
      return sum;
    }
  }

  private final ObjectMapper mapper = new ObjectMapper();
  private final GPTUserRepository repository;
  private final GPTAccessService accessService;
  private final GPTRequestService requestService;
  private final GPTRoomService roomService;
  private final RoomRepository roomRepository;
  private final AuthorizationHelper helper;
  private final GPTConfig config;
  private final GPTCounters counters;
  private final GPTStatisticService statisticService;
  // apiKey -> limiter
  private final HashMap<String, STRateLimiter> map = new HashMap<>();

  @Autowired
  public GPTService(
    GPTUserRepository repository,
    GPTAccessService accessService,
    GPTRequestService requestService,
    GPTRoomService roomService,
    RoomRepository roomRepository,
    AuthorizationHelper helper,
    GPTConfig config,
    GPTCounters counters,
    GPTStatisticService statisticService
  ) {
    this.repository = repository;
    this.accessService = accessService;
    this.requestService = requestService;
    this.roomService = roomService;
    this.roomRepository = roomRepository;
    this.helper = helper;
    this.config = config;
    this.counters = counters;
    this.statisticService = statisticService;
  }

  public Mono<GPTUser> getUserByAccount(UUID accountId) {
    return repository
      .findByAccountId(accountId)
      .switchIfEmpty(
        repository
          .save(new GPTUser(accountId))
          .flatMap(user -> repository.findById(user.getId()))
      );
  }

  public Mono<GPTModerationWrapped<GPTTextCompletionResponse>> requestText(
    GPTTextCompletionRequest request
  ) {
    final int n = request.getN() != null ? request.getN() : 1;
    final int requestTokens = request.getMaxTokens() != null
      ? request.getMaxTokens()
      : 16;
    return getInfo(request.getRoomId(), request.getModel())
      .flatMap(info -> {
        request.setApiKey(info.apiKey);
        request.setApiOrganization(info.apiOrganization);
        for (String prompt : request.getPrompt()) {
          final int promptTokens = info.model
            .getEncoder()
            .getEncodeCount(prompt);
          if (promptTokens + requestTokens > info.model.getMaxTokens()) {
            return Mono.error(
              new BadRequestException(
                makeErrorMessage(
                  STAGE_0,
                  "backend-error.model-too-less-context"
                )
              )
            );
          }
          for (int i = 0; i < n; i++) {
            info.prompts.add(
              new ApiPrompt(prompt, promptTokens, requestTokens)
            );
          }
        }
        return Mono.just(info);
      })
      .flatMap(info -> {
        return ensureAndReserveQuota(info)
          .flatMap(hasOwnApi -> handleModRequest(info))
          .flatMap(modOkay -> {
            return getRateLimiter(info.apiKey)
              .ensureLimits(
                requestService.requestTextCompletion(
                  info.account.getAccountId(),
                  request
                )
              )
              .onErrorResume(error -> {
                if (error instanceof ResponseStatusException) {
                  ResponseStatusException r = (ResponseStatusException) error;
                  return Mono.error(
                    new ResponseStatusException(
                      r.getStatus(),
                      makeErrorMessage(STAGE_3, "Own: " + error.getMessage()),
                      error
                    )
                  );
                }
                return Mono.error(
                  new InternalServerErrorException(
                    makeErrorMessage(STAGE_3, error.getMessage()),
                    error
                  )
                );
              })
              .flatMap(completion -> {
                GPTModerationRequest modRequest = new GPTModerationRequest();
                modRequest.setApiKey(info.apiKey);
                modRequest.setApiOrganization(info.apiOrganization);
                ArrayList<String> toCheck = new ArrayList<>(
                  info.prompts.size()
                );
                List<GPTTextCompletionChoice> list = completion.getChoices();
                for (int i = 0; i < list.size(); i++) {
                  ApiPrompt p = info.prompts.get(i);
                  GPTTextCompletionChoice c = list.get(i);
                  p.completionText = c.getText();
                  p.completionTokens =
                    info.model.getEncoder().getEncodeCount(p.completionText);
                  toCheck.add(p.promptText + p.completionText);
                }
                modRequest.setInput(toCheck);
                return saveMultiple(
                  finishReservation(info)
                    .flatMap(ignore ->
                      requestService.requestModeration(modRequest)
                    ),
                  info
                )
                  .map(mod ->
                    new GPTModerationWrapped<GPTTextCompletionResponse>(
                      completion,
                      mod
                    )
                  )
                  .onErrorResume(error -> {
                    return Mono.error(
                      new InternalServerErrorException(
                        makeErrorMessage(STAGE_4, error.getMessage()),
                        error
                      )
                    );
                  });
              });
          });
      });
  }

  public Mono<GPTModerationWrapped<GPTChatCompletionResponse>> requestChat(
    GPTChatCompletionRequest request
  ) {
    final int n = request.getN() != null ? request.getN() : 1;
    return getInfo(request.getRoomId(), request.getModel())
      .flatMap(info -> {
        request.setApiKey(info.apiKey);
        request.setApiOrganization(info.apiOrganization);
        final String prompt = GPTChatCompletionRequest.combineMessages(
          request.getMessages()
        );
        final int promptTokens = info.model.getEncoder().getEncodeCount(prompt);
        int requestTokens = request.getMaxTokens() != null
          ? request.getMaxTokens()
          : info.model.getMaxTokens() - promptTokens;
        if (requestTokens < 16) {
          requestTokens = 16;
        }
        if (promptTokens + requestTokens > info.model.getMaxTokens()) {
          return Mono.error(
            new BadRequestException(
              makeErrorMessage(STAGE_0, "backend-error.model-too-less-context")
            )
          );
        }
        for (int i = 0; i < n; i++) {
          info.prompts.add(new ApiPrompt(prompt, promptTokens, requestTokens));
        }
        return Mono.just(info);
      })
      .flatMap(info -> {
        return ensureAndReserveQuota(info)
          .flatMap(hasOwnApi -> handleModRequest(info))
          .flatMap(modOkay -> {
            return getRateLimiter(info.apiKey)
              .ensureLimits(
                requestService.requestChatCompletion(
                  info.account.getAccountId(),
                  request
                )
              )
              .onErrorResume(error -> {
                if (error instanceof ResponseStatusException) {
                  ResponseStatusException r = (ResponseStatusException) error;
                  return Mono.error(
                    new ResponseStatusException(
                      r.getStatus(),
                      makeErrorMessage(STAGE_3, "Own: " + error.getMessage()),
                      error
                    )
                  );
                }
                return Mono.error(
                  new InternalServerErrorException(
                    makeErrorMessage(STAGE_3, error.getMessage()),
                    error
                  )
                );
              })
              .flatMap(completion -> {
                GPTModerationRequest modRequest = new GPTModerationRequest();
                modRequest.setApiKey(info.apiKey);
                modRequest.setApiOrganization(info.apiOrganization);
                ArrayList<String> toCheck = new ArrayList<>(
                  info.prompts.size()
                );
                List<GPTChatCompletionChoice> list = completion.getChoices();
                for (int i = 0; i < list.size(); i++) {
                  ApiPrompt p = info.prompts.get(i);
                  p.completionText = list.get(i).getMessage().wrap();
                  p.completionTokens =
                    info.model.getEncoder().getEncodeCount(p.completionText);
                  toCheck.add(p.promptText + p.completionText);
                }
                modRequest.setInput(toCheck);
                return saveMultiple(
                  finishReservation(info)
                    .flatMap(ignore ->
                      requestService.requestModeration(modRequest)
                    ),
                  info
                )
                  .map(mod ->
                    new GPTModerationWrapped<GPTChatCompletionResponse>(
                      completion,
                      mod
                    )
                  )
                  .onErrorResume(error -> {
                    return Mono.error(
                      new InternalServerErrorException(
                        makeErrorMessage(STAGE_4, error.getMessage()),
                        error
                      )
                    );
                  });
              });
          });
      });
  }

  public Flux<ServerSentEvent<IGPTStreamObject>> requestStreamChat(
    GPTChatCompletionRequest request
  ) {
    final int n = request.getN() != null ? request.getN() : 1;
    return getInfo(request.getRoomId(), request.getModel())
      .flatMap(info -> {
        request.setApiKey(info.apiKey);
        request.setApiOrganization(info.apiOrganization);
        final String prompt = GPTChatCompletionRequest.combineMessages(
          request.getMessages()
        );
        final int promptTokens = info.model.getEncoder().getEncodeCount(prompt);
        int requestTokens = request.getMaxTokens() != null
          ? request.getMaxTokens()
          : info.model.getMaxTokens() - promptTokens;
        if (requestTokens < 16) {
          requestTokens = 16;
        }
        if (promptTokens + requestTokens > info.model.getMaxTokens()) {
          return Mono.error(
            new BadRequestException(
              makeErrorMessage(STAGE_0, "backend-error.model-too-less-context")
            )
          );
        }
        for (int i = 0; i < n; i++) {
          info.prompts.add(new ApiPrompt(prompt, promptTokens, requestTokens));
        }
        return Mono.just(info);
      })
      .flatMapMany(info -> {
        return ensureAndReserveQuota(info)
          .flatMap(hasOwnApi -> handleModRequest(info))
          .flatMapMany(modOkay -> {
            return getRateLimiter(info.apiKey)
              .ensureLimits(
                requestService.requestStreamChatCompletion(
                  info.account.getAccountId(),
                  request
                )
              )
              .onErrorResume(error -> {
                if (error instanceof ResponseStatusException) {
                  ResponseStatusException r = (ResponseStatusException) error;
                  return Flux.error(
                    new ResponseStatusException(
                      r.getStatus(),
                      makeErrorMessage(STAGE_3, "Own: " + error.getMessage()),
                      error
                    )
                  );
                }
                return Flux.error(
                  new InternalServerErrorException(
                    makeErrorMessage(STAGE_3, error.getMessage()),
                    error
                  )
                );
              })
              .flatMap(event -> {
                IGPTStreamObject o = event.data();
                if (o instanceof GPTCompletionResponse) {
                  GPTCompletionResponse<?> response = (GPTCompletionResponse<?>) o;
                  response
                    .getChoices()
                    .forEach(choice -> {
                      if (!(choice instanceof GPTChatCompletionDeltaChoice)) {
                        return;
                      }
                      GPTChatCompletionDeltaChoice delta = (GPTChatCompletionDeltaChoice) choice;
                      ApiPrompt p = info.prompts.get(delta.getIndex());
                      p.lastTokenGenerated = Instant.now();
                      @SuppressWarnings("unchecked")
                      ArrayList<GPTChatCompletionMessage> list = (ArrayList<GPTChatCompletionMessage>) p.completion;
                      if (list == null) {
                        list = new ArrayList<>();
                        p.completion = list;
                      }
                      if (delta.getDelta().getRole() != null) {
                        GPTChatCompletionMessage msg = new GPTChatCompletionMessage();
                        msg.setRole(delta.getDelta().getRole());
                        list.add(msg);
                      }
                      if (delta.getDelta().getContent() != null) {
                        GPTChatCompletionMessage msg = list.get(
                          list.size() - 1
                        );
                        msg.setContent(
                          msg.getContent() + delta.getDelta().getContent()
                        );
                      }
                      if (delta.getFinishReason() != null) {
                        p.completionText =
                          GPTChatCompletionRequest.combineMessages(list);
                        p.completionTokens =
                          info.model
                            .getEncoder()
                            .getEncodeCount(p.completionText);
                      }
                    });
                  return Flux.just(event);
                }
                if (o instanceof GPTCompletionDoneMarker) {
                  ArrayList<String> msgs = new ArrayList<>(info.prompts.size());
                  for (ApiPrompt p : info.prompts) {
                    @SuppressWarnings("unchecked")
                    ArrayList<GPTChatCompletionMessage> list = (ArrayList<GPTChatCompletionMessage>) p.completion;
                    if (list == null) {
                      list = new ArrayList<>();
                      p.completion = list;
                    }
                    if (p.completionText == null) {
                      p.completionText =
                        GPTChatCompletionRequest.combineMessages(list);
                      p.completionTokens =
                        info.model
                          .getEncoder()
                          .getEncodeCount(p.completionText);
                    }
                    msgs.add(p.promptText + p.completionText);
                  }
                  GPTModerationRequest modRequest = new GPTModerationRequest();
                  modRequest.setApiKey(info.apiKey);
                  modRequest.setApiOrganization(info.apiOrganization);
                  modRequest.setInput(msgs);
                  return saveMultiple(
                    requestService.requestModeration(modRequest),
                    info
                  )
                    .onErrorResume(error -> {
                      return Mono.error(
                        new InternalServerErrorException(
                          makeErrorMessage(STAGE_4, error.getMessage()),
                          error
                        )
                      );
                    })
                    .flatMapMany(moderation -> {
                      GPTCompletionModerationMarker marker = new GPTCompletionModerationMarker();
                      marker.setResponse(moderation);
                      return Flux.just(
                        ServerSentEvent
                          .builder((IGPTStreamObject) marker)
                          .build()
                      );
                    });
                }
                return Flux.empty();
              })
              .doFinally(signal -> {
                for (ApiPrompt p : info.prompts) {
                  @SuppressWarnings("unchecked")
                  ArrayList<GPTChatCompletionMessage> list = (ArrayList<GPTChatCompletionMessage>) p.completion;
                  if (list == null) {
                    list = new ArrayList<>();
                    p.completion = list;
                  }
                  if (p.completionText == null) {
                    p.completionText =
                      GPTChatCompletionRequest.combineMessages(list);
                    int additional = p.lastTokenGenerated != null
                      ? info.model.getInterruptTokens()
                      : 0;
                    p.completionTokens =
                      info.model.getEncoder().getEncodeCount(p.completionText) +
                      additional;
                  }
                }
                finishReservation(info).subscribe();
              });
          });
      });
  }

  public Flux<ServerSentEvent<IGPTStreamObject>> requestStreamText(
    GPTTextCompletionRequest request
  ) {
    final int n = request.getN() != null ? request.getN() : 1;
    final int requestTokens = request.getMaxTokens() != null
      ? request.getMaxTokens()
      : 16;
    return getInfo(request.getRoomId(), request.getModel())
      .flatMap(info -> {
        request.setApiKey(info.apiKey);
        request.setApiOrganization(info.apiOrganization);
        for (String prompt : request.getPrompt()) {
          final int promptTokens = info.model
            .getEncoder()
            .getEncodeCount(prompt);
          if (promptTokens + requestTokens > info.model.getMaxTokens()) {
            return Mono.error(
              new BadRequestException(
                makeErrorMessage(
                  STAGE_0,
                  "backend-error.model-too-less-context"
                )
              )
            );
          }
          for (int i = 0; i < n; i++) {
            info.prompts.add(
              new ApiPrompt(prompt, promptTokens, requestTokens)
            );
          }
        }
        return Mono.just(info);
      })
      .flatMapMany(info -> {
        return ensureAndReserveQuota(info)
          .flatMap(hasOwnApi -> handleModRequest(info))
          .flatMapMany(modOkay -> {
            return getRateLimiter(info.apiKey)
              .ensureLimits(
                requestService.requestStreamTextCompletion(
                  info.account.getAccountId(),
                  request
                )
              )
              .onErrorResume(error -> {
                if (error instanceof ResponseStatusException) {
                  ResponseStatusException r = (ResponseStatusException) error;
                  return Flux.error(
                    new ResponseStatusException(
                      r.getStatus(),
                      makeErrorMessage(STAGE_3, "Own: " + error.getMessage()),
                      error
                    )
                  );
                }
                return Flux.error(
                  new InternalServerErrorException(
                    makeErrorMessage(STAGE_3, error.getMessage()),
                    error
                  )
                );
              })
              .flatMap(event -> {
                IGPTStreamObject o = event.data();
                if (o instanceof GPTCompletionResponse) {
                  GPTCompletionResponse<?> response = (GPTCompletionResponse<?>) o;
                  response
                    .getChoices()
                    .forEach(choice -> {
                      if (!(choice instanceof GPTTextCompletionChoice)) {
                        return;
                      }
                      GPTTextCompletionChoice textChoice = (GPTTextCompletionChoice) choice;
                      ApiPrompt p = info.prompts.get(textChoice.getIndex());
                      p.lastTokenGenerated = Instant.now();
                      String text = (String) p.completion;
                      if (text == null) {
                        text = "";
                      }
                      p.completion = text + textChoice.getText();
                      if (textChoice.getFinishReason() != null) {
                        p.completionText = (String) p.completion;
                        p.completionTokens =
                          info.model
                            .getEncoder()
                            .getEncodeCount(p.completionText);
                      }
                    });
                  return Flux.just(event);
                }
                if (o instanceof GPTCompletionDoneMarker) {
                  GPTModerationRequest modRequest = new GPTModerationRequest();
                  modRequest.setApiKey(info.apiKey);
                  modRequest.setApiOrganization(info.apiOrganization);
                  ArrayList<String> toCheck = new ArrayList<>(
                    info.prompts.size()
                  );
                  for (ApiPrompt p : info.prompts) {
                    if (p.completionText == null) {
                      String text = (String) p.completion;
                      if (text == null) {
                        text = "";
                        p.completion = text;
                      }
                      p.completionText = text;
                      p.completionTokens =
                        info.model
                          .getEncoder()
                          .getEncodeCount(p.completionText);
                    }
                    toCheck.add(p.promptText + p.completionText);
                  }
                  modRequest.setInput(toCheck);
                  return saveMultiple(
                    requestService.requestModeration(modRequest),
                    info
                  )
                    .onErrorResume(error -> {
                      return Mono.error(
                        new InternalServerErrorException(
                          makeErrorMessage(STAGE_4, error.getMessage()),
                          error
                        )
                      );
                    })
                    .flatMapMany(moderation -> {
                      GPTCompletionModerationMarker marker = new GPTCompletionModerationMarker();
                      marker.setResponse(moderation);
                      return Flux.just(
                        ServerSentEvent
                          .builder((IGPTStreamObject) marker)
                          .build()
                      );
                    });
                }
                return Flux.empty();
              })
              .doFinally(signal -> {
                for (ApiPrompt p : info.prompts) {
                  if (p.completionText == null) {
                    String text = (String) p.completion;
                    if (text == null) {
                      text = "";
                      p.completion = text;
                    }
                    p.completionText = text;
                    int additional = p.lastTokenGenerated != null
                      ? info.model.getInterruptTokens()
                      : 0;
                    p.completionTokens =
                      info.model.getEncoder().getEncodeCount(p.completionText) +
                      additional;
                  }
                }
                finishReservation(info).subscribe();
              });
          });
      });
  }

  public Mono<Boolean> cancelRequest(UUID accountId) {
    return requestService.abortRequest(accountId);
  }

  private Mono<Boolean> finishReservation(ApiRequestInfo info) {
    GPTQuotaUnit cost = info.getRequestedCost();
    GPTQuotaUnit actualCost = info.getActualCost();
    if (info.room == null) {
      counters.getLimiter().finishReservation(cost, actualCost);
      return Mono.just(false);
    }
    return roomService
      .getInternal(info.room.getId())
      .flatMap(setting -> {
        final boolean isCreator = helper.checkCreatorOfRoom(
          info.account,
          info.room
        );
        final boolean isMod = helper.checkOnlyModeratorOfRoom(
          info.account,
          info.room
        );
        final boolean hasOwnApi = setting.getApiKey() != null;
        if (!hasOwnApi) {
          GPTActivationCode c = setting.getTrialCode();
          if (c != null) {
            c.getLimiter().finishReservation(cost, actualCost);
          }
        }
        return Mono
          .just(setting.makeLimiter(isCreator, isMod))
          .flatMap(limiter -> {
            counters.getLimiter().finishReservation(cost, actualCost);
            limiter.finishReservation(cost, actualCost);
            setting.reapplyLimiter(limiter, isCreator, isMod);
            return roomService.saveInternal(setting);
          })
          .map(ignored -> hasOwnApi);
      });
  }

  private Mono<Boolean> ensureAndReserveQuota(ApiRequestInfo info) {
    GPTQuotaUnit cost = info.getRequestedCost();
    if (info.room == null) {
      if (!counters.getLimiter().tryReserve(cost)) {
        return Mono.error(
          new BadRequestException(
            makeErrorMessage(STAGE_1, "backend-error.gpt.quota-empty")
          )
        );
      }
      return Mono.just(false);
    }
    return roomService
      .getInternal(info.room.getId())
      .flatMap(setting -> {
        final boolean isCreator = helper.checkCreatorOfRoom(
          info.account,
          info.room
        );
        final boolean isMod = helper.checkOnlyModeratorOfRoom(
          info.account,
          info.room
        );
        final boolean hasOwnApi = setting.getApiKey() != null;
        boolean hasReservedOnCode = false;
        if (!hasOwnApi) {
          GPTActivationCode c = setting.getTrialCode();
          if (c == null) {
            if (info.globalAccess.isGlobalActive()) {
              setting.setMaxAccumulatedRoomCost((long) config.getRestrictions().getGlobalAccumulatedQuota());
            } else {
              return Mono.error(
                new BadRequestException(
                  makeErrorMessage(STAGE_1, "backend-error.gpt.no-api")
                )
              );
            }
          } else if (!c.getLimiter().tryReserve(cost)) {
            return Mono.error(
              new BadRequestException(
                makeErrorMessage(STAGE_1, "backend-error.gpt.quota-empty")
              )
            );
          }
          hasReservedOnCode = c != null;
        }
        return Mono
          .just(setting.makeLimiter(isCreator, isMod))
          .filter(limiter -> limiter.tryReserve(cost))
          .switchIfEmpty(
            Mono
              .just(hasReservedOnCode)
              .flatMap(reserved -> {
                if (reserved.booleanValue()) {
                  setting
                    .getTrialCode()
                    .getLimiter()
                    .finishReservation(cost, new GPTQuotaUnit(0, 0));
                }
                return Mono.error(
                  new BadRequestException(
                    makeErrorMessage(STAGE_1, "backend-error.gpt.quota-empty")
                  )
                );
              })
          )
          .flatMap(limiter -> {
            counters.getLimiter().tryReserve(cost);
            setting.reapplyLimiter(limiter, isCreator, isMod);
            return roomService.saveInternal(setting);
          })
          .map(ignored -> hasOwnApi);
      });
  }

  private <T> Mono<T> saveMultiple(Mono<T> mono, ApiRequestInfo info) {
    return Flux
      .range(0, info.prompts.size())
      .flatMap(i ->
        statisticService.save(
          new GPTRequestStatistic(
            info.user.getId(),
            info.room.getId(),
            info.prompts.get(i).promptText,
            info.prompts.get(i).completionText
          )
        )
      )
      .then(mono);
  }

  private STRateLimiter getRateLimiter(String apiKey) {
    return map.computeIfAbsent(
      apiKey,
      k -> new STRateLimiter(Duration.ofMinutes(1), -1)
    );
  }

  private Mono<Boolean> handleModRequest(ApiRequestInfo info) {
    GPTModerationRequest modRequest = new GPTModerationRequest();
    modRequest.setApiKey(info.apiKey);
    modRequest.setApiOrganization(info.apiOrganization);
    ArrayList<String> list = new ArrayList<>();
    for (ApiPrompt p : info.prompts) {
      list.add(p.promptText);
    }
    if (list.size() < 1) {
      return Mono.just(true);
    }
    modRequest.setInput(list);
    return requestService
      .requestModeration(modRequest)
      .onErrorResume(error -> {
        return Mono.error(
          new InternalServerErrorException(
            makeErrorMessage(STAGE_2, "Error: " + error.getMessage()),
            error
          )
        );
      })
      .flatMap(response -> {
        for (GPTModerationResponseResult result : response.getResults()) {
          if (result.isFlagged()) {
            try {
              return Mono.error(
                new ForbiddenException(
                  makeErrorMessage(
                    STAGE_2,
                    "Moderation: " + mapper.writeValueAsString(response)
                  )
                )
              );
            } catch (JsonProcessingException e) {
              return Mono.error(
                new InternalServerErrorException(
                  makeErrorMessage(STAGE_2, "JSON-Error: " + e.getMessage()),
                  e
                )
              );
            }
          }
        }
        return Mono.just(true);
      });
  }

  private String makeErrorMessage(String stage, String message) {
    return "Stage (" + stage + "): " + message;
  }

  private Mono<ApiRequestInfo> getInfo(UUID roomId, String model) {
    final GPTModel m = GPTModel.getAllowedModels().get(model);
    if (m == null) {
      return Mono.error(
        new BadRequestException(
          makeErrorMessage(STAGE_0, "backend-error.gpt.model-not-registered")
        )
      );
    }
    if (roomId == null) {
      return helper
        .getCurrentUser()
        .filter(helper::isSuperAdmin)
        .switchIfEmpty(
          Mono.error(
            new ForbiddenException(
              makeErrorMessage(STAGE_0, "backend-error.gpt.must-be-admin")
            )
          )
        )
        .flatMap(user ->
          Mono.zip(
            Mono.just(user),
            getUserByAccount(user.getAccountId()),
            accessService.getGlobalAccessInfo()
          )
        )
        .filter(tuple -> !tuple.getT3().isRestricted())
        .switchIfEmpty(
          Mono.error(
            new BadRequestException(
              makeErrorMessage(STAGE_0, "backend-error.gpt.global-restricted")
            )
          )
        )
        .filter(tuple ->
          tuple.getT2().getConsented() != null && tuple.getT2().getConsented()
        )
        .switchIfEmpty(
          Mono.error(
            new BadRequestException(
              makeErrorMessage(STAGE_0, "backend-error.gpt.not-consented")
            )
          )
        )
        .map(tuple ->
          new ApiRequestInfo(
            tuple.getT1(),
            tuple.getT2(),
            null,
            config.getApiKey(),
            config.getOrganization(),
            tuple.getT3(),
            null,
            m
          )
        );
    }
    return helper
      .getCurrentUser()
      .flatMap(user ->
        Mono.zip(
          Mono.just(user),
          getUserByAccount(user.getAccountId()),
          roomRepository.findById(roomId),
          roomService.getInternal(roomId),
          accessService.getRoomAccessInfo(roomId)
        )
      )
      .switchIfEmpty(
        Mono.error(
          new NotFoundException(
            makeErrorMessage(STAGE_0, "backend-error.not-found")
          )
        )
      )
      .filter(tuple -> !tuple.getT5().isRestricted())
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            makeErrorMessage(STAGE_0, "backend-error.gpt.room-restricted")
          )
        )
      )
      .filter(tuple ->
        tuple.getT2().getConsented() != null && tuple.getT2().getConsented()
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            makeErrorMessage(STAGE_0, "backend-error.gpt.not-consented")
          )
        )
      )
      .map(tuple -> {
        String apiKey = config.getApiKey();
        String apiOrganization = config.getOrganization();
        if (tuple.getT4().getApiKey() != null) {
          apiKey = tuple.getT4().getApiKey();
          apiOrganization = tuple.getT4().getApiOrganization();
        }
        return new ApiRequestInfo(
          tuple.getT1(),
          tuple.getT2(),
          tuple.getT3(),
          apiKey,
          apiOrganization,
          tuple.getT5().getGlobalInfo(),
          tuple.getT5(),
          m
        );
      });
  }
}
