package de.thm.arsnova.frag.jetzt.backend.model.command;

public class PatchBrainstorming extends WebSocketCommand<PatchBrainstormingPayload> {

    public PatchBrainstorming() {
        super(PatchBrainstorming.class.getSimpleName());
    }

    public PatchBrainstorming(PatchBrainstormingPayload payload) {
        super(PatchBrainstorming.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatchBrainstorming that = (PatchBrainstorming) o;
        return this.getPayload().equals(that.getPayload());
    }
}
