package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Bookmark;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.BookmarkRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class BookmarkService {

    private static final Logger logger = LoggerFactory.getLogger(BookmarkService.class);

    private final AuthorizationHelper authorizationHelper;
    private final BookmarkRepository repository;
    private final CommentService commentService;

    @Autowired
    public BookmarkService(
            AuthorizationHelper authorizationHelper,
            BookmarkRepository repository,
            CommentService commentService
    ) {
        this.authorizationHelper = authorizationHelper;
        this.repository = repository;
        this.commentService = commentService;
    }

    public Mono<Bookmark> getById(UUID id) {
        return this.repository.findById(id);
    }

    public Flux<Bookmark> getByRoomIdAndUser(UUID roomId) {
        return this.authorizationHelper.getCurrentUser()
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flux()
                .flatMap(user -> this.repository.findByAccountIdAndRoomId(user.getAccountId(), roomId));
    }

    public Mono<Bookmark> create(Bookmark bookmark) {
        logger.trace("Creating new Bookmark: (Comment): (" + bookmark.getCommentId() + ")");

        bookmark.setId(null);
        return Mono.zip(this.commentService.get(bookmark.getCommentId()), this.authorizationHelper.getCurrentUser())
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .map(tuple -> {
                    bookmark.setRoomId(tuple.getT1().getRoomId());
                    bookmark.setAccountId(tuple.getT2().getAccountId());
                    return bookmark;
                })
                .flatMap(repository::save);
    }

    public Mono<Void> delete(UUID id) {
        return Mono.zip(this.authorizationHelper.getCurrentUser(), this.repository.findById(id))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple -> tuple.getT2().getAccountId().equals(tuple.getT1().getAccountId()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(tuple -> this.repository.deleteById(tuple.getT2().getId()));
    }
}
