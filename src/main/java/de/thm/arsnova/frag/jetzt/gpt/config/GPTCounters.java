package de.thm.arsnova.frag.jetzt.gpt.config;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTMultiQuotaLimiter;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTMultiQuotaLimiter.MultiQuotaEntry;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTQuotaUnit;
import de.thm.arsnova.frag.jetzt.gpt.util.GPTQuotaType;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GPTCounters {

  private static final Logger logger = LoggerFactory.getLogger(
    GPTCounters.class
  );
  private static final ObjectMapper mapper = new ObjectMapper(
    new YAMLFactory()
  );
  private static final GPTCounters instance;

  public static GPTCounters getInstance() {
    return instance;
  }

  static {
    instance = loadCounters();
  }

  private static GPTCounters loadCounters() {
    File f = getFile();
    if (!f.exists()) {
      logger.info("Counters not present, using default.");
      return new GPTCounters();
    }
    try {
      return mapper.readValue(f, GPTCounters.class);
    } catch (IOException e) {
      logger.error("Failed to parse counters!", e);
      return new GPTCounters();
    }
  }

  private static File getFile() {
    return Paths.get("gpt_counters.yml").toAbsolutePath().toFile();
  }

  private final GPTMultiQuotaLimiter limiter;

  public GPTCounters() {
    ArrayList<MultiQuotaEntry> list = new ArrayList<>();
    list.add(
      new MultiQuotaEntry(GPTQuotaType.DAILY, null, new GPTQuotaUnit(0, 8))
    );
    list.add(
      new MultiQuotaEntry(GPTQuotaType.WEEKLY, null, new GPTQuotaUnit(0, 8))
    );
    list.add(
      new MultiQuotaEntry(GPTQuotaType.MONTHLY, null, new GPTQuotaUnit(0, 8))
    );
    list.add(
      new MultiQuotaEntry(GPTQuotaType.GLOBAL, null, new GPTQuotaUnit(0, 8))
    );
    limiter =
      new GPTMultiQuotaLimiter(
        LocalDateTime.now(),
        Collections.unmodifiableList(list)
      );
  }

  @JsonCreator
  public GPTCounters(
    @JsonProperty("lastUpdate") Timestamp lastUpdate,
    @JsonProperty("dailyCounter") GPTQuotaUnit dailyCounter,
    @JsonProperty("weeklyCounter") GPTQuotaUnit weeklyCounter,
    @JsonProperty("monthlyCounter") GPTQuotaUnit monthlyCounter,
    @JsonProperty("accumulatedCounter") GPTQuotaUnit accumulatedCounter
  ) {
    ArrayList<MultiQuotaEntry> list = new ArrayList<>();
    list.add(new MultiQuotaEntry(GPTQuotaType.DAILY, null, dailyCounter));
    list.add(new MultiQuotaEntry(GPTQuotaType.WEEKLY, null, weeklyCounter));
    list.add(new MultiQuotaEntry(GPTQuotaType.MONTHLY, null, monthlyCounter));
    list.add(
      new MultiQuotaEntry(GPTQuotaType.GLOBAL, null, accumulatedCounter)
    );
    limiter =
      new GPTMultiQuotaLimiter(
        lastUpdate.toLocalDateTime(),
        Collections.unmodifiableList(list)
      );
  }

  public Timestamp getLastUpdate() {
    return Timestamp.valueOf(limiter.getLastUpdate());
  }

  public GPTQuotaUnit getDailyCounter() {
    return limiter.getLimits().get(0).getCounter();
  }

  public GPTQuotaUnit getWeeklyCounter() {
    return limiter.getLimits().get(1).getCounter();
  }

  public GPTQuotaUnit getMonthlyCounter() {
    return limiter.getLimits().get(2).getCounter();
  }

  public GPTQuotaUnit getAccumulatedCounter() {
    return limiter.getLimits().get(3).getCounter();
  }

  @JsonIgnore
  public GPTMultiQuotaLimiter getLimiter() {
    return limiter;
  }

  @JsonIgnore
  public void save() {
    try {
      mapper.writeValue(getFile(), this);
    } catch (IOException e) {
      logger.error("Could not save counters!", e);
    }
  }
}
