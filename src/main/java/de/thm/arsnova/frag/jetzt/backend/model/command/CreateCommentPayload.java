package de.thm.arsnova.frag.jetzt.backend.model.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;
import java.util.UUID;

public class CreateCommentPayload implements WebSocketPayload {

  private UUID roomId;
  private UUID creatorId;
  private String body;
  private String tag;
  private String keywordsFromSpacy;
  private String keywordsFromQuestioner;
  private String questionerName;
  private Comment.Language language;
  private UUID brainstormingSessionId;
  private UUID brainstormingWordId;
  private UUID commentReference;
  private boolean approved;
  private int gptWriterState;

  public CreateCommentPayload() {}

  public CreateCommentPayload(Comment c) {
    this.creatorId = c.getCreatorId();
    this.roomId = c.getRoomId();
    this.body = c.getBody();
    this.tag = c.getTag();
    this.keywordsFromSpacy = c.getKeywordsFromSpacy();
    this.keywordsFromQuestioner = c.getKeywordsFromQuestioner();
    this.language = c.getLanguage();
    this.questionerName = c.getQuestionerName();
    this.brainstormingSessionId = c.getBrainstormingSessionId();
    this.brainstormingWordId = c.getBrainstormingWordId();
    this.commentReference = c.getCommentReference();
    this.approved = c.isApproved();
    this.gptWriterState = c.getGptWriterState();
  }

  @JsonProperty("roomId")
  public UUID getRoomId() {
    return roomId;
  }

  @JsonProperty("roomId")
  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  @JsonProperty("creatorId")
  public UUID getCreatorId() {
    return creatorId;
  }

  @JsonProperty("creatorId")
  public void setCreatorId(UUID creatorId) {
    this.creatorId = creatorId;
  }

  @JsonProperty("body")
  public String getBody() {
    return body;
  }

  @JsonProperty("body")
  public void setBody(String body) {
    this.body = body;
  }

  @JsonProperty("tag")
  public String getTag() {
    return tag;
  }

  @JsonProperty("tag")
  public void setTag(String tag) {
    this.tag = tag;
  }

  @JsonProperty("keywordsFromSpacy")
  public String getKeywordsFromSpacy() {
    return keywordsFromSpacy;
  }

  @JsonProperty("keywordsFromSpacy")
  public void setKeywordsFromSpacy(String keywordsFromSpacy) {
    this.keywordsFromSpacy = keywordsFromSpacy;
  }

  @JsonProperty("keywordsFromQuestioner")
  public String getKeywordsFromQuestioner() {
    return keywordsFromQuestioner;
  }

  @JsonProperty("keywordsFromQuestioner")
  public void setKeywordsFromQuestioner(String keywordsFromQuestioner) {
    this.keywordsFromQuestioner = keywordsFromQuestioner;
  }

  @JsonProperty("language")
  public Comment.Language getLanguage() {
    return language;
  }

  @JsonProperty("language")
  public void setLanguage(Comment.Language language) {
    this.language = language;
  }

  @JsonProperty("questionerName")
  public String getQuestionerName() {
    return questionerName;
  }

  @JsonProperty("questionerName")
  public void setQuestionerName(String questionerName) {
    this.questionerName = questionerName;
  }

  public UUID getBrainstormingSessionId() {
    return brainstormingSessionId;
  }

  public void setBrainstormingSessionId(UUID brainstormingSessionId) {
    this.brainstormingSessionId = brainstormingSessionId;
  }

  public UUID getBrainstormingWordId() {
    return brainstormingWordId;
  }

  public void setBrainstormingWordId(UUID brainstormingWordId) {
    this.brainstormingWordId = brainstormingWordId;
  }

  @JsonProperty("commentReference")
  public UUID getCommentReference() {
    return commentReference;
  }

  @JsonProperty("commentReference")
  public void setCommentReference(UUID commentReference) {
    this.commentReference = commentReference;
  }

  public boolean isApproved() {
    return approved;
  }

  public void setApproved(boolean approved) {
    this.approved = approved;
  }

  public int getGptWriterState() {
    return gptWriterState;
  }

  public void setGptWriterState(int gptWriterState) {
    this.gptWriterState = gptWriterState;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result = prime * result + ((creatorId == null) ? 0 : creatorId.hashCode());
    result = prime * result + ((body == null) ? 0 : body.hashCode());
    result = prime * result + ((tag == null) ? 0 : tag.hashCode());
    result =
      prime *
      result +
      ((keywordsFromSpacy == null) ? 0 : keywordsFromSpacy.hashCode());
    result =
      prime *
      result +
      (
        (keywordsFromQuestioner == null) ? 0 : keywordsFromQuestioner.hashCode()
      );
    result =
      prime *
      result +
      ((questionerName == null) ? 0 : questionerName.hashCode());
    result = prime * result + ((language == null) ? 0 : language.hashCode());
    result =
      prime *
      result +
      (
        (brainstormingSessionId == null) ? 0 : brainstormingSessionId.hashCode()
      );
    result =
      prime *
      result +
      ((brainstormingWordId == null) ? 0 : brainstormingWordId.hashCode());
    result =
      prime *
      result +
      ((commentReference == null) ? 0 : commentReference.hashCode());
    result = prime * result + (approved ? 1231 : 1237);
    result = prime * result + gptWriterState;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    CreateCommentPayload other = (CreateCommentPayload) obj;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (creatorId == null) {
      if (other.creatorId != null) return false;
    } else if (!creatorId.equals(other.creatorId)) return false;
    if (body == null) {
      if (other.body != null) return false;
    } else if (!body.equals(other.body)) return false;
    if (tag == null) {
      if (other.tag != null) return false;
    } else if (!tag.equals(other.tag)) return false;
    if (keywordsFromSpacy == null) {
      if (other.keywordsFromSpacy != null) return false;
    } else if (!keywordsFromSpacy.equals(other.keywordsFromSpacy)) return false;
    if (keywordsFromQuestioner == null) {
      if (other.keywordsFromQuestioner != null) return false;
    } else if (
      !keywordsFromQuestioner.equals(other.keywordsFromQuestioner)
    ) return false;
    if (questionerName == null) {
      if (other.questionerName != null) return false;
    } else if (!questionerName.equals(other.questionerName)) return false;
    if (language != other.language) return false;
    if (brainstormingSessionId == null) {
      if (other.brainstormingSessionId != null) return false;
    } else if (
      !brainstormingSessionId.equals(other.brainstormingSessionId)
    ) return false;
    if (brainstormingWordId == null) {
      if (other.brainstormingWordId != null) return false;
    } else if (
      !brainstormingWordId.equals(other.brainstormingWordId)
    ) return false;
    if (commentReference == null) {
      if (other.commentReference != null) return false;
    } else if (!commentReference.equals(other.commentReference)) return false;
    if (approved != other.approved) return false;
    if (gptWriterState != other.gptWriterState) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "CreateCommentPayload [roomId=" +
      roomId +
      ", creatorId=" +
      creatorId +
      ", body=" +
      body +
      ", tag=" +
      tag +
      ", keywordsFromSpacy=" +
      keywordsFromSpacy +
      ", keywordsFromQuestioner=" +
      keywordsFromQuestioner +
      ", questionerName=" +
      questionerName +
      ", language=" +
      language +
      ", brainstormingSessionId=" +
      brainstormingSessionId +
      ", brainstormingWordId=" +
      brainstormingWordId +
      ", commentReference=" +
      commentReference +
      ", approved=" +
      approved +
      ", gptWriterState=" +
      gptWriterState +
      "]"
    );
  }
}
