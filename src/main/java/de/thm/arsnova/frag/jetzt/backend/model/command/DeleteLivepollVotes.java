package de.thm.arsnova.frag.jetzt.backend.model.command;

public class DeleteLivepollVotes
  extends WebSocketCommand<DeleteLivepollVotesPayload> {

  public DeleteLivepollVotes() {
    super(DeleteLivepollVotes.class.getSimpleName());
  }

  public DeleteLivepollVotes(DeleteLivepollVotesPayload p) {
    super(DeleteLivepollVotes.class.getSimpleName());
    this.payload = p;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DeleteLivepollVotes that = (DeleteLivepollVotes) o;
    return this.getPayload().equals(that.getPayload());
  }
}
