package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.CommentNotification;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Repository
public interface CommentNotificationRepository extends ReactiveCrudRepository<CommentNotification, UUID> {

    Flux<CommentNotification> findByNotificationSetting(short notificationSetting);

    Flux<CommentNotification> findByAccountIdAndRoomIdOrderByNotificationSettingAsc(UUID accountId, UUID roomId);

}
