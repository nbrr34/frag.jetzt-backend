package de.thm.arsnova.frag.jetzt.gpt.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class GPTStatistics {

  public static class GPTStatistic {

    private List<Double> percentiles;
    private double upperQuartile;
    private double lowerQuartile;
    private double lowerWhisker = 0;
    private double upperWhisker = 0;
    private long strayBulletsLower = 0;
    private long strayBulletsUpper = 0;
    private long count = 0;

    public static GPTStatistic buildByData(List<Double> sortedData) {
      GPTStatistic statistic = new GPTStatistic();
      statistic.count = sortedData.size();
      statistic.percentiles = new ArrayList<>(11);
      for (int i = 0; i <= 10; i++) {
        statistic.percentiles.add(getPercentile(sortedData, i / 10.0));
      }
      statistic.upperQuartile = getPercentile(sortedData, 0.75);
      statistic.lowerQuartile = getPercentile(sortedData, 0.25);
      if (sortedData.size() < 1) {
        return statistic;
      }
      final double iqr =
        (statistic.upperQuartile - statistic.lowerQuartile) * 1.5;
      final double lowerBorder = statistic.lowerQuartile - iqr;
      int i = 0;
      for (; i < sortedData.size(); i++) {
        double d = sortedData.get(i);
        if (d < lowerBorder) {
          statistic.strayBulletsLower++;
        } else {
          break;
        }
      }
      statistic.lowerWhisker = sortedData.get(i);
      final double upperBorder = statistic.upperQuartile + iqr;
      i = sortedData.size() - 1;
      for (; i >= 0; i--) {
        double d = sortedData.get(i);
        if (d > upperBorder) {
          statistic.strayBulletsUpper++;
        } else {
          break;
        }
      }
      statistic.upperWhisker = sortedData.get(i);
      return statistic;
    }

    private static double getPercentile(List<Double> sortedData, double value) {
      if (sortedData.size() < 1) {
        return 0;
      }
      final double d = sortedData.size() * value;
      final int i = (int) d;
      if (i + 1 < sortedData.size() && Math.abs(d - i) < Double.MIN_NORMAL) {
        return (sortedData.get(i) + sortedData.get(i + 1)) / 2;
      }
      if (i >= sortedData.size()) {
        return sortedData.get(i - 1);
      }
      return sortedData.get(i);
    }

    public List<Double> getPercentiles() {
      return percentiles;
    }

    public void setPercentiles(List<Double> percentiles) {
      this.percentiles = percentiles;
    }

    public double getUpperQuartile() {
      return upperQuartile;
    }

    public void setUpperQuartile(double upperQuartile) {
      this.upperQuartile = upperQuartile;
    }

    public double getLowerQuartile() {
      return lowerQuartile;
    }

    public void setLowerQuartile(double lowerQuartile) {
      this.lowerQuartile = lowerQuartile;
    }

    public double getLowerWhisker() {
      return lowerWhisker;
    }

    public void setLowerWhisker(double lowerWhisker) {
      this.lowerWhisker = lowerWhisker;
    }

    public double getUpperWhisker() {
      return upperWhisker;
    }

    public void setUpperWhisker(double upperWhisker) {
      this.upperWhisker = upperWhisker;
    }

    public long getStrayBulletsLower() {
      return strayBulletsLower;
    }

    public void setStrayBulletsLower(long strayBulletsLower) {
      this.strayBulletsLower = strayBulletsLower;
    }

    public long getStrayBulletsUpper() {
      return strayBulletsUpper;
    }

    public void setStrayBulletsUpper(long strayBulletsUpper) {
      this.strayBulletsUpper = strayBulletsUpper;
    }

    public long getCount() {
      return count;
    }

    public void setCount(long count) {
      this.count = count;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result =
        prime * result + ((percentiles == null) ? 0 : percentiles.hashCode());
      long temp;
      temp = Double.doubleToLongBits(upperQuartile);
      result = prime * result + (int) (temp ^ (temp >>> 32));
      temp = Double.doubleToLongBits(lowerQuartile);
      result = prime * result + (int) (temp ^ (temp >>> 32));
      temp = Double.doubleToLongBits(lowerWhisker);
      result = prime * result + (int) (temp ^ (temp >>> 32));
      temp = Double.doubleToLongBits(upperWhisker);
      result = prime * result + (int) (temp ^ (temp >>> 32));
      result =
        prime * result + (int) (strayBulletsLower ^ (strayBulletsLower >>> 32));
      result =
        prime * result + (int) (strayBulletsUpper ^ (strayBulletsUpper >>> 32));
      result = prime * result + (int) (count ^ (count >>> 32));
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) return true;
      if (obj == null) return false;
      if (getClass() != obj.getClass()) return false;
      GPTStatistic other = (GPTStatistic) obj;
      if (percentiles == null) {
        if (other.percentiles != null) return false;
      } else if (!percentiles.equals(other.percentiles)) return false;
      if (
        Double.doubleToLongBits(upperQuartile) !=
        Double.doubleToLongBits(other.upperQuartile)
      ) return false;
      if (
        Double.doubleToLongBits(lowerQuartile) !=
        Double.doubleToLongBits(other.lowerQuartile)
      ) return false;
      if (
        Double.doubleToLongBits(lowerWhisker) !=
        Double.doubleToLongBits(other.lowerWhisker)
      ) return false;
      if (
        Double.doubleToLongBits(upperWhisker) !=
        Double.doubleToLongBits(other.upperWhisker)
      ) return false;
      if (strayBulletsLower != other.strayBulletsLower) return false;
      if (strayBulletsUpper != other.strayBulletsUpper) return false;
      if (count != other.count) return false;
      return true;
    }

    @Override
    public String toString() {
      return (
        "GPTStatistic [percentiles=" +
        percentiles +
        ", upperQuartile=" +
        upperQuartile +
        ", lowerQuartile=" +
        lowerQuartile +
        ", lowerWhisker=" +
        lowerWhisker +
        ", upperWhisker=" +
        upperWhisker +
        ", strayBulletsLower=" +
        strayBulletsLower +
        ", strayBulletsUpper=" +
        strayBulletsUpper +
        ", count=" +
        count +
        "]"
      );
    }
  }

  private GPTQuotaUnit dailyCounter;
  private GPTQuotaUnit weeklyCounter;
  private GPTQuotaUnit monthlyCounter;
  private GPTQuotaUnit accumulatedCounter;
  private Timestamp lastUpdate;

  public GPTStatistics(
    Timestamp lastUpdate,
    GPTQuotaUnit dailyCounter,
    GPTQuotaUnit weeklyCounter,
    GPTQuotaUnit monthlyCounter,
    GPTQuotaUnit accumulatedCounter
  ) {
    this.lastUpdate = lastUpdate;
    this.dailyCounter = dailyCounter;
    this.weeklyCounter = weeklyCounter;
    this.monthlyCounter = monthlyCounter;
    this.accumulatedCounter = accumulatedCounter;
  }

  public GPTQuotaUnit getDailyCounter() {
    return dailyCounter;
  }

  public void setDailyCounter(GPTQuotaUnit dailyCounter) {
    this.dailyCounter = dailyCounter;
  }

  public GPTQuotaUnit getWeeklyCounter() {
    return weeklyCounter;
  }

  public void setWeeklyCounter(GPTQuotaUnit weeklyCounter) {
    this.weeklyCounter = weeklyCounter;
  }

  public GPTQuotaUnit getMonthlyCounter() {
    return monthlyCounter;
  }

  public void setMonthlyCounter(GPTQuotaUnit monthlyCounter) {
    this.monthlyCounter = monthlyCounter;
  }

  public GPTQuotaUnit getAccumulatedCounter() {
    return accumulatedCounter;
  }

  public void setAccumulatedCounter(GPTQuotaUnit accumulatedCounter) {
    this.accumulatedCounter = accumulatedCounter;
  }

  public Timestamp getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(Timestamp lastUpdate) {
    this.lastUpdate = lastUpdate;
  }
}
