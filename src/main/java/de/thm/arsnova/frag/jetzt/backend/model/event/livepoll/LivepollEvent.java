package de.thm.arsnova.frag.jetzt.backend.model.event.livepoll;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketMessage;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;
import java.util.UUID;

public class LivepollEvent<P extends WebSocketPayload>
  extends WebSocketMessage<P> {

  protected UUID livepollId;

  public LivepollEvent(String type) {
    super(type);
  }

  public LivepollEvent(String type, UUID livepollId) {
    super(type);
    this.livepollId = livepollId;
  }

  @JsonProperty("type")
  public String getType() {
    return type;
  }

  @Override
  @JsonProperty("type")
  public void setType(String type) {
    this.type = type;
  }

  public UUID getLivepollId() {
    return livepollId;
  }

  public void setLivepollId(UUID livepollId) {
    this.livepollId = livepollId;
  }

  @Override
  public String toString() {
    return (
      "LivepollEvent [livepollId=" +
      livepollId +
      ", type=" +
      type +
      ", payload=" +
      payload +
      "]"
    );
  }
}
