package de.thm.arsnova.frag.jetzt.gpt.model;

import de.thm.arsnova.frag.jetzt.gpt.util.GPTQuotaType;
import java.time.LocalDateTime;
import java.util.List;

public class GPTMultiQuotaLimiter {

  public static class MultiQuotaEntry {

    /**
     * <strong>null</strong> means no limit set.
     * <em>Negative</em> values are not allowed and <em>zero</em> means no quota and forbidden.
     */
    private final GPTQuotaUnit limit;
    private final GPTQuotaType type;
    private GPTQuotaUnit counter;

    public MultiQuotaEntry(
      GPTQuotaType type,
      GPTQuotaUnit limit,
      GPTQuotaUnit counter
    ) {
      this.limit = limit;
      this.type = type;
      this.counter = counter;
    }

    public GPTQuotaUnit getLimit() {
      return limit;
    }

    public GPTQuotaType getType() {
      return type;
    }

    public GPTQuotaUnit getCounter() {
      return counter;
    }

    public void setCounter(GPTQuotaUnit counter) {
      this.counter = counter;
    }
  }

  private LocalDateTime lastUpdate;
  private final List<MultiQuotaEntry> limits;

  public GPTMultiQuotaLimiter(
    LocalDateTime lastUpdate,
    List<MultiQuotaEntry> limits
  ) {
    this.lastUpdate = lastUpdate;
    this.limits = limits;
  }

  public LocalDateTime getLastUpdate() {
    return lastUpdate;
  }

  public List<MultiQuotaEntry> getLimits() {
    return limits;
  }

  public synchronized long getRemaining() {
    long remaining = Long.MAX_VALUE;
    boolean hadRestriction = false;
    final LocalDateTime current = LocalDateTime.now();
    for (MultiQuotaEntry entry : limits) {
      if (entry.limit == null || entry.limit.getValue() == null) {
        continue;
      }
      hadRestriction = true;
      if (entry.type.hasChanged(lastUpdate, current)) {
        entry.counter = entry.counter.zero();
      }
      final long counter = entry.counter.toPlain(8);
      final long limit = entry.limit.toPlain(8);
      if (limit <= counter) {
        remaining = 0;
        break;
      }
      remaining = Math.min(remaining, limit - counter);
    }
    lastUpdate = current;
    return hadRestriction ? remaining : -1;
  }

  public synchronized boolean tryReserve(GPTQuotaUnit amount) {
    final long remaining = getRemaining();
    if (remaining >= 0 && remaining < amount.toPlain(8)) {
      return false;
    }
    limits.forEach(elem -> elem.counter = elem.counter.add(amount));
    return true;
  }

  public synchronized void finishReservation(
    GPTQuotaUnit reservedAmount,
    GPTQuotaUnit actualAmount
  ) {
    final LocalDateTime current = LocalDateTime.now();
    limits.forEach(elem -> {
      elem.counter =
        elem.type.hasChanged(lastUpdate, current)
          ? new GPTQuotaUnit(actualAmount.toPlain(8), 8)
          : elem.counter.minus(
            new GPTQuotaUnit(
              reservedAmount.toPlain(8) - actualAmount.toPlain(8),
              8
            )
          );
    });
    lastUpdate = current;
  }
}
