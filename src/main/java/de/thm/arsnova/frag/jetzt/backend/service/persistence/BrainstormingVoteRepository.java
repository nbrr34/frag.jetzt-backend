package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingVote;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface BrainstormingVoteRepository extends ReactiveCrudRepository<BrainstormingVote, UUID> {

    Mono<Void> deleteByAccountIdAndWordId(UUID accountId, UUID wordId);

    Flux<Void> deleteByWordId(UUID wordId);

    Mono<BrainstormingVote> findByAccountIdAndWordId(UUID accountId, UUID wordId);

}
