package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.service.AccountService;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTGlobalAccessInfo;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTRoomAccessInfo;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting.GPTRoomSettingRight;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomUsageTime;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTUserRepository;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class GPTAccessService {

  private final AuthorizationHelper helper;
  private final GPTConfig config;
  private final GPTUserRepository repository;
  private final GPTRoomService roomService;
  private final RoomRepository roomRepository;
  private final AccountService accountService;

  @Autowired
  public GPTAccessService(
    AuthorizationHelper helper,
    GPTConfig config,
    GPTUserRepository repository,
    GPTRoomService roomService,
    RoomRepository roomRepository,
    AccountService accountService
  ) {
    this.helper = helper;
    this.config = config;
    this.repository = repository;
    this.roomService = roomService;
    this.roomRepository = roomRepository;
    this.accountService = accountService;
  }

  public Mono<GPTGlobalAccessInfo> getGlobalAccessInfo() {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        Mono.zip(
          Mono.just(user),
          repository.findByAccountId(user.getAccountId())
        )
      )
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .map(tuple -> {
        return new GPTGlobalAccessInfo(
          tuple.getT2().isRequestForbidden(),
          tuple.getT1().getName() != null &&
          tuple.getT1().getName().length() > 1,
          config.getApiKey() != null,
          config.getRestrictions().isActive(),
          config.getRestrictions().isGlobalActive(),
          config.getRestrictions().getEndDate() != null &&
          config
            .getRestrictions()
            .getEndDate()
            .before(Timestamp.from(Instant.now())),
          tuple.getT2().getConsented() != null && tuple.getT2().getConsented()
        );
      });
  }

  public Mono<GPTRoomAccessInfo> getRoomAccessInfo(UUID roomId) {
    return helper
      .getCurrentUser()
      .flatMap(user ->
        Mono.zip(
          getGlobalAccessInfo(),
          Mono.just(user),
          roomRepository
            .findById(roomId)
            .flatMap(room ->
              Mono.zip(
                Mono.just(room),
                accountService
                  .get(room.getOwnerId())
                  .map(owner ->
                    owner.getEmail() != null && !owner.getEmail().isEmpty()
                  )
              )
            ),
          roomService.getInternal(roomId)
        )
      )
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .map(tuple -> {
        GPTRoomSetting setting = tuple.getT4();
        return new GPTRoomAccessInfo(
          tuple.getT1(),
          tuple.getT3().getT2(),
          setting.getApiKey() != null,
          setting.isTrialEnabled(),
          isRoomZero(setting),
          isModeratorZero(setting),
          isParticipantZero(setting),
          isUsageTimeOver(setting),
          helper.checkOnlyModeratorOfRoom(tuple.getT2(), tuple.getT3().getT1()),
          helper.checkCreatorOfRoom(tuple.getT2(), tuple.getT3().getT1()),
          GPTRoomSettingRight.ALLOW_UNREGISTERED_USERS.isRightSet(
            setting.getRightsBitset()
          )
        );
      });
  }

  private boolean isRoomZero(GPTRoomSetting setting) {
    return (
      (
        setting.getMaxAccumulatedRoomCost() != null &&
        setting.getMaxAccumulatedRoomCost() < 1
      ) ||
      (
        setting.getMaxMonthlyRoomCost() != null &&
        setting.getMaxMonthlyRoomCost() < 1
      ) ||
      (
        setting.getMaxDailyRoomCost() != null &&
        setting.getMaxDailyRoomCost() < 1
      )
    );
  }

  public boolean isModeratorZero(GPTRoomSetting setting) {
    return (
      (
        setting.getMaxAccumulatedModeratorCost() != null &&
        setting.getMaxAccumulatedModeratorCost() < 1
      ) ||
      (
        setting.getMaxMonthlyModeratorCost() != null &&
        setting.getMaxMonthlyModeratorCost() < 1
      ) ||
      (
        setting.getMaxDailyModeratorCost() != null &&
        setting.getMaxDailyModeratorCost() < 1
      )
    );
  }

  public boolean isParticipantZero(GPTRoomSetting setting) {
    return (
      (
        setting.getMaxAccumulatedParticipantCost() != null &&
        setting.getMaxAccumulatedParticipantCost() < 1
      ) ||
      (
        setting.getMaxMonthlyParticipantCost() != null &&
        setting.getMaxMonthlyParticipantCost() < 1
      ) ||
      (
        setting.getMaxDailyParticipantCost() != null &&
        setting.getMaxDailyParticipantCost() < 1
      )
    );
  }

  private boolean isUsageTimeOver(GPTRoomSetting setting) {
    if (setting.getUsageTimes().isEmpty()) {
      return false;
    }
    final Timestamp current = Timestamp.from(Instant.now());
    boolean restricted = true;
    for (GPTRoomUsageTime time : setting.getUsageTimes()) {
      if (time.getRepeatDuration() == null || time.getRepeatUnit() == null) {
        if (
          time.getStartDate().compareTo(current) <= 0 &&
          time.getEndDate().compareTo(current) >= 0
        ) {
          restricted = false;
          break;
        }
        continue;
      }
      final long start = time
        .getRepeatUnit()
        .toOffset(time.getRepeatDuration(), time.getStartDate());
      final long end = time
        .getRepeatUnit()
        .toOffset(time.getRepeatDuration(), time.getEndDate());
      final long curr = time
        .getRepeatUnit()
        .toOffset(time.getRepeatDuration(), current);
      if (start > end) {
        if (curr >= start || curr <= end) {
          restricted = false;
          break;
        }
        continue;
      }
      if (curr >= start && curr <= end) {
        restricted = false;
        break;
      }
    }
    return restricted;
  }
}
