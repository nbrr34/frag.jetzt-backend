package de.thm.arsnova.frag.jetzt.backend.model.command;

public class CreateBrainstorming extends WebSocketCommand<CreateBrainstormingPayload> {

    public CreateBrainstorming(){
        super(CreateBrainstorming.class.getSimpleName());
    }

    public CreateBrainstorming(CreateBrainstormingPayload payload){
        super(CreateBrainstorming.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateBrainstorming that = (CreateBrainstorming) o;
        return this.getPayload().equals(that.getPayload());
    }
}
