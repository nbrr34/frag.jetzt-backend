package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingCategory;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession.BrainstormingWordWithMeta;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingWord;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.*;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

@Service
public class BrainstormingSessionService {

    @Value("${app.brainstorming.titleLength}")
    private int titleLength;
    @Value("${app.brainstorming.minWordCount}")
    private int minWordCount;
    @Value("${app.brainstorming.maxWordCount}")
    private int maxWordCount;
    @Value("${app.brainstorming.minWordLength}")
    private int minWordLength;
    @Value("${app.brainstorming.maxWordLength}")
    private int maxWordLength;

    private static final Logger logger = LoggerFactory.getLogger(BrainstormingSessionService.class);

    private final AuthorizationHelper authorizationHelper;
    private final RoomRepository roomRepository;
    private final BrainstormingSessionRepository repository;
    private final BrainstormingVoteRepository voteRepository;
    private final BrainstormingWordRepository wordRepository;
    private final BrainstormingCategoryRepository categoryRepository;

    @Autowired
    public BrainstormingSessionService(
            AuthorizationHelper authorizationHelper,
            RoomRepository roomRepository,
            BrainstormingSessionRepository repository,
            BrainstormingVoteRepository voteRepository,
            BrainstormingWordRepository wordRepository,
            BrainstormingCategoryRepository categoryRepository
    ) {
        this.authorizationHelper = authorizationHelper;
        this.roomRepository = roomRepository;
        this.repository = repository;
        this.voteRepository = voteRepository;
        this.wordRepository = wordRepository;
        this.categoryRepository = categoryRepository;
    }

    public Mono<BrainstormingSession> get(UUID id) {
        return this.repository.findById(id)
                .flatMap(this::applyVotes);
    }

    public Flux<BrainstormingSession> findByRoomId(UUID roomId) {
        return this.repository.findByRoomId(roomId)
                .flatMap(this::applyVotes);
    }

    public Mono<BrainstormingSession> getActiveSessionByRoomId(UUID roomId) {
        return this.repository.findByRoomId(roomId)
                .filter(BrainstormingSession::isActive)
                .collectList()
                .flatMap(list -> {
                    if (list.size() < 1) return Mono.empty();
                    return Mono.just(list.get(0));
                })
                .flatMap(this::applyVotes);
    }

    public Flux<BrainstormingCategory> getCategories(UUID roomId) {
        return categoryRepository.findByRoomId(roomId);
    }

    public Flux<BrainstormingCategory> updateCategories(UUID roomId, List<String> categories) {
        return Mono.zip(authorizationHelper.getCurrentUser(), roomRepository.findById(roomId))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple2 -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple2.getT1(), tuple2.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(tuple2 -> getCategories(roomId).collectList())
                .flatMapMany(oldCategories -> Flux.fromStream(oldCategories.stream().filter(c -> {
                            Iterator<String> categoryIterator = categories.iterator();
                            while (categoryIterator.hasNext()) {
                                if (categoryIterator.next().equals(c.getName())) {
                                    categoryIterator.remove();
                                    return false;
                                }
                            }
                            return true;
                        }))
                        .flatMap(c -> categoryRepository.deleteById(c.getId()))
                        .then(Mono.just(categories))
                        .flatMapMany(Flux::fromIterable)
                        .map(category -> {
                            BrainstormingCategory c = new BrainstormingCategory();
                            c.setName(category);
                            c.setRoomId(roomId);
                            return c;
                        })
                        .flatMap(categoryRepository::save)
                        .then(Mono.just(roomId))
                        .flatMapMany(categoryRepository::findByRoomId));


    }

    public Mono<Tuple2<BrainstormingWord, Boolean>> createWord(UUID sessionId, String word, String realWord) {
        final String brainWord = word.toLowerCase(Locale.ROOT);
        return repository.findById(sessionId)
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(this::checkCanCreateIdea)
                .flatMap(r -> wordRepository.findBySessionIdAndWord(sessionId, brainWord))
                .switchIfEmpty(Mono.just(new BrainstormingWord()))
                .flatMap(sessionWord -> {
                    if (!sessionWord.isNew()) {
                        return Mono.just(Tuples.of(sessionWord, true));
                    }
                    sessionWord.setWord(brainWord);
                    sessionWord.setSessionId(sessionId);
                    sessionWord.setCorrectedWord(realWord);
                    return this.validateWord(sessionWord)
                            .flatMap(wordRepository::save)
                            .map(sWord -> Tuples.of(sWord, false));
                });
    }

    public Mono<Tuple2<UUID, BrainstormingWord>> patchWord(BrainstormingWord entity, Map<String, Object> changes) {
        return Mono.zip(
                        authorizationHelper.getCurrentUser(),
                        repository.findById(entity.getSessionId())
                                .flatMap(session -> roomRepository.findById(session.getRoomId()))
                )
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple2 -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple2.getT1(), tuple2.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(tuple2 -> Mono.zip(
                        Mono.just(tuple2.getT2().getId()),
                        this.validateWord(this.parseWordChanges(entity, changes))
                                .flatMap(wordRepository::save)
                ));
    }

    public Mono<BrainstormingSession> patch(BrainstormingSession entity, Map<String, Object> changes) {
        return Mono.zip(authorizationHelper.getCurrentUser(), roomRepository.findById(entity.getRoomId()))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple2 -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple2.getT1(), tuple2.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .map(tuple2 -> this.parseSessionChanges(entity, changes))
                .flatMap(this::validateSession)
                .flatMap(repository::save)
                .flatMap(c -> this.get(c.getId()));
    }

    public Mono<BrainstormingSession> create(BrainstormingSession session) {
        logger.trace("Creating new Session: " + session.toString());

        session.setId(null);
        session.setActive(true);
        return Mono.zip(authorizationHelper.getCurrentUser(), this.roomRepository.findById(session.getRoomId()))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple.getT1(), tuple.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(tuple -> getActiveSessionByRoomId(tuple.getT2().getId()))
                .switchIfEmpty(Mono.just(new BrainstormingSession()))
                .flatMap(activeSession -> {
                    if (!activeSession.isNew()) {
                        return Mono.error(new ForbiddenException("There is already a active session!"));
                    }
                    return Mono.just(activeSession);
                })
                .flatMap(c -> this.validateSession(session))
                .flatMap(repository::save);
    }

    public Mono<Void> deleteByIdAndRoomId(UUID sessionId, UUID roomId) {
        return Mono.zip(authorizationHelper.getCurrentUser(), this.roomRepository.findById(roomId))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple.getT1(), tuple.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(c -> repository.deleteById(sessionId));
    }

    public Mono<BrainstormingSession> checkCanCreateRating(BrainstormingSession session) {
        return Mono.zip(
                        Mono.just(session),
                        authorizationHelper.getCurrentUser(),
                        roomRepository.findById(session.getRoomId())
                )
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple3 -> tuple3.getT1().isActive() && (
                        authorizationHelper.checkModeratorOrCreatorOfRoom(tuple3.getT2(), tuple3.getT3()) ||
                                tuple3.getT1().isRatingAllowed()
                ))
                .switchIfEmpty(Mono.error(new BadRequestException("Can not create rating when session has not enabled votes or closed!")))
                .map(Tuple2::getT1);
    }

    private Mono<BrainstormingSession> checkCanCreateIdea(BrainstormingSession session) {
        return Mono.zip(
                        Mono.just(session),
                        authorizationHelper.getCurrentUser(),
                        roomRepository.findById(session.getRoomId())
                )
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .filter(tuple3 -> tuple3.getT1().isActive() && (
                        authorizationHelper.checkModeratorOrCreatorOfRoom(tuple3.getT2(), tuple3.getT3()) ||
                                !tuple3.getT1().isIdeasFrozen()
                ))
                .switchIfEmpty(Mono.error(new BadRequestException("Can not create word when session has frozen ideas or closed!")))
                .map(Tuple2::getT1);
    }

    private BrainstormingSession parseSessionChanges(BrainstormingSession entity, Map<String, Object> changes) {
        changes.forEach((key, value) -> {
            switch (key) {
                case "title":
                    entity.setTitle((String) value);
                    break;
                case "active":
                    entity.setActive((Boolean) value);
                    break;
                case "maxWordLength":
                    entity.setMaxWordLength((Integer) value);
                    break;
                case "maxWordCount":
                    entity.setMaxWordCount((Integer) value);
                    break;
                case "language":
                    entity.setLanguage((String) value);
                    break;
                case "ratingAllowed":
                    entity.setRatingAllowed((Boolean) value);
                    break;
                case "ideasFrozen":
                    entity.setIdeasFrozen((Boolean) value);
                    break;
                case "ideasTimeDuration":
                    entity.setIdeasTimeDuration((Integer) value);
                    break;
                case "ideasEndTimestamp":
                    if (value == null) {
                        entity.setIdeasEndTimestamp(null);
                    } else {
                        entity.setIdeasEndTimestamp(new Timestamp(((Number) value).longValue()));
                    }
                    break;
                case "id":
                case "roomId":
                    throw new ForbiddenException("You are not allowed to change the " + key);
                default:
                    throw new BadRequestException("Invalid ChangeAttribute provided (" + key + ")");
            }
        });
        return entity;
    }

    private Mono<BrainstormingSession> validateSession(BrainstormingSession session) {
        return Mono.just(session)
                .filter(s -> s.getTitle() != null && s.getTitle().length() <= titleLength)
                .switchIfEmpty(Mono.error(new BadRequestException("Maximal Title length exceeded or empty")))
                .filter(s -> s.getMaxWordCount() <= maxWordCount && s.getMaxWordCount() >= minWordCount)
                .switchIfEmpty(Mono.error(new BadRequestException("Maximal Word Count should be between " + minWordCount + " and " + maxWordCount)))
                .filter(s -> s.getMaxWordLength() <= maxWordLength && s.getMaxWordLength() >= minWordLength)
                .switchIfEmpty(Mono.error(new BadRequestException("Maximal Word Length should be between " + minWordLength + " and " + maxWordLength)))
                .filter(s -> s.getCreatedAt() != null && Timestamp.from(Instant.now()).compareTo(s.getCreatedAt()) >= 0)
                .switchIfEmpty(Mono.error(new BadRequestException("The session can only start from now.")));
    }

    private BrainstormingWord parseWordChanges(BrainstormingWord entity, Map<String, Object> changes) {
        changes.forEach((key, value) -> {
            switch (key) {
                case "correctedWord":
                    entity.setCorrectedWord((String) value);
                    break;
                case "banned":
                    entity.setBanned((Boolean) value);
                    break;
                case "categoryId":
                    if (value == null) {
                        entity.setCategoryId(null);
                    } else {
                        entity.setCategoryId(UUID.fromString((String) value));
                    }
                    break;
                case "id":
                case "sessionId":
                case "word":
                case "upvotes":
                case "downvotes":
                    throw new ForbiddenException("You are not allowed to change the " + key);
                default:
                    throw new BadRequestException("Invalid ChangeAttribute provided (" + key + ")");
            }
        });
        return entity;
    }

    private Mono<BrainstormingWord> validateWord(BrainstormingWord word) {
        return Mono.just(word)
                .filter(s -> s.getWord() != null && s.getWord().length() <= 255)
                .switchIfEmpty(Mono.error(new BadRequestException("Maximal Word length exceeded or empty")))
                .filter(s -> s.getWord().toLowerCase(Locale.ROOT).equals(s.getWord()))
                .switchIfEmpty(Mono.error(new BadRequestException("Word must be lowercase!")))
                .filter(s -> s.getCorrectedWord() == null || s.getCorrectedWord().length() <= 255)
                .switchIfEmpty(Mono.error(new BadRequestException("Corrected Word length exceeded")));
    }

    private Mono<BrainstormingSession> applyVotes(BrainstormingSession session) {
        return authorizationHelper.getCurrentUser()
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(user -> Mono.zip(
                        Mono.just(user),
                        wordRepository.findBySessionId(session.getId()).collect(
                                () -> new HashMap<UUID, BrainstormingWordWithMeta>(),
                                (acc, word) -> acc.put(word.getId(), new BrainstormingWordWithMeta(word, null))
                        )
                ))
                .flatMap(tuple2 -> wordRepository.findBySessionId(session.getId())
                        .flatMap(word -> voteRepository.findByAccountIdAndWordId(tuple2.getT1().getAccountId(), word.getId()))
                        .collect(tuple2::getT2, (acc, vote) -> acc.get(vote.getWordId()).setOwnHasUpvoted(vote.isUpvote())))
                .flatMap(map -> {
                    session.setWordsWithMeta(map);
                    return Mono.just(session);
                });
    }
}
