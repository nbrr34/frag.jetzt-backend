package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingWord;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface BrainstormingWordRepository extends ReactiveCrudRepository<BrainstormingWord, UUID> {

    Flux<BrainstormingWord> findBySessionId(UUID sessionId);

    @Query("UPDATE brainstorming_word SET category_id = NULL WHERE session_id = :sessionId;")
    Mono<Void> resetBySessionId(UUID sessionId);

    Mono<BrainstormingWord> findBySessionIdAndWord(UUID sessionId, String word);
}
