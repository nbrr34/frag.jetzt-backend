package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.BonusToken;
import de.thm.arsnova.frag.jetzt.backend.service.BonusTokenFindQueryService;
import de.thm.arsnova.frag.jetzt.backend.service.BonusTokenService;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController("BonusTokenController")
@RequestMapping("/bonustoken")
public class BonusTokenController extends AbstractEntityController {
    private static final Logger logger = LoggerFactory.getLogger(BonusTokenController.class);

    protected static final String REQUEST_MAPPING = "/bonustoken";
    protected static final String DELETE_MAPPING = "/delete";
    protected static final String DELETE_BY_ROOM_MAPPING = "/deletebyroom";

    private final BonusTokenService service;
    private final BonusTokenFindQueryService findQueryService;

    @Autowired
    public BonusTokenController(
            BonusTokenService service,
            BonusTokenFindQueryService findQueryService
    ) {
        this.service = service;
        this.findQueryService = findQueryService;
    }

    @DeleteMapping(DELETE_MAPPING)
    public Mono<Void> delete(
            @RequestParam("commentid") final UUID commentId,
            @RequestParam("userid") final UUID userid
    ) {
        logger.info("Searching for and deleting bonus token, commentId = {}",  commentId);
        return service.deleteByCommentIdAndAccountId(commentId, userid);
    }

    @DeleteMapping(DELETE_BY_ROOM_MAPPING)
    public Mono<Void> deleteByRoom(
        @RequestParam("roomid") final UUID roomId
    ) {
        logger.info("Searching for bonus tokens with roomId = {}", roomId);
        return service.deleteByRoomId(roomId);
    }

    @PostMapping(FIND_MAPPING)
    public Flux<BonusToken> find(@RequestBody final FindQuery<BonusToken> findQuery) {
        logger.debug("Resolving find query: {}", findQuery);
        return findQueryService.resolveQuery(findQuery);
    }
}
