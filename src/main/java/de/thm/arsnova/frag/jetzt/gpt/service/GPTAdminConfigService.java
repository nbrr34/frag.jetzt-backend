package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig.GPTActivationCode;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig.GPTRestrictions;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTConfigRequest;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRoomSettingRepository;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class GPTAdminConfigService {

  private final GPTConfig config;
  private final GPTRoomSettingRepository settingRepository;

  @Autowired
  public GPTAdminConfigService(
    GPTConfig config,
    GPTRoomSettingRepository settingRepository
  ) {
    this.config = config;
    this.settingRepository = settingRepository;
  }

  public Mono<Void> updateConfiguration(final Map<String, Object> data) {
    AtomicReference<Mono<?>> additionalTasks = new AtomicReference<>(
      Mono.just(true)
    );
    AtomicBoolean hasRestrictions = new AtomicBoolean(false);
    data.forEach((key, value) -> {
      switch (key) {
        case "apiKey":
          config.setApiKey((String) value);
          break;
        case "organization":
          config.setOrganization((String) value);
          break;
        case "restrictions":
          hasRestrictions.set(true);
          break;
        default:
          throw new BadRequestException("Invalid ChangeAttribute: " + key);
      }
    });
    if (!hasRestrictions.get()) {
      config.save();
      return Mono.empty();
    }
    final GPTRestrictions confRest = config.getRestrictions();
    @SuppressWarnings("unchecked")
    final Map<String, Object> restricts = (Map<String, Object>) data.get(
      "restrictions"
    );
    restricts.forEach((key, value) -> {
      switch (key) {
        case "active":
          confRest.setActive((boolean) value);
          break;
        case "globalAccumulatedQuota":
          confRest.setGlobalAccumulatedQuota((int) value);
          break;
        case "globalActive":
          confRest.setGlobalActive((boolean) value);
          break;
        case "endDate":
          if (value instanceof String) {
            try {
              value = Timestamp.from(Instant.parse((String) value));
            } catch (DateTimeParseException e) {
              throw new BadRequestException(
                "Restrictions - endDate wrong formatted!"
              );
            }
          }
          confRest.setEndDate((Timestamp) value);
          break;
        case "platformCodes":
          final ArrayList<GPTActivationCode> newCodes = new ArrayList<>(
            confRest.getPlatformCodes()
          );
          for (Object o : ((ArrayList<?>) value)) {
            if (!(o instanceof HashMap)) {
              throw new BadRequestException("platform code has wrong type");
            }
            @SuppressWarnings("unchecked")
            HashMap<String, Object> entry = (HashMap<String, Object>) o;
            String code = (String) entry.get("code");
            int index = -1;
            for (int i = 0; i < newCodes.size(); i++) {
              if (newCodes.get(i).getCode().equals(code)) {
                index = i;
                break;
              }
            }
            if (entry.containsKey("delete")) {
              if (index > -1) {
                GPTActivationCode activationCode = newCodes.remove(index);
                if (
                  activationCode != null &&
                  activationCode.getActivatedRoomId() != null
                ) {
                  additionalTasks.set(
                    additionalTasks
                      .get()
                      .flatMap(ignored ->
                        this.settingRepository.findByRoomId(
                            activationCode.getActivatedRoomId()
                          )
                      )
                      .flatMap(setting -> {
                        setting.setTrialEnabled(false);
                        return settingRepository.save(setting);
                      })
                  );
                }
              }
            } else if (entry.containsKey("maximalCost")) {
              if (index > -1) {
                throw new BadRequestException(
                  "could not add code, code is already present"
                );
              }
              int maximalCost = ((Integer) entry.get("maximalCost")).intValue();
              newCodes.add(new GPTActivationCode(code, maximalCost));
            }
          }
          confRest.setPlatformCodes(newCodes);
          break;
        default:
          throw new BadRequestException("Invalid ChangeAttribute: " + key);
      }
    });
    config.save();
    return additionalTasks.get().then();
  }

  public Mono<GPTConfigRequest> getConfiguration() {
    return Mono.just(new GPTConfigRequest(config.clone()));
  }
}
