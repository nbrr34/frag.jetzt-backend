package de.thm.arsnova.frag.jetzt.backend.model.command;

public class DeleteLivepollSession extends WebSocketCommand<DeleteLivepollSessionPayload> {

  public DeleteLivepollSession() {
    super(DeleteCommentsByRoom.class.getSimpleName());
  }

  public DeleteLivepollSession(DeleteLivepollSessionPayload p) {
    super(DeleteCommentsByRoom.class.getSimpleName());
    this.payload = p;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DeleteLivepollSession that = (DeleteLivepollSession) o;
    return this.getPayload().equals(that.getPayload());
  }
}
