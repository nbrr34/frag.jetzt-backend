package de.thm.arsnova.frag.jetzt.backend.model.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class UpdateBrainstormingCategoriesPayload implements WebSocketPayload {

    private UUID roomId;
    private List<String> categories;

    public UpdateBrainstormingCategoriesPayload() {
    }

    public UpdateBrainstormingCategoriesPayload(UUID roomId, List<String> categories) {
        this.roomId = roomId;
        this.categories = categories;
    }

    @JsonProperty("roomId")
    public UUID getRoomId() {
        return roomId;
    }

    @JsonProperty("roomId")
    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    @JsonProperty("categories")
    public List<String> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "UpdateBrainstormingCategoriesPayload{" +
                "roomId=" + roomId +
                ", categories=" + categories +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateBrainstormingCategoriesPayload that = (UpdateBrainstormingCategoriesPayload) o;
        return Objects.equals(roomId, that.roomId) && Objects.equals(categories, that.categories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roomId, categories);
    }
}
