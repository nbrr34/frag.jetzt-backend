package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;

import java.util.UUID;

public class Rating implements Persistable<UUID> {

    @Id
    private UUID id;
    private UUID accountId;
    private float rating;

    @Override
    public boolean isNew() {
        return id == null;
    }

    public Rating() {

    }

    public Rating(UUID accountId, float rating) {
        this.accountId = accountId;
        this.rating = rating;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
