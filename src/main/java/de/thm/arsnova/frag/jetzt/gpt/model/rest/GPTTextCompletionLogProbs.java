package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;

public class GPTTextCompletionLogProbs {

  private List<Integer> textOffset;
  private List<Double> tokenLogprobs;
  private List<String> tokens;
  private List<Map<String, Double>> topLogprobs;

  @JsonProperty("top_logprobs")
  public List<Map<String, Double>> getTopLogprobs() {
    return topLogprobs;
  }

  public void setTopLogprobs(List<Map<String, Double>> topLogprobs) {
    this.topLogprobs = topLogprobs;
  }

  public List<String> getTokens() {
    return tokens;
  }

  public void setTokens(List<String> tokens) {
    this.tokens = tokens;
  }

  @JsonProperty("token_logprobs")
  public List<Double> getTokenLogprobs() {
    return tokenLogprobs;
  }

  public void setTokenLogprobs(List<Double> tokenLogprobs) {
    this.tokenLogprobs = tokenLogprobs;
  }

  @JsonProperty("text_offset")
  public List<Integer> getTextOffset() {
    return textOffset;
  }

  public void setTextOffset(List<Integer> textOffset) {
    this.textOffset = textOffset;
  }
}
