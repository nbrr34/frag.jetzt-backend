package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import de.thm.arsnova.frag.jetzt.backend.model.RoomQuestionCounts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Service
public class CommentFindQueryService {

    private static final Logger logger = LoggerFactory.getLogger(CommentFindQueryService.class);

    private final CommentService commentService;

    @Autowired
    public CommentFindQueryService(final CommentService commentService) {
        this.commentService = commentService;
    }

    public Flux<RoomQuestionCounts> resolveCountForRooms(final List<Comment> roomIds) {
        return Flux.just(roomIds.toArray(new Comment[0]))
                .flatMapSequential(room -> commentService.getByRoomId(room.getRoomId())
                        .filter(comment -> comment.getDeletedAt() == null &&
                                comment.isAck() == room.isAck() &&
                                comment.getBrainstormingSessionId() == null)
                        .collectList())
                .map(comments -> {
                    long questionCount = 0;
                    long responseCount = 0;
                    HashMap<UUID, Comment> map = new HashMap<>();
                    // sort in ascending order for linear time complexity
                    comments.sort((Comment a, Comment b) -> a.getCreatedAt().compareTo(b.getCreatedAt()));
                    // read flag will be miss-used for detection purpuse if it has parent
                    for (Comment comment : comments) {
                        map.put(comment.getId(), comment);
                        comment.setRead(comment.getCommentReference() == null);
                        if (comment.getCommentReference() == null) {
                            ++questionCount;
                            continue;
                        }
                        Comment parent = map.get(comment.getCommentReference());
                        boolean isReachable = parent != null && parent.isRead();
                        if (isReachable) {
                            comment.setRead(true);
                            ++responseCount;
                        }
                    }
                    return new RoomQuestionCounts(questionCount, responseCount);
                });
    }

    public Flux<Comment> resolveQuery(final FindQuery<Comment> findQuery) {
        if (findQuery.getProperties().getRoomId() != null) {
            boolean acceptDeleted = findQuery.getProperties().getDeletedAt() != null;
            return commentService.getByRoomId(findQuery.getProperties().getRoomId())
                    .filter(c -> c.isAck() == findQuery.getProperties().isAck())
                    .filter(c -> (c.getDeletedAt() != null) == acceptDeleted);
        }
        return Flux.empty();
    }
}
