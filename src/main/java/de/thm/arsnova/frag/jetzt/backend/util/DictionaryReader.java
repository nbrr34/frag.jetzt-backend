package de.thm.arsnova.frag.jetzt.backend.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import org.springframework.core.io.Resource;

public class DictionaryReader {

  public static class PasswordFoundRange {

    private int start;
    private int end;

    PasswordFoundRange(int start, int end) {
      this.start = start;
      this.end = end;
    }

    @JsonIgnore
    boolean add(int otherStart, int otherEnd) {
      if (otherStart <= end && otherEnd >= start) {
        start = Math.min(start, otherStart);
        end = Math.max(end, otherEnd);
        return true;
      }
      return false;
    }

    public int getStart() {
      return start;
    }

    public int getEnd() {
      return end;
    }
  }

  private ArrayList<String> dictionary = new ArrayList<>();
  private int minLength = Integer.MAX_VALUE;
  private int maxLength = 0;

  public DictionaryReader(Resource resource) {
    try {
      InputStream is = resource.getInputStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(is));
      String line;
      while ((line = reader.readLine()) != null) {
        int length = line.length();
        if (length < 1) {
          continue;
        }
        dictionary.add(line);
        if (minLength > length) {
          minLength = length;
        }
        if (maxLength < length) {
          maxLength = length;
        }
      }
      is.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    dictionary.sort(
      new Comparator<String>() {
        @Override
        public int compare(String arg0, String arg1) {
          return arg0.compareTo(arg1);
        }
      }
    );
  }

  public ArrayList<PasswordFoundRange> find(
    String password,
    ArrayList<PasswordFoundRange> foundRanges
  ) {
    final int len = password.length() - minLength;
    for (int i = 0; i <= len; i++) {
      for (
        int k = minLength;
        password.length() - i - k >= 0 && k <= maxLength;
        k++
      ) {
        int end = i + k;
        if (binarySearch(password.substring(i, end))) {
          int added = -1;
          for (int j = foundRanges.size() - 1; j >= 0; j--) {
            if (foundRanges.get(j).add(i, end)) {
              if (added >= 0) {
                PasswordFoundRange r = foundRanges.remove(j);
                added -= 1;
                foundRanges.get(added).add(r.start, r.end);
              } else {
                added = j;
              }
            }
          }
          if (added < 0) {
            foundRanges.add(new PasswordFoundRange(i, end));
          }
        }
      }
    }
    return foundRanges;
  }

  private boolean binarySearch(String password) {
    int left = 0;
    int right = this.dictionary.size() - 1;
    while (left <= right) {
      int mid = left + ((right - left) / 2);
      int comparing = password.compareTo(this.dictionary.get(mid));
      if (comparing == 0) {
        return true;
      } else if (comparing < 0) {
        right = mid - 1;
      } else {
        left = mid + 1;
      }
    }
    return false;
  }
}
