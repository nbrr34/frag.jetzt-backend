package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingCategory;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.UUID;

public interface BrainstormingCategoryRepository extends ReactiveCrudRepository<BrainstormingCategory, UUID>  {

    Flux<BrainstormingCategory> findByRoomId(UUID roomId);

}
