package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

public class Tag implements Persistable<UUID> {

    @Id
    private UUID id;
    private UUID roomId;
    private String tag;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    public Tag(){
    }

    public Tag(UUID roomId, String tag) {
        this.roomId = roomId;
        this.tag = tag;
    }

    @Override
    public boolean isNew() {
        return id == null;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", roomId=" + roomId +
                ", tag='" + tag + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag1 = (Tag) o;
        return Objects.equals(id, tag1.id) &&
                Objects.equals(roomId, tag1.roomId) &&
                Objects.equals(tag, tag1.tag) &&
                Objects.equals(createdAt, tag1.createdAt) &&
                Objects.equals(updatedAt, tag1.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomId, tag, createdAt, updatedAt);
    }
}
