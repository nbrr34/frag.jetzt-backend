package de.thm.arsnova.frag.jetzt.backend.model.command;

public class CreateBrainstormingWord extends WebSocketCommand<CreateBrainstormingWordPayload> {

    public CreateBrainstormingWord() {
        super(CreateBrainstormingWord.class.getSimpleName());
    }

    public CreateBrainstormingWord(CreateBrainstormingWordPayload payload) {
        super(CreateBrainstormingWord.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateBrainstormingWord that = (CreateBrainstormingWord) o;
        return this.getPayload().equals(that.getPayload());
    }

}
