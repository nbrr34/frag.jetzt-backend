package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class BrainstormingCreated extends WebSocketEvent<BrainstormingCreatedPayload> {

    public BrainstormingCreated() {
        super(BrainstormingCreated.class.getSimpleName());
    }

    public BrainstormingCreated(BrainstormingCreatedPayload payload, UUID roomId) {
        super(BrainstormingCreated.class.getSimpleName(), roomId);
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingCreated that = (BrainstormingCreated) o;
        return this.getPayload().equals(that.getPayload());
    }
}
