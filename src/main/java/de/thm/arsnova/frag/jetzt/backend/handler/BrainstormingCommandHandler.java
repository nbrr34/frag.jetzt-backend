package de.thm.arsnova.frag.jetzt.backend.handler;

import de.thm.arsnova.frag.jetzt.backend.RoomEventSource;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingCategory;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingVote;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingWord;
import de.thm.arsnova.frag.jetzt.backend.model.command.*;
import de.thm.arsnova.frag.jetzt.backend.service.BrainstormingSessionService;
import de.thm.arsnova.frag.jetzt.backend.service.BrainstormingVoteService;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.BrainstormingSessionRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.BrainstormingWordRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

@Component
public class BrainstormingCommandHandler {

    @Value("${app.brainstorming.minWordLength}")
    private int minWordLength;
    @Value("${app.brainstorming.wordLengthLimit}")
    private int wordLengthLimit;

    private static final Logger logger = LoggerFactory.getLogger(BrainstormingCommandHandler.class);

    private final BrainstormingSessionService service;
    private final BrainstormingSessionRepository repository;
    private final BrainstormingVoteService voteService;
    private final BrainstormingWordRepository wordRepository;
    private final RoomEventSource eventer;

    @Autowired
    public BrainstormingCommandHandler(
            BrainstormingSessionService service,
            BrainstormingSessionRepository repository,
            BrainstormingVoteService voteService,
            BrainstormingWordRepository wordRepository,
            RoomEventSource eventer
    ) {
        this.service = service;
        this.repository = repository;
        this.voteService = voteService;
        this.wordRepository = wordRepository;
        this.eventer = eventer;
    }

    public Mono<Void> handle(ResetBrainstormingVotes command) {
        logger.trace("got new command: " + command.toString());

        ResetBrainstormingVotesPayload payload = command.getPayload();
        return voteService.deleteAllBySessionId(payload.getSessionId())
                .count()
                .flatMap(count -> service.get(payload.getSessionId()))
                .doOnSuccess(session -> eventer.sessionVotesReset(session.getId(), session.getRoomId()))
                .then();
    }

    public Mono<Void> handle(ResetBrainstormingCategorization command) {
        logger.trace("got new command: " + command.toString());

        ResetBrainstormingCategorizationPayload payload = command.getPayload();
        return wordRepository.resetBySessionId(payload.getSessionId())
                .then(service.get(payload.getSessionId()))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .doOnSuccess(session -> eventer.sessionCategorizationReset(session.getId(), session.getRoomId()))
                .then();
    }

    public Mono<BrainstormingSession> handle(CreateBrainstorming command) {
        logger.trace("got new command: " + command.toString());

        CreateBrainstormingPayload payload = command.getPayload();
        BrainstormingSession session = new BrainstormingSession();
        session.setRoomId(payload.getRoomId());
        session.setTitle(payload.getTitle());
        session.setMaxWordLength(payload.getMaxWordLength());
        session.setMaxWordCount(payload.getMaxWordCount());
        session.setLanguage(payload.getLanguage());
        session.setRatingAllowed(payload.isRatingAllowed());
        session.setIdeasFrozen(payload.isIdeasFrozen());
        session.setIdeasTimeDuration(payload.getIdeasTimeDuration());
        session.setIdeasEndTimestamp(payload.getIdeasEndTimestamp());
        return this.service.create(session)
                .doOnSuccess(eventer::sessionCreated);
    }

    public Mono<BrainstormingWord> handle(CreateBrainstormingWord command) {
        logger.trace("got new command: " + command.toString());

        CreateBrainstormingWordPayload payload = command.getPayload();
        return this.verifyWords(payload.getName(), payload.getRealName())
                .flatMap(tuple2 -> this.service.createWord(payload.getSessionId(), tuple2.getT1(), tuple2.getT2()))
                .flatMap(tuple2 -> Mono.zip(
                        Mono.just(tuple2.getT1()),
                        Mono.just(tuple2.getT2()),
                        repository.findById(tuple2.getT1().getSessionId())
                ))
                .doOnSuccess(data -> {
                    if (!data.getT2()) {
                        this.eventer.sessionWordCreated(data.getT1(), data.getT3().getRoomId());
                    }
                })
                .map(Tuple2::getT1);
    }

    public Flux<BrainstormingCategory> handle(UpdateBrainstormingCategories command) {
        logger.trace("got new command: " + command.toString());

        UpdateBrainstormingCategoriesPayload payload = command.getPayload();
        return this.service.updateCategories(payload.getRoomId(), payload.getCategories())
                .collectList()
                .doOnSuccess((list) -> this.eventer.sessionCategoriesUpdated(payload.getRoomId(), list))
                .flatMapMany(Flux::fromIterable);
    }

    public Mono<BrainstormingSession> handle(PatchBrainstorming command) {
        logger.trace("got new command: " + command.toString());

        PatchBrainstormingPayload payload = command.getPayload();
        return this.service.get(payload.getId())
                .flatMap(session -> this.service.patch(session, payload.getChanges()))
                .doOnSuccess(s -> this.eventer.sessionPatched(s.getId(), s.getRoomId(), payload.getChanges()));
    }

    public Mono<BrainstormingWord> handle(PatchBrainstormingWord command) {
        logger.trace("got new command: " + command.toString());

        PatchBrainstormingWordPayload payload = command.getPayload();
        return this.wordRepository.findById(payload.getId())
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(word -> this.service.patchWord(word, payload.getChanges()))
                .doOnSuccess(tuple2 -> this.eventer.sessionWordPatched(payload.getId(), tuple2.getT1(), payload.getChanges()))
                .map(Tuple2::getT2);
    }

    public Mono<Void> handle(DeleteBrainstorming command) {
        logger.trace("got new command: " + command.toString());

        return repository.findById(command.getPayload().getId())
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(session -> this.service.deleteByIdAndRoomId(session.getId(), session.getRoomId())
                        .doOnSuccess(s -> eventer.sessionDeleted(session.getId(), session.getRoomId())));
    }

    public Mono<BrainstormingVote> handle(CreateBrainstormingVote command) {
        logger.trace("got new command: " + command.toString());

        CreateBrainstormingVotePayload payload = command.getPayload();
        return wordRepository.findById(payload.getWordId())
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(w -> Mono.zip(
                        Mono.just(w),
                        repository.findById(w.getSessionId())
                ))
                .switchIfEmpty(Mono.error(new NotFoundException("Session does not exist!")))
                .flatMap(tuple2 -> Mono.zip(Mono.just(tuple2.getT1()), service.checkCanCreateRating(tuple2.getT2())))
                .flatMap(tuple2 -> Mono.zip(
                        this.voteService
                                .create(new BrainstormingVote(null, tuple2.getT1().getId(), payload.isUpvote())),
                        Mono.just(tuple2.getT2().getRoomId())
                ))
                .doOnSuccess(tuple2 -> eventer.sessionWordVoted(tuple2.getT1().getWordId(), tuple2.getT2()))
                .map(Tuple2::getT1);
    }

    public Mono<Void> handle(DeleteBrainstormingVote command) {
        logger.trace("got new command: " + command.toString());

        DeleteBrainstormingVotePayload payload = command.getPayload();
        return wordRepository.findById(payload.getWordId())
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(w -> Mono.zip(
                        Mono.just(w),
                        repository.findById(w.getSessionId())
                ))
                .switchIfEmpty(Mono.error(new NotFoundException("Session does not exist!")))
                .flatMap(tuple2 -> Mono.zip(Mono.just(tuple2.getT1()), service.checkCanCreateRating(tuple2.getT2())))
                .flatMap(tuple2 -> this.voteService.deleteByWordId(tuple2.getT1().getId())
                        .then(Mono.zip(
                                Mono.just(tuple2.getT1().getId()),
                                Mono.just(tuple2.getT2().getRoomId())
                        )))
                .doOnSuccess(tuple2 -> eventer.sessionWordVoted(tuple2.getT1(), tuple2.getT2()))
                .then();
    }

    private Mono<Tuple2<String, String>> verifyWords(String word, String realWord) {
        return Mono.just(word)
                .filter(w -> w != null && w.length() <= wordLengthLimit && w.length() >= minWordLength)
                .switchIfEmpty(Mono.error(new BadRequestException("The word must be between " + minWordLength + " and " + wordLengthLimit)))
                .map(w -> realWord)
                .filter(w -> w != null && w.length() <= wordLengthLimit && w.length() >= minWordLength)
                .switchIfEmpty(Mono.error(new BadRequestException("The word must be between " + minWordLength + " and " + wordLengthLimit)))
                .map(w -> Tuples.of(word, realWord));
    }
}
