package de.thm.arsnova.frag.jetzt.gpt.model.requestStatistic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRequestStatistic;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

public class GPTRequestStatisticResult {

  private final HashMap<UUID, HashSet<String>> conversations = new HashMap<>();

  @JsonIgnore
  public GPTRequestStatisticResult accept(GPTRequestStatistic statistic) {
    HashSet<String> set = conversations.computeIfAbsent(
      statistic.getRequesterId(),
      id -> new HashSet<>()
    );
    String prompt = statistic.getPrompt();
    String completion = statistic.getCompletion();
    int lastBreak = prompt.lastIndexOf('\n');
    if (lastBreak >= 0) {
      set.remove(prompt.substring(0, lastBreak));
    }
    set.add(prompt + "\n" + completion);
    return this;
  }

  public HashMap<UUID, HashSet<String>> getConversations() {
    return conversations;
  }
}
