package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.controller.VoteController;
import de.thm.arsnova.frag.jetzt.backend.model.AbstractVote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Service
public class VoteFindQueryService {
    private final VoteService voteService;
    private final CommentService commentService;

    @Autowired
    public VoteFindQueryService(
            final VoteService voteService,
            final CommentService commentService
    ) {
        this.voteService = voteService;
        this.commentService = commentService;
    }

    public Flux<AbstractVote> resolveQuery(final FindQuery<VoteController.WebServiceVote> findQuery) {
        if (findQuery.getExternalFilters().get("roomId") instanceof String && findQuery.getProperties().getAccountId() != null) {
            UUID roomId = UUID.fromString((String) findQuery.getExternalFilters().get("roomId"));
            UUID userId = findQuery.getProperties().getAccountId();
            return commentService.getByRoomId(roomId)
                    .flatMap(c -> voteService.getForCommentAndUser(c.getId(), userId));
        }
        return Flux.empty();
    }
}
