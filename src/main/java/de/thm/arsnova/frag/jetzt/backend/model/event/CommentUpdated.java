package de.thm.arsnova.frag.jetzt.backend.model.event;

import de.thm.arsnova.frag.jetzt.backend.model.command.UpdateComment;

import java.util.UUID;

public class CommentUpdated extends WebSocketEvent<CommentUpdatedPayload> {
    public CommentUpdated() {
        super(UpdateComment.class.getSimpleName());
    }

    public CommentUpdated(CommentUpdatedPayload p, UUID id) {
        super(CommentUpdated.class.getSimpleName(), id);
        this.payload = p;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentUpdated that = (CommentUpdated) o;
        return this.getPayload().equals(that.getPayload());
    }
}
