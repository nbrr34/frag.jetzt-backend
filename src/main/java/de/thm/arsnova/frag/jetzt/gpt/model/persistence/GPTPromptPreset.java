package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class GPTPromptPreset implements Persistable<UUID> {

  @Id
  private UUID id;

  private UUID accountId;
  private String act;
  private String prompt;
  private String language;
  private float temperature;
  private float presencePenalty;
  private float frequencyPenalty;
  private float topP;
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  public GPTPromptPreset() {}

  public GPTPromptPreset(
    UUID accountId,
    String act,
    String prompt,
    String language,
    float temperature,
    float presencePenalty,
    float frequencyPenalty,
    float topP
  ) {
    this.accountId = accountId;
    this.act = act;
    this.prompt = prompt;
    this.language = language;
    this.temperature = temperature;
    this.presencePenalty = presencePenalty;
    this.frequencyPenalty = frequencyPenalty;
    this.topP = topP;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public String getAct() {
    return act;
  }

  public void setAct(String act) {
    this.act = act;
  }

  public String getPrompt() {
    return prompt;
  }

  public void setPrompt(String prompt) {
    this.prompt = prompt;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public float getTemperature() {
    return temperature;
  }

  public void setTemperature(float temperature) {
    this.temperature = temperature;
  }

  public float getPresencePenalty() {
    return presencePenalty;
  }

  public void setPresencePenalty(float presencePenalty) {
    this.presencePenalty = presencePenalty;
  }

  public float getFrequencyPenalty() {
    return frequencyPenalty;
  }

  public void setFrequencyPenalty(float frequencyPenalty) {
    this.frequencyPenalty = frequencyPenalty;
  }

  public float getTopP() {
    return topP;
  }

  public void setTopP(float topP) {
    this.topP = topP;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((act == null) ? 0 : act.hashCode());
    result = prime * result + ((prompt == null) ? 0 : prompt.hashCode());
    result = prime * result + ((language == null) ? 0 : language.hashCode());
    result = prime * result + Float.floatToIntBits(temperature);
    result = prime * result + Float.floatToIntBits(presencePenalty);
    result = prime * result + Float.floatToIntBits(frequencyPenalty);
    result = prime * result + Float.floatToIntBits(topP);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTPromptPreset other = (GPTPromptPreset) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (act == null) {
      if (other.act != null) return false;
    } else if (!act.equals(other.act)) return false;
    if (prompt == null) {
      if (other.prompt != null) return false;
    } else if (!prompt.equals(other.prompt)) return false;
    if (language == null) {
      if (other.language != null) return false;
    } else if (!language.equals(other.language)) return false;
    if (
      Float.floatToIntBits(temperature) !=
      Float.floatToIntBits(other.temperature)
    ) return false;
    if (
      Float.floatToIntBits(presencePenalty) !=
      Float.floatToIntBits(other.presencePenalty)
    ) return false;
    if (
      Float.floatToIntBits(frequencyPenalty) !=
      Float.floatToIntBits(other.frequencyPenalty)
    ) return false;
    if (
      Float.floatToIntBits(topP) != Float.floatToIntBits(other.topP)
    ) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTPromptPreset [id=" +
      id +
      ", accountId=" +
      accountId +
      ", act=" +
      act +
      ", prompt=" +
      prompt +
      ", language=" +
      language +
      ", temperature=" +
      temperature +
      ", presencePenalty=" +
      presencePenalty +
      ", frequencyPenalty=" +
      frequencyPenalty +
      ", topP=" +
      topP +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
