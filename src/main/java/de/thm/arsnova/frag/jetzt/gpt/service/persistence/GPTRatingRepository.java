package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import java.util.UUID;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.backend.model.RatingResult;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRating;
import reactor.core.publisher.Mono;

@Repository
public interface GPTRatingRepository extends ReactiveCrudRepository<GPTRating, UUID> {
    
    @Query("SELECT AVG(rating) AS rating, " +
            "COUNT(account_id) AS people, " +
            "ROUND(COUNT(CASE WHEN rating <= 1 THEN 1 END) * 100 / COALESCE(NULLIF(COUNT(account_id), 0), 1)::float) AS one_star_percent, " +
            "ROUND(COUNT(CASE WHEN rating <= 2 AND rating > 1 THEN 1 END) * 100 / COALESCE(NULLIF(COUNT(account_id), 0), 1)::float) AS two_star_percent, " +
            "ROUND(COUNT(CASE WHEN rating <= 3 AND rating > 2 THEN 1 END) * 100 / COALESCE(NULLIF(COUNT(account_id), 0), 1)::float) AS three_star_percent, " +
            "ROUND(COUNT(CASE WHEN rating <= 4 AND rating > 3 THEN 1 END) * 100 / COALESCE(NULLIF(COUNT(account_id), 0), 1)::float) AS four_star_percent, " +
            "ROUND(COUNT(CASE WHEN rating <= 5 AND rating > 4 THEN 1 END) * 100 / COALESCE(NULLIF(COUNT(account_id), 0), 1)::float) AS five_star_percent " +
            "FROM rating")
    Mono<RatingResult> getRatings();

    Mono<GPTRating> findByAccountId(UUID accountId);

    Mono<Void> deleteByAccountId(UUID accountId);

}
