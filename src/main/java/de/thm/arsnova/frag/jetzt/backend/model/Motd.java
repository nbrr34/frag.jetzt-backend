package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Table
public class Motd implements Persistable<UUID> {

    @Id
    private UUID id;
    private Timestamp startTimestamp;
    private Timestamp endTimestamp;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;
    @Transient
    private Map<MotdMessage.Language, MotdMessage> messages;

    public Motd() {
    }

    public Motd(Timestamp startTimestamp, Timestamp endTimestamp, Map<MotdMessage.Language, MotdMessage> messages) {
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
        this.messages = messages;
    }

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Timestamp getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Timestamp startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public Timestamp getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(Timestamp endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public Map<MotdMessage.Language, MotdMessage> getMessages() {
        return messages;
    }

    public void setMessages(Map<MotdMessage.Language, MotdMessage> messages) {
        this.messages = messages;
    }

    @Override
    public String toString() {
        return "Motd{" +
                "id=" + id +
                ", startTimestamp=" + startTimestamp +
                ", endTimestamp=" + endTimestamp +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", messages=" + messages +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Motd motd = (Motd) o;
        return Objects.equals(id, motd.id) &&
                Objects.equals(startTimestamp, motd.startTimestamp) &&
                Objects.equals(endTimestamp, motd.endTimestamp) &&
                Objects.equals(messages, motd.messages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startTimestamp, endTimestamp, messages);
    }
}
