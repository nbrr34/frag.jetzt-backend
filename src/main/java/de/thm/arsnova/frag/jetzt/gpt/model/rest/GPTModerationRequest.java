package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@JsonInclude(Include.NON_NULL)
public class GPTModerationRequest implements IGPTRequest {

  private String apiKey;
  private String apiOrganization;
  private List<String> input;
  /** Defaults to null */
  private String model;

  @JsonIgnore
  @Override
  public UUID getRoomId() {
    return null;
  }

  @JsonIgnore
  @Override
  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  @JsonIgnore
  @Override
  public String getApiOrganization() {
    return apiOrganization;
  }

  public void setApiOrganization(String apiOrganization) {
    this.apiOrganization = apiOrganization;
  }

  public List<String> getInput() {
    return input;
  }

  public void setInput(List<String> input) {
    if (input == null || input.size() < 1) {
      throw new IllegalArgumentException("input can not be empty!");
    }
    this.input = Collections.unmodifiableList(input);
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }
}
