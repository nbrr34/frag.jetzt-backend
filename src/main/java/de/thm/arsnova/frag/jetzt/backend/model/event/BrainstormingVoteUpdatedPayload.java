package de.thm.arsnova.frag.jetzt.backend.model.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingWord;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class BrainstormingVoteUpdatedPayload implements WebSocketPayload {

    private UUID wordId;
    private int upvotes;
    private int downvotes;

    public BrainstormingVoteUpdatedPayload() {

    }

    public BrainstormingVoteUpdatedPayload(BrainstormingWord word) {
        if (word != null) {
            this.wordId = word.getId();
            this.upvotes = word.getUpvotes();
            this.downvotes = word.getDownvotes();
        }
    }

    public UUID getWordId() {
        return wordId;
    }

    public void setWordId(UUID wordId) {
        this.wordId = wordId;
    }

    @JsonProperty("upvotes")
    public int getUpvotes() {
        return upvotes;
    }

    @JsonProperty("upvotes")
    public void setUpvotes(int upvotes) {
        this.upvotes = upvotes;
    }

    @JsonProperty("downvotes")
    public int getDownvotes() {
        return downvotes;
    }

    @JsonProperty("downvotes")
    public void setDownvotes(int downvotes) {
        this.downvotes = downvotes;
    }

    @Override
    public String toString() {
        return "BrainstormingVoteUpdatedPayload{" +
                "wordId=" + wordId +
                ", upvotes=" + upvotes +
                ", downvotes=" + downvotes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingVoteUpdatedPayload that = (BrainstormingVoteUpdatedPayload) o;
        return upvotes == that.upvotes && downvotes == that.downvotes && Objects.equals(wordId, that.wordId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wordId, upvotes, downvotes);
    }
}
