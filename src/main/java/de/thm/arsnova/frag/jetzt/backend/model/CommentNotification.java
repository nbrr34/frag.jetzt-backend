package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Table
public class CommentNotification implements Persistable<UUID> {

    @Id
    private UUID id;
    private UUID accountId;
    private UUID roomId;
    private short notificationSetting;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public short getNotificationSetting() {
        return notificationSetting;
    }

    public void setNotificationSetting(short notificationSetting) {
        this.notificationSetting = notificationSetting;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "CommentNotification{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", roomId=" + roomId +
                ", notificationSetting=" + notificationSetting +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentNotification that = (CommentNotification) o;
        return notificationSetting == that.notificationSetting &&
                Objects.equals(id, that.id) &&
                Objects.equals(accountId, that.accountId) &&
                Objects.equals(roomId, that.roomId) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(updatedAt, that.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountId, roomId, notificationSetting, createdAt, updatedAt);
    }
}
