package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface IGPTCompletionChoice {
  /**
   * Index of this choice. Satisfies index < n.
   */
  int getIndex();

  /**
   * Possible outcomes:
   * <ul>
   *   <li>'stop' (model output completed)</li>
   *   <li>'length' (maxTokens or token limit)</li>
   *   <li>'content_filter' (omitted content due to filter)</li>
   *   <li>null (Still in Progress or incomplete)</li>
   * </ul>
   */
  @JsonProperty("finish_reason")
  String getFinishReason();
}
