package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.sql.Timestamp;
import java.util.UUID;

@Repository
public interface CommentRepository extends ReactiveCrudRepository<Comment, UUID> {

    @Query("ALTER TABLE comment DISABLE TRIGGER USER;")
    Mono<Void> disableTriggers();

    @Query("ALTER TABLE comment ENABLE TRIGGER USER;")
    Mono<Void> enableTriggers();

    Flux<Void> deleteAllByRoomId(UUID roomId);

    Flux<Comment> findByRoomId(UUID roomId);

    Flux<Comment> findByRoomIdAndCreatedAtGreaterThanEqualAndDeletedAtIsNullOrderByNumberAsc(UUID roomId, Timestamp timestamp);

}
