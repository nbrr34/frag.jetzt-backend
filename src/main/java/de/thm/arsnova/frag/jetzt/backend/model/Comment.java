package de.thm.arsnova.frag.jetzt.backend.model;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class Comment implements Persistable<UUID> {

  public enum Language {
    DE,
    EN,
    FR,
    ES,
    IT,
    NL,
    PT,
    AUTO,
  }

  @Id
  protected UUID id;

  protected UUID roomId;
  protected UUID creatorId;
  protected String number;
  protected String body;
  protected boolean read;
  protected boolean favorite;
  protected int correct;
  protected boolean ack;
  protected boolean bookmark;
  protected String keywordsFromSpacy;
  protected String keywordsFromQuestioner;
  protected int upvotes = 0;
  protected int downvotes = 0;
  protected int score = 0;
  protected String tag;
  protected String questionerName;
  protected Language language;
  protected UUID brainstormingSessionId;
  protected UUID brainstormingWordId;
  protected UUID commentReference;
  protected int commentDepth;
  protected boolean approved;
  protected int gptWriterState;
  protected Timestamp createdAt = Timestamp.from(Instant.now());
  protected Timestamp updatedAt;
  protected Timestamp deletedAt;

  @Override
  public boolean isNew() {
    return id == null;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public UUID getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(UUID creatorId) {
    this.creatorId = creatorId;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public boolean isRead() {
    return read;
  }

  public void setRead(boolean read) {
    this.read = read;
  }

  public boolean isFavorite() {
    return favorite;
  }

  public void setFavorite(boolean favorite) {
    this.favorite = favorite;
  }

  public boolean isBookmark() {
    return bookmark;
  }

  public void setBookmark(boolean bookmark) {
    this.bookmark = bookmark;
  }

  public int getCorrect() {
    return correct;
  }

  public void setCorrect(int correct) {
    this.correct = correct;
  }

  public boolean isAck() {
    return ack;
  }

  public void setAck(boolean ack) {
    this.ack = ack;
  }

  public int getScore() {
    return score;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getKeywordsFromSpacy() {
    return keywordsFromSpacy;
  }

  public void setKeywordsFromSpacy(String keywordsFromSpacy) {
    this.keywordsFromSpacy = keywordsFromSpacy;
  }

  public String getKeywordsFromQuestioner() {
    return keywordsFromQuestioner;
  }

  public void setKeywordsFromQuestioner(String keywordsFromQuestioner) {
    this.keywordsFromQuestioner = keywordsFromQuestioner;
  }

  public int getUpvotes() {
    return upvotes;
  }

  public void setUpvotes(int upvotes) {
    this.upvotes = upvotes;
  }

  public int getDownvotes() {
    return downvotes;
  }

  public void setDownvotes(int downvotes) {
    this.downvotes = downvotes;
  }

  public Language getLanguage() {
    return language;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

  public String getQuestionerName() {
    return questionerName;
  }

  public void setQuestionerName(String questionerName) {
    this.questionerName = questionerName;
  }

  public UUID getBrainstormingSessionId() {
    return brainstormingSessionId;
  }

  public void setBrainstormingSessionId(UUID brainstormingSessionId) {
    this.brainstormingSessionId = brainstormingSessionId;
  }

  public UUID getBrainstormingWordId() {
    return brainstormingWordId;
  }

  public void setBrainstormingWordId(UUID brainstormingWordId) {
    this.brainstormingWordId = brainstormingWordId;
  }

  public UUID getCommentReference() {
    return commentReference;
  }

  public void setCommentReference(UUID commentReference) {
    this.commentReference = commentReference;
  }

  public int getCommentDepth() {
    return commentDepth;
  }

  public void setCommentDepth(int commentDepth) {
    this.commentDepth = commentDepth;
  }

  public boolean isApproved() {
    return approved;
  }

  public void setApproved(boolean approved) {
    this.approved = approved;
  }

  public int getGptWriterState() {
    return gptWriterState;
  }

  public void setGptWriterState(int gptWriterState) {
    this.gptWriterState = gptWriterState;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public Timestamp getDeletedAt() {
    return deletedAt;
  }

  public void markAsDeleted() {
    deletedAt = Timestamp.from(Instant.now());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result = prime * result + ((creatorId == null) ? 0 : creatorId.hashCode());
    result = prime * result + ((number == null) ? 0 : number.hashCode());
    result = prime * result + ((body == null) ? 0 : body.hashCode());
    result = prime * result + (read ? 1231 : 1237);
    result = prime * result + (favorite ? 1231 : 1237);
    result = prime * result + correct;
    result = prime * result + (ack ? 1231 : 1237);
    result = prime * result + (bookmark ? 1231 : 1237);
    result =
      prime *
      result +
      ((keywordsFromSpacy == null) ? 0 : keywordsFromSpacy.hashCode());
    result =
      prime *
      result +
      (
        (keywordsFromQuestioner == null) ? 0 : keywordsFromQuestioner.hashCode()
      );
    result = prime * result + upvotes;
    result = prime * result + downvotes;
    result = prime * result + score;
    result = prime * result + ((tag == null) ? 0 : tag.hashCode());
    result =
      prime *
      result +
      ((questionerName == null) ? 0 : questionerName.hashCode());
    result = prime * result + ((language == null) ? 0 : language.hashCode());
    result =
      prime *
      result +
      (
        (brainstormingSessionId == null) ? 0 : brainstormingSessionId.hashCode()
      );
    result =
      prime *
      result +
      ((brainstormingWordId == null) ? 0 : brainstormingWordId.hashCode());
    result =
      prime *
      result +
      ((commentReference == null) ? 0 : commentReference.hashCode());
    result = prime * result + commentDepth;
    result = prime * result + (approved ? 1231 : 1237);
    result = prime * result + gptWriterState;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Comment other = (Comment) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (creatorId == null) {
      if (other.creatorId != null) return false;
    } else if (!creatorId.equals(other.creatorId)) return false;
    if (number == null) {
      if (other.number != null) return false;
    } else if (!number.equals(other.number)) return false;
    if (body == null) {
      if (other.body != null) return false;
    } else if (!body.equals(other.body)) return false;
    if (read != other.read) return false;
    if (favorite != other.favorite) return false;
    if (correct != other.correct) return false;
    if (ack != other.ack) return false;
    if (bookmark != other.bookmark) return false;
    if (keywordsFromSpacy == null) {
      if (other.keywordsFromSpacy != null) return false;
    } else if (!keywordsFromSpacy.equals(other.keywordsFromSpacy)) return false;
    if (keywordsFromQuestioner == null) {
      if (other.keywordsFromQuestioner != null) return false;
    } else if (
      !keywordsFromQuestioner.equals(other.keywordsFromQuestioner)
    ) return false;
    if (upvotes != other.upvotes) return false;
    if (downvotes != other.downvotes) return false;
    if (score != other.score) return false;
    if (tag == null) {
      if (other.tag != null) return false;
    } else if (!tag.equals(other.tag)) return false;
    if (questionerName == null) {
      if (other.questionerName != null) return false;
    } else if (!questionerName.equals(other.questionerName)) return false;
    if (language != other.language) return false;
    if (brainstormingSessionId == null) {
      if (other.brainstormingSessionId != null) return false;
    } else if (
      !brainstormingSessionId.equals(other.brainstormingSessionId)
    ) return false;
    if (brainstormingWordId == null) {
      if (other.brainstormingWordId != null) return false;
    } else if (
      !brainstormingWordId.equals(other.brainstormingWordId)
    ) return false;
    if (commentReference == null) {
      if (other.commentReference != null) return false;
    } else if (!commentReference.equals(other.commentReference)) return false;
    if (commentDepth != other.commentDepth) return false;
    if (approved != other.approved) return false;
    if (gptWriterState != other.gptWriterState) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "Comment [id=" +
      id +
      ", roomId=" +
      roomId +
      ", creatorId=" +
      creatorId +
      ", number=" +
      number +
      ", body=" +
      body +
      ", read=" +
      read +
      ", favorite=" +
      favorite +
      ", correct=" +
      correct +
      ", ack=" +
      ack +
      ", bookmark=" +
      bookmark +
      ", keywordsFromSpacy=" +
      keywordsFromSpacy +
      ", keywordsFromQuestioner=" +
      keywordsFromQuestioner +
      ", upvotes=" +
      upvotes +
      ", downvotes=" +
      downvotes +
      ", score=" +
      score +
      ", tag=" +
      tag +
      ", questionerName=" +
      questionerName +
      ", language=" +
      language +
      ", brainstormingSessionId=" +
      brainstormingSessionId +
      ", brainstormingWordId=" +
      brainstormingWordId +
      ", commentReference=" +
      commentReference +
      ", commentDepth=" +
      commentDepth +
      ", approved=" +
      approved +
      ", gptWriterState=" +
      gptWriterState +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", deletedAt=" +
      deletedAt +
      "]"
    );
  }
}
