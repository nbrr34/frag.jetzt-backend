package de.thm.arsnova.frag.jetzt.gpt.model;

public class GPTRoomAccessInfo {

  private final GPTGlobalAccessInfo globalInfo;
  private final boolean roomOwnerRegistered;
  private final boolean apiKeyPresent;
  private final boolean usingTrial;
  private final boolean roomDisabled;
  private final boolean moderatorDisabled;
  private final boolean participantDisabled;
  private final boolean usageTimeOver;
  private final boolean restricted;

  public GPTRoomAccessInfo(
    GPTGlobalAccessInfo globalInfo,
    boolean roomOwnerRegistered,
    boolean apiKeyPresent,
    boolean usingTrial,
    boolean roomDisabled,
    boolean moderatorDisabled,
    boolean participantDisabled,
    boolean usageTimeOver,
    boolean isMod,
    boolean isOwner,
    boolean allowUnregistered
  ) {
    this.globalInfo = globalInfo;
    this.roomOwnerRegistered = roomOwnerRegistered;
    this.apiKeyPresent = apiKeyPresent;
    this.usingTrial = usingTrial;
    this.roomDisabled = roomDisabled;
    this.moderatorDisabled = moderatorDisabled;
    this.participantDisabled = participantDisabled;
    this.usageTimeOver = usageTimeOver;
    this.restricted =
      globalInfo.isBlocked() ||
      !globalInfo.isRegistered() &&
      !allowUnregistered ||
      (!apiKeyPresent && !usingTrial && !roomOwnerRegistered) ||
      (!apiKeyPresent && !usingTrial && !globalInfo.isGlobalActive()) ||
      (!apiKeyPresent && globalInfo.isRestricted()) ||
      roomDisabled ||
      (isMod && moderatorDisabled) ||
      (!isMod && !isOwner && participantDisabled) ||
      (!isOwner && usageTimeOver);
  }

  public GPTGlobalAccessInfo getGlobalInfo() {
    return globalInfo;
  }

  public boolean isRoomOwnerRegistered() {
    return roomOwnerRegistered;
  }

  public boolean isApiKeyPresent() {
    return apiKeyPresent;
  }

  public boolean isUsingTrial() {
    return usingTrial;
  }

  public boolean isRoomDisabled() {
    return roomDisabled;
  }

  public boolean isModeratorDisabled() {
    return moderatorDisabled;
  }

  public boolean isParticipantDisabled() {
    return participantDisabled;
  }

  public boolean isUsageTimeOver() {
    return usageTimeOver;
  }

  public boolean isRestricted() {
    return restricted;
  }

  @Override
  public String toString() {
    return (
      "GPTRoomAccessInfo [globalInfo=" +
      globalInfo +
      ", roomOwnerRegistered=" +
      roomOwnerRegistered +
      ", apiKeyPresent=" +
      apiKeyPresent +
      ", usingTrial=" +
      usingTrial +
      ", roomDisabled=" +
      roomDisabled +
      ", moderatorDisabled=" +
      moderatorDisabled +
      ", participantDisabled=" +
      participantDisabled +
      ", usageTimeOver=" +
      usageTimeOver +
      ", restricted=" +
      restricted +
      "]"
    );
  }
}
