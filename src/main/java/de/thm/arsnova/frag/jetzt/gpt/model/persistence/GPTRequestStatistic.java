package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTChatCompletionMessage;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.Nullable;

@Table
public class GPTRequestStatistic implements Persistable<UUID> {

  private static final ObjectMapper jsonMapper = new ObjectMapper();
  private static final Logger logger = LoggerFactory.getLogger(
    GPTRequestStatistic.class
  );

  @Id
  private UUID id;

  private UUID requesterId;
  private UUID roomId;
  private String prompt;
  private String completion;
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  public static String messagesToString(List<GPTChatCompletionMessage> prompt) {
    try {
      StringBuilder b = new StringBuilder();
      for (GPTChatCompletionMessage m : prompt) {
        if (b.length() > 0) {
          b.append('\n');
        }
        b.append(jsonMapper.writeValueAsString(m));
      }
      return b.toString();
    } catch (JsonProcessingException e) {
      logger.error("Could not create GPTRequestStatistic!", e);
      return "";
    }
  }

  public GPTRequestStatistic() {}

  public GPTRequestStatistic(
    UUID requesterId,
    UUID roomId,
    String prompt,
    String completion
  ) {
    this.requesterId = requesterId;
    this.roomId = roomId;
    this.prompt = prompt;
    this.completion = completion;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  @Nullable
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getRequesterId() {
    return requesterId;
  }

  public void setRequesterId(UUID requesterId) {
    this.requesterId = requesterId;
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public String getPrompt() {
    return prompt;
  }

  public void setPrompt(String prompt) {
    this.prompt = prompt;
  }

  public String getCompletion() {
    return completion;
  }

  public void setCompletion(String completion) {
    this.completion = completion;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result =
      prime * result + ((requesterId == null) ? 0 : requesterId.hashCode());
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result = prime * result + ((prompt == null) ? 0 : prompt.hashCode());
    result =
      prime * result + ((completion == null) ? 0 : completion.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTRequestStatistic other = (GPTRequestStatistic) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (requesterId == null) {
      if (other.requesterId != null) return false;
    } else if (!requesterId.equals(other.requesterId)) return false;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (prompt == null) {
      if (other.prompt != null) return false;
    } else if (!prompt.equals(other.prompt)) return false;
    if (completion == null) {
      if (other.completion != null) return false;
    } else if (!completion.equals(other.completion)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTRequestStatistic [id=" +
      id +
      ", requesterId=" +
      requesterId +
      ", roomId=" +
      roomId +
      ", prompt=" +
      prompt +
      ", completion=" +
      completion +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
