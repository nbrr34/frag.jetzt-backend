package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.RequestParams.LoginCredentials;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.security.JwtUtil;
import de.thm.arsnova.frag.jetzt.backend.security.SecurityContextRepository;
import de.thm.arsnova.frag.jetzt.backend.service.AccountService;
import de.thm.arsnova.frag.jetzt.backend.service.RoomAccessService;
import de.thm.arsnova.frag.jetzt.backend.util.DictionaryReader;
import de.thm.arsnova.frag.jetzt.backend.util.DictionaryReader.PasswordFoundRange;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.UnauthorizedException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController("AuthenticationController")
@RequestMapping(AuthenticationController.REQUEST_MAPPING)
public class AuthenticationController extends AbstractEntityController {

  private static final Logger logger = LoggerFactory.getLogger(
    AuthenticationController.class
  );

  protected static final String REQUEST_MAPPING = "/auth";
  private static final String LOGIN_MAPPING = "/login";
  private static final String PASSWORD_DICT_MAPPING =
    "/password-dictionary/{password}";

  private final ArrayList<DictionaryReader> dictionaries = new ArrayList<>();
  private final SecurityContextRepository securityContextRepository;
  private final AccountService accountService;
  private final RoomAccessService roomAccessService;
  private final JwtUtil jwtUtil;

  @Autowired
  public AuthenticationController(
    SecurityContextRepository securityContextRepository,
    AccountService accountService,
    RoomAccessService roomAccessService,
    JwtUtil jwtUtil,
    ResourceLoader loader
  ) {
    this.securityContextRepository = securityContextRepository;
    this.accountService = accountService;
    this.roomAccessService = roomAccessService;
    this.jwtUtil = jwtUtil;
    this.loadDictionaries(loader);
  }

  @PostMapping(LOGIN_MAPPING)
  public Mono<AuthenticatedUser> login(
    @RequestParam(defaultValue = "false") final boolean refresh
  ) {
    return securityContextRepository
      .getAuthenticatedUser(refresh)
      .switchIfEmpty(Mono.error(new UnauthorizedException("Access denied")))
      .flatMap(user ->
        accountService.refreshLogin(user.getAccountId(), Mono.just(user))
      );
  }

  @PostMapping(PASSWORD_DICT_MAPPING)
  public Mono<ArrayList<PasswordFoundRange>> checkDictionary(
    @PathVariable String password
  ) {
    ArrayList<PasswordFoundRange> foundRanges = new ArrayList<>();
    dictionaries.forEach(dict -> dict.find(password, foundRanges));
    return Mono.just(foundRanges);
  }

  @PostMapping(LOGIN_MAPPING + "/registered")
  public Mono<AuthenticatedUser> loginRegistered(
    @RequestBody final LoginCredentials loginCredentials
  ) {
    return accountService
      .getFragJetztCredentialsByEmail(loginCredentials.getLoginId())
      // Check password
      .filter(fragJetztCredentials ->
        accountService.checkPassword(
          fragJetztCredentials.getPassword(),
          loginCredentials.getPassword()
        )
      )
      .switchIfEmpty(Mono.error(new UnauthorizedException("DisabledException")))
      .filter(fragJetztCredentials ->
        fragJetztCredentials.getActivationKey() == null
      )
      .switchIfEmpty(
        Mono.error(new ForbiddenException("Activation in process"))
      )
      .filter(fragJetztCredentials ->
        fragJetztCredentials.getPasswordResetKey() == null
      )
      .switchIfEmpty(
        Mono.error(new ForbiddenException("Password reset in process"))
      )
      .filter(fragJetztCredentials -> !fragJetztCredentials.isPasswordExpired())
      .switchIfEmpty(Mono.error(new ForbiddenException("Password expired")))
      .flatMap(fragJetztCredentials ->
        accountService.getByEmail(fragJetztCredentials.getEmail())
      )
      .filter(account -> account.getId() != null)
      .switchIfEmpty(
        Mono.error(new UnauthorizedException("Account not in Database"))
      )
      .flatMap(account ->
        roomAccessService
          .getByAccountId(account.getId())
          .collectList()
          .map(roomAccesses ->
            new AuthenticatedUser(
              jwtUtil.generateToken(account.getId(), "registered"),
              account.getId().toString(),
              "registered",
              account.getEmail(),
              roomAccesses,
              Collections.emptyList()
            )
          )
          .flatMap(user ->
            accountService.refreshLogin(user.getAccountId(), Mono.just(user))
          )
      );
  }

  @PostMapping(LOGIN_MAPPING + "/guest")
  public Mono<AuthenticatedUser> postLoginGuest() {
    return accountService
      .createGuest()
      .map(account ->
        new AuthenticatedUser(
          jwtUtil.generateToken(account.getId(), "guest"),
          account.getId().toString(),
          "guest",
          account.getEmail(),
          // emptyList as RoomAccesses, because a new User doesn´t have them
          Collections.emptyList(),
          Collections.emptyList()
        )
      );
  }

  private void loadDictionaries(ResourceLoader loader) {
    long m1 =
      Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    try {
      Resource[] resources = ResourcePatternUtils.getResourcePatternResolver(loader)
      .getResources("classpath:security/*.dict");
      for (Resource resource : resources) {
        logger.info("Loading Dictionary " + resource.getFilename());
        dictionaries.add(new DictionaryReader(resource));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    long m2 =
      Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    float d = (float) ((m2 - m1) / (1024.0 * 1024.0));
    logger.info("Dictionarys size: " + d + " MiB");
  }
}
