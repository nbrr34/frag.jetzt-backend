package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GPTModerationResponse {

  public static class GPTModerationResponseResult {

    private Map<String, Boolean> categories;
    private Map<String, Double> categoryScores;
    private boolean flagged;

    public Map<String, Boolean> getCategories() {
      return categories;
    }

    public void setCategories(Map<String, Boolean> categories) {
      this.categories =
        categories != null ? Collections.unmodifiableMap(categories) : null;
    }

    @JsonProperty("categoryScores")
    public Map<String, Double> getCategoryScores() {
      return categoryScores;
    }

    @JsonProperty("category_scores")
    public void setCategoryScores(Map<String, Double> categoryScores) {
      this.categoryScores =
        categoryScores != null
          ? Collections.unmodifiableMap(categoryScores)
          : null;
    }

    public boolean isFlagged() {
      return flagged;
    }

    public void setFlagged(boolean flagged) {
      this.flagged = flagged;
    }
  }

  private String id;
  private String model;
  private List<GPTModerationResponseResult> results;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public List<GPTModerationResponseResult> getResults() {
    return results;
  }

  public void setResults(List<GPTModerationResponseResult> results) {
    this.results =
      results != null ? Collections.unmodifiableList(results) : null;
  }
}
