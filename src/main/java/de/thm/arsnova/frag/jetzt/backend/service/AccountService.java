package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Account;
import de.thm.arsnova.frag.jetzt.backend.model.FragJetztCredentials;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.AccountRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.FragJetztCredentialRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.UnauthorizedException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.UserNotFoundException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Pattern;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class AccountService {

  private static final Logger logger = LoggerFactory.getLogger(
    AccountService.class
  );

  @Value("${server.root-url}")
  private String rootUrl;

  @Value("${app.mail.sender-name}")
  private String mailSenderName;

  @Value("${app.mail.sender-address}")
  private String mailSenderAddress;

  @Value("${app.registration.domains}")
  private String[] allowedEmailDomains; // = new ArrayList<>(Collections.singletonList("*"));

  @Value("${app.registration.registermailsubject}")
  private String registerMailSubject;

  @Value("${app.registration.registernmailbody}")
  private String registerMailBody;

  @Value("${app.registration.resetpasswordmailsubject}")
  private String resetPasswordMailSubject;

  @Value("${app.registration.resetpasswordmailbody}")
  private String resetPasswordMailBody;

  @Value("${app.registration.resetpasswordexpiration}")
  private Integer resetPasswordExpiration;

  @Value("${app.registration.activationkeyexpiration}")
  private Integer activationKeyExpiration;

  @Value("${app.credentials.emaillength}")
  private int maxCredentialsMailLength;

  @Value("${app.credentials.passwordlength}")
  private int maxCredentialsPasswordLength;

  private final AuthorizationHelper authorizationHelper;
  private final AccountRepository repository;
  private final FragJetztCredentialRepository fragJetztCredentialRepository;

  private final JavaMailSender mailSender;

  private Pattern mailPattern;
  private BCryptPasswordEncoder encoder;

  @Autowired
  public AccountService(
    AuthorizationHelper authorizationHelper,
    AccountRepository repository,
    FragJetztCredentialRepository fragJetztCredentialRepository,
    JavaMailSender mailSender
  ) {
    this.authorizationHelper = authorizationHelper;
    this.repository = repository;
    this.fragJetztCredentialRepository = fragJetztCredentialRepository;
    this.mailSender = mailSender;
  }

  // Is scheduled for every 10 Minutes
  @Scheduled(fixedDelay = 600000)
  public void deleteAccountsWithMissedActivation() {
    logger.info("Deleting stale accounts");
    final Date expired = new Date();
    expired.setTime(expired.getTime() - activationKeyExpiration * 1000);
    fragJetztCredentialRepository
      .findByActivationKeyTimeLessThanAndActivationKeyIsNotNull(expired)
      .flatMap(fragJetztCredentials -> {
        return fragJetztCredentialRepository
          .delete(fragJetztCredentials)
          .then(repository.deleteById(fragJetztCredentials.getId()));
      })
      .subscribe();
  }

  public Mono<Account> get(final UUID id) {
    return repository.findById(id);
  }

  public Flux<String> getAllEmails() {
    return repository
      .findAll()
      .filter(acc -> acc.getEmail() != null)
      .map(acc -> acc.getEmail());
  }

  public Mono<Boolean> isSuperAdmin() {
    return authorizationHelper
      .getCurrentUser()
      .map(authorizationHelper::isSuperAdmin);
  }

  public Flux<Account> get(final UUID[] id) {
    return repository.findAllById(Arrays.asList(id));
  }

  public Mono<Account> create(final Account account) {
    logger.trace("Creating new account: " + account.toString());
    return repository.save(account);
  }

  public Mono<Account> create(final String email, final String password) {
    final String lcEmail = email.toLowerCase();

    if (mailPattern == null) {
      parseMailAddressPattern();
    }

    if (mailPattern == null || !mailPattern.matcher(lcEmail).matches()) {
      logger.info(
        "User registration failed. {} does not match pattern.",
        lcEmail
      );
      return null;
    }

    FragJetztCredentials credential = new FragJetztCredentials(
      lcEmail,
      encodePassword(password),
      generateRandomString(8),
      new Timestamp(new Date().getTime()),
      null,
      null
    );
    return validateFragJetztCredentials(credential)
      .flatMap(fragJetztCredentials -> repository.findByEmailIgnoreCase(lcEmail)
      )
      .switchIfEmpty(Mono.just(new Account(null, null, null)))
      .flatMap(account -> {
        if (account.getId() == null) {
          return fragJetztCredentialRepository.save(credential);
        } else {
          return Mono.error(new ForbiddenException("User already exists"));
        }
      })
      .flatMap(fragJetztCredentials -> {
        final Account account = new Account();
        account.setEmail(fragJetztCredentials.getEmail());
        return repository.save(account);
      })
      .doOnSuccess(account -> sendActivationEmail(credential));
  }

  public Mono<Account> createGuest() {
    Account account = new Account();
    logger.trace("Creating new guest account: " + account);
    return repository.save(account);
  }

  public Mono<Account> update(final Account account) {
    return authorizationHelper
      .getCurrentUser()
      .filter(authenticatedUser ->
        authenticatedUser.getAccountId().equals(account.getId())
      )
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(authenticatedUser -> repository.save(account));
  }

  public Mono<Void> delete(final UUID id) {
    return authorizationHelper
      .getCurrentUser()
      .filter(authenticatedUser -> authenticatedUser.getAccountId().equals(id))
      .switchIfEmpty(Mono.error(new ForbiddenException()))
      .flatMap(authenticatedUser -> {
        if (
          authenticatedUser.getName() != null
        ) return fragJetztCredentialRepository.deleteByEmail(
          authenticatedUser.getName()
        ); else return repository.deleteById(authenticatedUser.getAccountId());
      });
  }

  public Mono<Account> getByEmail(final String email) {
    return repository.findByEmailIgnoreCase(email);
  }

  public Mono<FragJetztCredentials> getFragJetztCredentialsByEmail(
    String email
  ) {
    return fragJetztCredentialRepository.findByEmailIgnoreCase(email);
  }

  public Mono<FragJetztCredentials> activateAccount(
    final String email,
    final String key,
    final String clientAddress
  ) {
    /*if (isBannedFromLogin(clientAddress)) {
            return Mono.error(new ForbiddenException("Banned from login"));
        }*/
    return fragJetztCredentialRepository
      .findByEmailIgnoreCase(email)
      .switchIfEmpty(Mono.error(new UserNotFoundException()))
      .flatMap(fragJetztCredentials -> {
        if (!fragJetztCredentials.getActivationKey().equals(key)) {
          return Mono.error(new ForbiddenException("Wrong activation key"));
        } else {
          fragJetztCredentials.setActivationKey(null);
          return fragJetztCredentialRepository.save(fragJetztCredentials);
        }
      });
  }

  public Mono<Void> initiatePasswordReset(final String email) {
    return fragJetztCredentialRepository
      .findByEmailIgnoreCase(email)
      .filter(fragJetztCredentials -> fragJetztCredentials.getId() != null)
      .switchIfEmpty(
        Mono.error(new BadRequestException("Account does not exist"))
      )
      .filter(fragJetztCredentials ->
        fragJetztCredentials.getActivationKey() == null
      )
      .switchIfEmpty(
        Mono.error(new BadRequestException("Account has activation key set"))
      )
      .flatMap(fragJetztCredentials -> {
        if (
          fragJetztCredentials.getPasswordResetTime() != null &&
          !keyExpired(
            fragJetztCredentials.getPasswordResetTime().getTime(),
            resetPasswordExpiration
          )
        ) {
          return Mono.error(new BadRequestException());
        } else {
          fragJetztCredentials.setPasswordResetKey(generateRandomString(8));
          fragJetztCredentials.setPasswordResetTime(
            new Timestamp(new Date().getTime())
          );
          return fragJetztCredentialRepository.save(fragJetztCredentials);
        }
      })
      .map(fragJetztCredentials -> {
        sendPasswortResetMail(fragJetztCredentials);
        return fragJetztCredentials;
      })
      .then();
  }

  public Mono<Void> resetPassword(
    final String email,
    final String key,
    final String password
  ) {
    return fragJetztCredentialRepository
      .findByEmailIgnoreCase(email)
      .map(fragJetztCredentials -> {
        if (
          fragJetztCredentials.getPasswordResetKey() == null ||
          !fragJetztCredentials.getPasswordResetKey().equals(key)
        ) {
          logger.info(
            "Password reset failed. Invalid key provided for User {}.",
            fragJetztCredentials.getEmail()
          );
          throw new UnauthorizedException("Invalid Key");
        } else if (
          keyExpired(
            fragJetztCredentials.getPasswordResetTime().getTime(),
            resetPasswordExpiration
          )
        ) {
          fragJetztCredentials.setPasswordResetKey(null);
          throw new UnauthorizedException("Key expired");
        } else if (
          checkPassword(fragJetztCredentials.getPassword(), password)
        ) {
          throw new ForbiddenException("New password is old password");
        } else {
          return fragJetztCredentials;
        }
      })
      .map(credentials -> {
        credentials.setPassword(encodePassword(password));
        credentials.setPasswordResetKey(null);
        credentials.setPasswordResetTime(null);
        credentials.resetPasswordExpiry();
        return credentials;
      })
      .flatMap(this::validateFragJetztCredentials)
      .flatMap(fragJetztCredentialRepository::save)
      .then();
  }

  public Mono<Void> resetActivation(final String email) {
    return fragJetztCredentialRepository
      .findByEmailIgnoreCase(email)
      .filter(fragJetztCredentials ->
        fragJetztCredentials.getPasswordResetKey() == null
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException("Account has password reset key set")
        )
      )
      .map(fragJetztCredentials -> {
        fragJetztCredentials.setActivationKey(generateRandomString(8));
        fragJetztCredentials.setActivationKeyTime(
          new Timestamp(new Date().getTime())
        );
        return fragJetztCredentials;
      })
      .flatMap(fragJetztCredentialRepository::save)
      .doOnSuccess(this::sendActivationEmail)
      .then();
  }

  private String encodePassword(final String password) {
    if (null == encoder) {
      encoder = new BCryptPasswordEncoder(12);
    }
    return encoder.encode(password);
  }

  public boolean checkPassword(final String encodedPass, final String pass) {
    if (null == encoder) {
      encoder = new BCryptPasswordEncoder(12);
    }
    return encoder.matches(pass, encodedPass);
  }

  public <T> Mono<T> refreshLogin(UUID accountId, Mono<T> t) {
    return repository
      .findById(accountId)
      .flatMap(user -> {
        user.refreshLastLogin();
        user.refreshLastActive();
        return repository.save(user);
      })
      .flatMap(account -> t)
      .switchIfEmpty(t);
  }

  public <T> Mono<T> refreshLastActive(UUID accountId, Mono<T> t) {
    return repository
      .findById(accountId)
      .flatMap(user -> {
        user.refreshLastActive();
        return repository.save(user);
      })
      .flatMap(account -> t)
      .switchIfEmpty(t);
  }

  private void parseMailAddressPattern() {
    List<String> allowedEmailDomains = Arrays.asList(this.allowedEmailDomains);
    if (allowedEmailDomains != null && !allowedEmailDomains.isEmpty()) {
      final List<String> patterns = new ArrayList<>();
      if (allowedEmailDomains.contains("*")) {
        patterns.add("([a-z0-9-]+\\.)+[a-z0-9-]+");
      } else {
        final Pattern patternPattern = Pattern.compile(
          "[a-z0-9.*-]+",
          Pattern.CASE_INSENSITIVE
        );
        for (final String patternStr : allowedEmailDomains) {
          if (patternPattern.matcher(patternStr).matches()) {
            patterns.add(
              patternStr
                .replaceAll("[.]", "[.]")
                .replaceAll("[*]", "[a-z0-9-]+?")
            );
          }
        }
      }

      mailPattern =
        Pattern.compile(
          "[a-z0-9._-]+?@(" + String.join("|", patterns) + ")",
          Pattern.CASE_INSENSITIVE
        );
      logger.info(
        "Allowed e-mail addresses (pattern) for registration: '{}'.",
        mailPattern.pattern()
      );
    }
  }

  private String generateRandomString(int length) {
    String characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

    StringBuilder sb = new StringBuilder(length);
    for (int i = 0; i < sb.capacity(); i++) {
      int idx = (int) (characters.length() * Math.random());
      sb.append(characters.charAt(idx));
    }
    return sb.toString();
  }

  private void sendActivationEmail(
    final FragJetztCredentials fragJetztCredentials
  ) {
    Date resetDate = new Date();
    resetDate.setTime(
      fragJetztCredentials.getActivationKeyTime().getTime() +
      activationKeyExpiration *
      1000
    );
    sendEmail(
      fragJetztCredentials.getEmail(),
      registerMailSubject,
      MessageFormat.format(
        registerMailBody,
        fragJetztCredentials.getActivationKey(),
        resetDate,
        rootUrl
      )
    );
  }

  private void sendPasswortResetMail(
    FragJetztCredentials fragJetztCredentials
  ) {
    String key = fragJetztCredentials.getPasswordResetKey();
    Date resetDate = new Date();
    resetDate.setTime(
      fragJetztCredentials.getPasswordResetTime().getTime() +
      resetPasswordExpiration *
      1000
    );
    sendEmail(
      fragJetztCredentials.getEmail(),
      resetPasswordMailSubject,
      MessageFormat.format(resetPasswordMailBody, key, resetDate, rootUrl)
    );
  }

  private void sendEmail(String destMail, String subject, String body) {
    final MimeMessage msg = mailSender.createMimeMessage();
    final MimeMessageHelper helper = new MimeMessageHelper(
      msg,
      StandardCharsets.UTF_8.displayName()
    );
    try {
      helper.setFrom(mailSenderName + "<" + mailSenderAddress + ">");
      helper.setTo(destMail);
      helper.setSubject(subject);
      helper.setText(body);

      logger.info(
        "Sending mail \"{}\" from \"{}\" to \"{}\"",
        registerMailSubject,
        msg.getFrom(),
        destMail
      );
      mailSender.send(msg);
    } catch (final MailException | MessagingException e) {
      logger.warn("Mail \"{}\" could not be sent.", registerMailSubject, e);
    }
  }

  private boolean keyExpired(long creationTime, long expiresAfterSeconds) {
    return (
      System.currentTimeMillis() > creationTime + expiresAfterSeconds * 1000
    );
  }

  private Mono<FragJetztCredentials> validateFragJetztCredentials(
    FragJetztCredentials credentials
  ) {
    return Mono
      .just(credentials)
      .filter(c -> c.getEmail().length() <= maxCredentialsMailLength)
      .switchIfEmpty(
        Mono.error(new BadRequestException("Maximal email length exceeded"))
      )
      .filter(c -> c.getPassword().length() <= maxCredentialsPasswordLength)
      .switchIfEmpty(
        Mono.error(new BadRequestException("Maximal password length exceeded"))
      );
  }
}
