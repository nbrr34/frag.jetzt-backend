package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.CommentChange;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.CommentChangeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

@Service
public class CommentChangeService {

    private static final Logger logger = LoggerFactory.getLogger(CommentChangeService.class);

    private final CommentChangeRepository repository;

    @Autowired
    public CommentChangeService(
            CommentChangeRepository repository
    ) {
        this.repository = repository;
    }

    // runs every hour on second 0 and minute 0
    @Scheduled(cron = "0 0 * * * *")
    public void deleteOutdatedChanges() {
        final Timestamp timestamp = Timestamp.from(Instant.now().minus(8, ChronoUnit.DAYS));
        repository.deleteByCreatedAtLessThanEqual(timestamp).subscribe();
    }

    public Mono<CommentChange> get(UUID id) {
        return repository.findById(id);
    }

    public Mono<CommentChange> create(CommentChange change) {
        return repository.save(change);
    }

    public Flux<CommentChange> createAll(List<CommentChange> changes) {
        return repository.saveAll(changes);
    }

    public Flux<CommentChange> receiveMissedRoomInformationSince(List<UUID> roomIds, Timestamp lastJoin) {
        if (roomIds.size() < 1) {
            return Flux.empty();
        }
        return repository.findByRoomIdInAndTypeAndCreatedAtGreaterThanEqual(roomIds,
                CommentChange.CommentChangeType.CREATED, lastJoin);
    }

    public Flux<CommentChange> receiveMissedCommentInformationSince(List<UUID> commentIds, Timestamp lastJoin) {
        if (commentIds.size() < 1) {
            return Flux.empty();
        }
        return repository.findByCommentIdInAndTypeIsNotAndCreatedAtGreaterThanEqual(commentIds,
                CommentChange.CommentChangeType.CREATED, lastJoin);
    }

    public Mono<List<CommentChange>> getChangesForNotification(UUID roomId, Timestamp lastNotification) {
        return repository.findByRoomIdAndCreatedAtGreaterThanEqual(roomId, lastNotification).collectList();
    }


}
