package de.thm.arsnova.frag.jetzt.backend.model.event.livepoll;

import java.util.UUID;

public class LivepollResult extends LivepollEvent<LivepollResultPayload> {
    
    public LivepollResult() {
        super(LivepollResult.class.getSimpleName());
    }

    public LivepollResult(LivepollResultPayload payload, UUID livepollId) {
        super(LivepollResult.class.getSimpleName());
        this.payload = payload;
        this.livepollId = livepollId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LivepollResult that = (LivepollResult) o;
        return this.getPayload().equals(that.getPayload());
    }

}
