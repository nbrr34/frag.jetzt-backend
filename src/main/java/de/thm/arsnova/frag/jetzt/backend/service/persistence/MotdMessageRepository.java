package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.MotdMessage;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.Collection;
import java.util.UUID;

public interface MotdMessageRepository extends ReactiveCrudRepository<MotdMessage, UUID> {

    Flux<MotdMessage> findAllByMotdIdIn(Collection<UUID> uuids);

}
