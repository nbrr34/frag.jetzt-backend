package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Account;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface AccountRepository extends ReactiveCrudRepository<Account, UUID> {
    Mono<Account> findByEmailIgnoreCase(String email);
}
