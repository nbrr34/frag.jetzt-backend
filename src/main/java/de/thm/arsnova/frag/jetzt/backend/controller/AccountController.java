package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.Account;
import de.thm.arsnova.frag.jetzt.backend.model.RequestParams.LoginCredentials;
import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import de.thm.arsnova.frag.jetzt.backend.service.AccountFindQueryService;
import de.thm.arsnova.frag.jetzt.backend.service.AccountService;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.service.RoomAccessService;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController("AccountController")
@RequestMapping("/user")
public class AccountController extends AbstractEntityController {

    static class PasswordReset {
        private String key;
        private String password;

        public String getKey() {
            return key;
        }

        public void setKey(final String key) {
            this.key = key;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(final String password) {
            this.password = password;
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    protected static final String REQUEST_MAPPING = "/user";
    private static final String REGISTER_MAPPING = "/register";
    private static final String SUPER_ADMIN_MAPPING = "/super-admin";
    private static final String ACTIVATE_MAPPING = DEFAULT_ALIAS_MAPPING + "/activate";
    private static final String RESET_ACTIVATE_MAPPING = DEFAULT_ALIAS_MAPPING + "/resetactivation";
    private static final String RESET_PASSWORD_MAPPING = DEFAULT_ID_MAPPING + "/resetpassword";
    private static final String ROOM_HISTORY_MAPPING = DEFAULT_ID_MAPPING + "/roomHistory";
    private static final String ROOM_HISTORY_DELETE_MAPPING = ROOM_HISTORY_MAPPING + "/{roomId}";
    private static final String GET_ROOM_ACCESS_MAPPING = DEFAULT_ID_MAPPING + "/roomAccess";

    private final AccountService service;
    private final AccountFindQueryService findQueryService;
    private final RoomAccessService roomAccessService;

    @Autowired
    public AccountController(
            AccountService service,
            AccountFindQueryService findQueryService,
            RoomAccessService roomAccessService
    ) {
        this.service = service;
        this.findQueryService = findQueryService;
        this.roomAccessService = roomAccessService;
    }

    @PostMapping(REGISTER_MAPPING)
    public Mono<Void> register(@RequestBody final LoginCredentials credentials) {
        return service.create(credentials.getLoginId(), credentials.getPassword())
                .onErrorResume(e -> Mono.error(new ForbiddenException("User already exists")))
                .then();
    }

    @PostMapping(ACTIVATE_MAPPING)
    public Mono<Void> activate(@PathVariable final String alias, @RequestParam final String key, ServerHttpRequest request) {
        return service.activateAccount(alias, key, request.getRemoteAddress().toString())
                .then();
    }

    @PostMapping(RESET_ACTIVATE_MAPPING)
    public Mono<Void> resetactivate(@PathVariable final String alias, ServerHttpRequest request) {
        return service.resetActivation(alias)
                .then();
    }

    @PostMapping(RESET_PASSWORD_MAPPING)
    public Mono<Void> resetPassword(@PathVariable final String id, @RequestBody final PasswordReset passwordReset) {
        if (passwordReset.getKey() != null) {
            if (passwordReset.getPassword() == null) return Mono.error(new BadRequestException("No Password provided"));
            return service.resetPassword(id, passwordReset.getKey(), passwordReset.getPassword())
                    .then();
        } else {
            return service.initiatePasswordReset(id)
                    .then();
        }
    }

    @GetMapping
    public Flux<Account> get(@RequestParam UUID[] ids) {
        return service.get(ids)
                .switchIfEmpty(Mono.error(NotFoundException::new));
    }

    @GetMapping(GET_MAPPING)
    public Mono<Account> get(@PathVariable UUID id) {
        return service.get(id)
                .switchIfEmpty(Mono.error(NotFoundException::new));
    }

    @GetMapping(SUPER_ADMIN_MAPPING)
    public Mono<Boolean> getSuperAdmin() {
        return service.isSuperAdmin();
    }

    @GetMapping(GET_ROOM_ACCESS_MAPPING)
    public Flux<RoomAccess> getRoomAccess(@PathVariable UUID id) {
        return roomAccessService.getByAccountId(id);
    }

    @PutMapping(PUT_MAPPING)
    public Mono<Account> put(@RequestBody final Account entity) {
        return service.update(entity);
    }

    @PostMapping(POST_MAPPING)
    public Mono<Account> post(@RequestBody final Account entity) {
        return service.create(entity);
    }

    @DeleteMapping(DELETE_MAPPING)
    public Mono<Void> delete(@PathVariable final UUID id) {
        return service.delete(id);
    }

    @PostMapping(FIND_MAPPING)
    public Flux<Account> find(@RequestBody final FindQuery<Account> findQuery) {
        logger.debug("Resolving find query: {}", findQuery);
        return findQueryService.resolveQuery(findQuery);
    }

    @PostMapping(ROOM_HISTORY_MAPPING)
    public Mono<RoomAccess> postRoomHistoryEntry(@PathVariable final UUID id, @RequestBody final RoomAccess roomAccess) {
        roomAccess.setAccountId(id);
        return roomAccessService.createRoomHistory(roomAccess);
    }

    @DeleteMapping(ROOM_HISTORY_DELETE_MAPPING)
    public Mono<Void> deleteRoomHistoryEntry(@PathVariable final UUID id, @PathVariable final UUID roomId) {
        return roomAccessService.delete(roomId, id);
    }
}
