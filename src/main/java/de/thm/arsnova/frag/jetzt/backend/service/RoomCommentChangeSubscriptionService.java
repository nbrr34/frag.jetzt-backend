package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.RoomCommentChangeSubscription;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomCommentChangeSubscriptionRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class RoomCommentChangeSubscriptionService {

    private static final Logger logger = LoggerFactory.getLogger(RoomCommentChangeSubscriptionService.class);

    private final RoomCommentChangeSubscriptionRepository repository;
    private final AuthorizationHelper authorizationHelper;
    private final RoomService roomService;

    @Autowired
    public RoomCommentChangeSubscriptionService(
            final RoomCommentChangeSubscriptionRepository repository,
            final AuthorizationHelper authorizationHelper,
            final RoomService roomService
    ) {
        this.repository = repository;
        this.authorizationHelper = authorizationHelper;
        this.roomService = roomService;
    }

    public Flux<RoomCommentChangeSubscription> get() {
        return authorizationHelper.getCurrentUser()
                .flatMapMany(user -> repository.findByAccountId(user.getAccountId()));
    }

    public Mono<RoomCommentChangeSubscription> create(UUID roomId) {
        return Mono.zip(authorizationHelper.getCurrentUser(), roomService.get(roomId))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .map(tuple2 -> {
                    RoomCommentChangeSubscription subscription = new RoomCommentChangeSubscription();
                    subscription.setAccountId(tuple2.getT1().getAccountId());
                    subscription.setRoomId(tuple2.getT2().getId());
                    return subscription;
                })
                .flatMap(repository::save);
    }

    public Mono<Void> deleteByRoomId(UUID roomId) {
        return authorizationHelper.getCurrentUser()
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(user -> repository.deleteByAccountIdAndRoomId(user.getAccountId(), roomId));
    }

}
