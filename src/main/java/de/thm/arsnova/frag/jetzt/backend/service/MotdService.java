package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Motd;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.MotdMessageRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.MotdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

@Service
public class MotdService {

    private final MotdRepository repository;
    private final MotdMessageRepository messageRepository;
    private final AuthorizationHelper authorizationHelper;

    @Autowired
    public MotdService(
            MotdRepository repository,
            MotdMessageRepository messageRepository,
            AuthorizationHelper authorizationHelper
    ) {
        this.repository = repository;
        this.messageRepository = messageRepository;
        this.authorizationHelper = authorizationHelper;
    }

    public Mono<Motd> create(Motd motd) {
        return Mono.zip(Mono.just(new Motd(motd.getStartTimestamp(), motd.getEndTimestamp(), motd.getMessages())),
                        authorizationHelper.getCurrentUser())
                .filter(tuple2 -> authorizationHelper.isSuperAdmin(tuple2.getT2()))
                .switchIfEmpty(Mono.error(new IllegalArgumentException("No Super admin rights!")))
                .map(Tuple2::getT1)
                .filter(m -> m.getMessages() != null && m.getMessages().size() > 0 &&
                        m.getMessages().keySet().stream().allMatch(Objects::nonNull))
                .switchIfEmpty(Mono.error(new IllegalArgumentException("Messages are not present!")))
                .filter(m -> m.getStartTimestamp() != null && m.getEndTimestamp() != null)
                .switchIfEmpty(Mono.error(new IllegalArgumentException("Timestamps are null!")))
                .filter(m -> m.getEndTimestamp().after(Timestamp.from(Instant.now())))
                .switchIfEmpty(Mono.error(new IllegalArgumentException("End of Motd is already reached!")))
                .flatMap(m -> repository.save(m).flatMap(newMotd -> Flux.fromIterable(m.getMessages().values())
                        .flatMap(msg -> {
                            msg.setMotdId(newMotd.getId());
                            return messageRepository.save(msg);
                        })
                        .collectList()
                        .map(messages -> {
                            messages.forEach(msg -> newMotd.getMessages().put(msg.getLanguage(), msg));
                            return newMotd;
                        })));
    }

    public Flux<Motd> getActiveMotds(long time) {
        Timestamp timestamp = new Timestamp(time);
        return applyTransientFields(repository
                .findByStartTimestampLessThanEqualAndEndTimestampGreaterThanEqual(timestamp, timestamp));
    }

    public Flux<Motd> getMotdsBefore(long before) {
        return applyTransientFields(repository.findByEndTimestampLessThan(new Timestamp(before)));
    }

    private Flux<Motd> applyTransientFields(Flux<Motd> motdFlux) {
        return motdFlux.collectList()
                .flatMapMany(motds -> {
                    HashMap<UUID, Motd> map = new HashMap<>();
                    motds.forEach(motd -> {
                        map.put(motd.getId(), motd);
                        motd.setMessages(new HashMap<>());
                    });
                    return messageRepository.findAllByMotdIdIn(map.keySet())
                            .collectList()
                            .flatMapMany(list -> {
                                list.forEach(message -> map.get(message.getMotdId())
                                        .getMessages().put(message.getLanguage(), message));
                                return Flux.fromIterable(motds);
                            });
                });
    }

}
