package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GPTChatCompletionChoice implements IGPTCompletionChoice {

  private GPTChatCompletionMessage message;
  private int index;
  private String finishReason;

  public GPTChatCompletionMessage getMessage() {
    return message;
  }

  public void setMessage(GPTChatCompletionMessage message) {
    this.message = message;
  }

  @Override
  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  @JsonProperty("finish_reason")
  @Override
  public String getFinishReason() {
    return finishReason;
  }

  public void setFinishReason(String finishReason) {
    this.finishReason = finishReason;
  }
}
