package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Rating;
import de.thm.arsnova.frag.jetzt.backend.model.RatingResult;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RatingRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class RatingService {

    private final RatingRepository repository;
    private final AuthorizationHelper authorizationHelper;

    @Autowired
    public RatingService(
            AuthorizationHelper authorizationHelper,
            RatingRepository repository
    ) {
        this.authorizationHelper = authorizationHelper;
        this.repository = repository;
    }

    public Mono<Rating> create(Rating entity) {
        return getByAccountId(entity.getAccountId())
                .switchIfEmpty(Mono.just(entity))
                .flatMap(r -> {
                    entity.setId(r.getId());
                    return Mono.just(entity);
                })
                .filter(r -> r.getRating() >= 0 && r.getRating() <= 5 && r.getAccountId() != null)
                .switchIfEmpty(Mono.error(IllegalArgumentException::new))
                .flatMap(this.repository::save);
    }

    public Mono<RatingResult> getRatings() {
        return this.repository.getRatings();
    }

    public Mono<Rating> getByAccountId(UUID userId) {
        return this.authorizationHelper.getCurrentUser()
                .filter(user -> user.getAccountId().equals(userId))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(user -> this.repository.findByAccountId(userId));
    }

    public Mono<Void> deleteByAccountId(UUID userId) {
        return this.authorizationHelper.getCurrentUser()
                .filter(user -> user.getAccountId().equals(userId))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(user -> this.repository.deleteByAccountId(userId));
    }

}
