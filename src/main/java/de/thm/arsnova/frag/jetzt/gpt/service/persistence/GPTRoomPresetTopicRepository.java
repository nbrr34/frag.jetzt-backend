package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomPresetTopic;
import reactor.core.publisher.Flux;

@Repository
public interface GPTRoomPresetTopicRepository
  extends ReactiveCrudRepository<GPTRoomPresetTopic, UUID> {
  Flux<GPTRoomPresetTopic> findAllBySettingId(UUID settingId);

  Flux<Void> deleteAllBySettingId(UUID settingId);
}
