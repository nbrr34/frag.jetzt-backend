package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Table
public class CommentChange implements Persistable<UUID> {

    public enum CommentChangeType {
        CREATED,
        DELETED,
        ANSWERED,
        CHANGE_ACK,
        CHANGE_FAVORITE,
        CHANGE_CORRECT,
        CHANGE_TAG,
        CHANGE_SCORE
    }

    public enum UserRole {
        PARTICIPANT,
        EDITING_MODERATOR,
        EXECUTIVE_MODERATOR,
        CREATOR;

        public static UserRole fromRole(RoomAccess.Role role) {
            switch (role) {
                case PARTICIPANT:
                    return PARTICIPANT;
                case EDITING_MODERATOR:
                    return EDITING_MODERATOR;
                case EXECUTIVE_MODERATOR:
                    return EXECUTIVE_MODERATOR;
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

    @Id
    private UUID id;
    private UUID commentId;
    private UUID roomId;
    private CommentChangeType type;
    private String previousValueString;
    private String currentValueString;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;
    private UUID initiatorId;
    private UserRole initiatorRole;

    public CommentChange() {

    }

    public CommentChange(UUID commentId, UUID roomId, CommentChangeType type, String previousValueString, String currentValueString, UUID initiatorId, UserRole initiatorRole) {
        this.commentId = commentId;
        this.roomId = roomId;
        this.type = type;
        this.previousValueString = previousValueString;
        this.currentValueString = currentValueString;
        this.initiatorId = initiatorId;
        this.initiatorRole = initiatorRole;
    }

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getCommentId() {
        return commentId;
    }

    public void setCommentId(UUID commentId) {
        this.commentId = commentId;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public CommentChangeType getType() {
        return type;
    }

    public void setType(CommentChangeType type) {
        this.type = type;
    }

    public String getPreviousValueString() {
        return previousValueString;
    }

    public void setPreviousValueString(String previousValueString) {
        this.previousValueString = previousValueString;
    }

    public String getCurrentValueString() {
        return currentValueString;
    }

    public void setCurrentValueString(String currentValueString) {
        this.currentValueString = currentValueString;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public UUID getInitiatorId() {
        return initiatorId;
    }

    public void setInitiatorId(UUID initiatorId) {
        this.initiatorId = initiatorId;
    }

    public UserRole getInitiatorRole() {
        return initiatorRole;
    }

    public void setInitiatorRole(UserRole initiatorRole) {
        this.initiatorRole = initiatorRole;
    }

    @Override
    public String toString() {
        return "CommentChange{" +
                "id=" + id +
                ", commentId=" + commentId +
                ", roomId=" + roomId +
                ", type=" + type +
                ", previousValueString='" + previousValueString + '\'' +
                ", currentValueString='" + currentValueString + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", initiatorId=" + initiatorId +
                ", initiatorRole=" + initiatorRole +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentChange that = (CommentChange) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(commentId, that.commentId) &&
                Objects.equals(roomId, that.roomId) &&
                type == that.type &&
                Objects.equals(previousValueString, that.previousValueString) &&
                Objects.equals(currentValueString, that.currentValueString) &&
                Objects.equals(initiatorId, that.initiatorId) &&
                initiatorRole == that.initiatorRole;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, commentId, roomId, type, previousValueString, currentValueString, initiatorId, initiatorRole);
    }
}
