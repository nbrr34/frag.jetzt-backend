package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.InternalServerErrorException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.UnauthorizedException;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig.GPTActivationCode;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting.GPTRoomSettingRight;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomUsageTime;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomUsageTime.GPTRoomUsageRepeatUnit;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRoomSettingRepository;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRoomUsageTimeRepository;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class GPTRoomService {

  public static class UsageTimeAction {

    private UUID deleteId;
    private Integer repeatDuration;
    private GPTRoomUsageRepeatUnit repeatUnit;
    private Timestamp startDate;
    private Timestamp endDate;

    public UUID getDeleteId() {
      return deleteId;
    }

    public void setDeleteId(UUID deleteId) {
      this.deleteId = deleteId;
    }

    public Integer getRepeatDuration() {
      return repeatDuration;
    }

    public void setRepeatDuration(Integer repeatDuration) {
      this.repeatDuration = repeatDuration;
    }

    public GPTRoomUsageRepeatUnit getRepeatUnit() {
      return repeatUnit;
    }

    public void setRepeatUnit(GPTRoomUsageRepeatUnit repeatUnit) {
      this.repeatUnit = repeatUnit;
    }

    public Timestamp getStartDate() {
      return startDate;
    }

    public void setStartDate(Timestamp startDate) {
      this.startDate = startDate;
    }

    public Timestamp getEndDate() {
      return endDate;
    }

    public void setEndDate(Timestamp endDate) {
      this.endDate = endDate;
    }
  }

  private final GPTRoomSettingRepository repository;
  private final RoomRepository roomRepository;
  private final GPTRoomUsageTimeRepository usageTimeRepository;
  private final AuthorizationHelper authorizationHelper;
  private final GPTConfig config;

  @Autowired
  public GPTRoomService(
    final GPTRoomSettingRepository repository,
    final RoomRepository roomRepository,
    final GPTRoomUsageTimeRepository usageTimeRepository,
    final AuthorizationHelper authorizationHelper,
    final GPTConfig config
  ) {
    this.repository = repository;
    this.roomRepository = roomRepository;
    this.usageTimeRepository = usageTimeRepository;
    this.authorizationHelper = authorizationHelper;
    this.config = config;
  }

  public Flux<GPTRoomUsageTime> updateUsageTimes(
    UUID roomId,
    List<UsageTimeAction> actionList
  ) {
    List<UUID> toDelete = new ArrayList<>();
    List<GPTRoomUsageTime> toAdd = new ArrayList<>();
    return repository
      .findByRoomId(roomId)
      .flatMapMany(setting -> {
        for (UsageTimeAction action : actionList) {
          if (action.getDeleteId() != null) {
            toDelete.add(action.getDeleteId());
            continue;
          }
          toAdd.add(
            new GPTRoomUsageTime(
              setting.getId(),
              action.getRepeatDuration(),
              action.getRepeatUnit(),
              action.getStartDate(),
              action.getEndDate()
            )
          );
        }
        Mono<?> actions = Mono.just(true);
        if (!toDelete.isEmpty()) {
          actions =
            actions.flatMap(ignored ->
              usageTimeRepository.deleteAllById(toDelete).then(Mono.just(true))
            );
        }
        if (!toAdd.isEmpty()) {
          actions =
            actions.flatMap(ignored -> {
              return usageTimeRepository.saveAll(toAdd).then(Mono.just(true));
            });
        }
        return actions.flatMapMany(ignored ->
          usageTimeRepository.findAllBySettingId(setting.getId())
        );
      });
  }

  public Mono<Boolean> activateTrial(UUID roomId, String trialCode) {
    return authorizationHelper
      .getCurrentUser()
      .filter(user -> user.getName() != null && user.getName().length() > 0)
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMap(user -> {
        return roomRepository
          .findById(roomId)
          .filter(room -> authorizationHelper.checkCreatorOfRoom(user, room))
          .switchIfEmpty(Mono.error(ForbiddenException::new));
      })
      .flatMap(ignored -> {
        GPTActivationCode found = null;
        for (GPTActivationCode code : config
          .getRestrictions()
          .getPlatformCodes()) {
          if (
            code.getActivatedRoomId() == null &&
            code.getCode().equals(trialCode)
          ) {
            found = code;
            break;
          }
        }
        if (found == null) {
          return Mono.error(ForbiddenException::new);
        }
        final GPTActivationCode code = found;
        return repository
          .findByRoomId(roomId)
          .flatMap(setting -> {
            if (setting.isTrialEnabled()) {
              return Mono.just(false);
            }
            setting.setTrialEnabled(true);
            if (!code.claim(roomId)) {
              return Mono.error(
                new InternalServerErrorException(
                  "TrialCode was claimed from someone other"
                )
              );
            }
            return repository
              .save(setting)
              .onErrorResume(err -> {
                code.unclaim();
                return Mono.error(err);
              })
              .map(ignore -> true);
          });
      });
  }

  public Mono<GPTRoomSetting> patchSettings(
    UUID roomId,
    final Map<String, Object> changes
  ) {
    return Mono
      .zip(
        authorizationHelper.getCurrentUser(),
        roomRepository.findById(roomId),
        repository.findByRoomId(roomId)
      )
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .filter(tuple3 ->
        authorizationHelper.checkModeratorOrCreatorOfRoom(
          tuple3.getT1(),
          tuple3.getT2()
        )
      )
      .switchIfEmpty(Mono.error(UnauthorizedException::new))
      .map(tuple3 ->
        parseChanges(
          tuple3.getT3(),
          changes,
          authorizationHelper.checkCreatorOfRoom(tuple3.getT1(), tuple3.getT2())
        )
      )
      .flatMap(this::validate)
      .flatMap(repository::save)
      .flatMap(ignored -> getByRoomId(roomId));
  }

  public Mono<GPTRoomSetting> createOrGetByRoomId(UUID roomId) {
    return repository
      .findByRoomId(roomId)
      .switchIfEmpty(
        repository
          .save(new GPTRoomSetting(roomId))
          .flatMap(e -> repository.findById(e.getId()))
      );
  }

  public Mono<GPTRoomSetting> getByRoomId(UUID roomId) {
    return Mono
      .zip(
        authorizationHelper.getCurrentUser(),
        roomRepository.findById(roomId)
      )
      .filter(tuple2 ->
        authorizationHelper.checkModeratorOrCreatorOfRoom(
          tuple2.getT1(),
          tuple2.getT2()
        )
      )
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMap(ignored -> createOrGetByRoomId(roomId))
      .flatMap(this::applyTransientFields)
      .map(setting -> {
        setting.setApiKey(setting.getMaskedApiKey());
        return setting;
      });
  }

  public Mono<GPTRoomSetting> getInternal(UUID roomId) {
    return roomRepository
      .findById(roomId)
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .flatMap(ignored -> createOrGetByRoomId(roomId))
      .flatMap(this::applyTransientFields);
  }

  public Mono<GPTRoomSetting> saveInternal(GPTRoomSetting entity) {
    return repository.save(entity);
  }

  private Mono<GPTRoomSetting> applyTransientFields(GPTRoomSetting setting) {
    return usageTimeRepository
      .findAllBySettingId(setting.getId())
      .collectList()
      .map(usageTimes -> {
        setting.setTrialCode(
          config.getRestrictions().getRoomCode(setting.getRoomId())
        );
        setting.setUsageTimes(usageTimes);
        setting.setGlobalAccumulatedQuota(config.getRestrictions().getGlobalAccumulatedQuota());
        return setting;
      });
  }

  private Mono<GPTRoomSetting> validate(GPTRoomSetting entity) {
    return Mono
      .just(entity)
      .filter(e ->
        e.getMaxDailyRoomCost() == null || e.getMaxDailyRoomCost() >= 0
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Quota may be null, 0 or positive! (maxDailyRoomCost)"
          )
        )
      )
      .filter(e ->
        e.getMaxMonthlyRoomCost() == null || e.getMaxMonthlyRoomCost() >= 0
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Quota may be null, 0 or positive! (maxMonthlyRoomCost)"
          )
        )
      )
      .filter(e ->
        e.getMaxAccumulatedRoomCost() == null ||
        e.getMaxAccumulatedRoomCost() >= 0
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Quota may be null, 0 or positive! (maxAccumulatedRoomCost)"
          )
        )
      )
      .filter(e ->
        e.getMaxDailyParticipantCost() == null ||
        e.getMaxDailyParticipantCost() >= 0
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Quota may be null, 0 or positive! (maxDailyParticipantCost)"
          )
        )
      )
      .filter(e ->
        e.getMaxMonthlyParticipantCost() == null ||
        e.getMaxMonthlyParticipantCost() >= 0
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Quota may be null, 0 or positive! (maxMonthlyParticipantCost)"
          )
        )
      )
      .filter(e ->
        e.getMaxAccumulatedParticipantCost() == null ||
        e.getMaxAccumulatedParticipantCost() >= 0
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Quota may be null, 0 or positive! (maxAccumulatedParticipantCost)"
          )
        )
      )
      .filter(e ->
        e.getMaxDailyModeratorCost() == null ||
        e.getMaxDailyModeratorCost() >= 0
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Quota may be null, 0 or positive! (maxDailyModeratorCost)"
          )
        )
      )
      .filter(e ->
        e.getMaxMonthlyModeratorCost() == null ||
        e.getMaxMonthlyModeratorCost() >= 0
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Quota may be null, 0 or positive! (maxMonthlyModeratorCost)"
          )
        )
      )
      .filter(e ->
        e.getMaxAccumulatedModeratorCost() == null ||
        e.getMaxAccumulatedModeratorCost() >= 0
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Quota may be null, 0 or positive! (maxAccumulatedModeratorCost)"
          )
        )
      );
  }

  private GPTRoomSetting parseChanges(
    GPTRoomSetting entity,
    Map<String, Object> changes,
    boolean isOwner
  ) {
    final int rights = entity.getRightsBitset();
    changes.forEach((key, value) -> {
      switch (key) {
        case "apiKey":
          if (
            !isOwner && !GPTRoomSettingRight.CHANGE_API_KEY.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the apiKey"
            );
          }
          entity.setApiKey((String) value);
          break;
        case "apiOrganization":
          if (
            !isOwner && !GPTRoomSettingRight.CHANGE_API_KEY.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the apiOrganization"
            );
          }
          entity.setApiOrganization((String) value);
          break;
        case "maxDailyRoomCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_ROOM_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxDailyRoomCost"
            );
          }
          entity.setMaxDailyRoomCost((Integer) value);
          break;
        case "maxMonthlyRoomCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_ROOM_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxMonthlyRoomCost"
            );
          }
          entity.setMaxMonthlyRoomCost((Integer) value);
          break;
        case "maxMonthlyFlowingRoomCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_ROOM_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxMonthlyFlowingRoomCost"
            );
          }
          entity.setMaxMonthlyFlowingRoomCost((Integer) value);
          break;
        case "maxAccumulatedRoomCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_ROOM_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxAccumulatedRoomCost"
            );
          }
          entity.setMaxAccumulatedRoomCost(
            value != null ? ((Number) value).longValue() : null
          );
          break;
        case "maxDailyParticipantCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_PARTICIPANT_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxDailyParticipantCost"
            );
          }
          entity.setMaxDailyParticipantCost((Integer) value);
          break;
        case "maxMonthlyParticipantCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_PARTICIPANT_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxMonthlyParticipantCost"
            );
          }
          entity.setMaxMonthlyParticipantCost((Integer) value);
          break;
        case "maxMonthlyFlowingParticipantCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_PARTICIPANT_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxMonthlyFlowingParticipantCost"
            );
          }
          entity.setMaxMonthlyFlowingParticipantCost((Integer) value);
          break;
        case "maxAccumulatedParticipantCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_PARTICIPANT_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxAccumulatedParticipantCost"
            );
          }
          entity.setMaxAccumulatedParticipantCost(
            value != null ? ((Number) value).longValue() : null
          );
          break;
        case "maxDailyModeratorCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_MODERATOR_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxDailyModeratorCost"
            );
          }
          entity.setMaxDailyModeratorCost((Integer) value);
          break;
        case "maxMonthlyModeratorCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_MODERATOR_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxMonthlyModeratorCost"
            );
          }
          entity.setMaxMonthlyModeratorCost((Integer) value);
          break;
        case "maxMonthlyFlowingModeratorCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_MODERATOR_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxMonthlyFlowingModeratorCost"
            );
          }
          entity.setMaxMonthlyFlowingModeratorCost((Integer) value);
          break;
        case "maxAccumulatedModeratorCost":
          if (
            !isOwner &&
            !GPTRoomSettingRight.CHANGE_MODERATOR_QUOTA.isRightSet(rights)
          ) {
            throw new ForbiddenException(
              "You are not allowed to change the maxAccumulatedModeratorCost"
            );
          }
          entity.setMaxAccumulatedModeratorCost(
            value != null ? ((Number) value).longValue() : null
          );
          break;
        case "rightsBitset":
          if (!isOwner) {
            throw new ForbiddenException(
              "You are not allowed to change the rightsBitset"
            );
          }
          entity.setRightsBitset((int) value);
          break;
        case "id":
        case "roomId":
        case "dailyQuotaCounter":
        case "monthlyQuotaCounter":
        case "accumulatedQuotaCounter":
        case "dailyCostCounter":
        case "monthlyCostCounter":
        case "accumulatedCostCounter":
        case "lastQuotaUsage":
        case "trialCostCounter":
        case "paymentCounter":
        case "createdAt":
        case "updatedAt":
        case "trialCode":
          throw new ForbiddenException(
            "You are not allowed to change the " + key
          );
        case "trialEnabled":
        case "usageTimes":
        case "roleInstruction":
          throw new BadRequestException("Wrong endpoint!");
        default:
          throw new BadRequestException(
            "Invalid ChangeAttribute provided (" + key + ")"
          );
      }
    });
    return entity;
  }
}
