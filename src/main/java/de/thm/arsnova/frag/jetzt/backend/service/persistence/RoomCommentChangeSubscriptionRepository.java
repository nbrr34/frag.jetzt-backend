package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.RoomCommentChangeSubscription;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface RoomCommentChangeSubscriptionRepository extends ReactiveCrudRepository<RoomCommentChangeSubscription, UUID> {

    Flux<RoomCommentChangeSubscription> findByAccountId(UUID accountId);

    Mono<Void> deleteByAccountIdAndRoomId(UUID accountId, UUID roomId);

}
