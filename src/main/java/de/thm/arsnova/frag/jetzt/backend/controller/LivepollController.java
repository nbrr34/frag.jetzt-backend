package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.handler.LivepollCommandHandler;
import de.thm.arsnova.frag.jetzt.backend.model.command.CreateLivepollSession;
import de.thm.arsnova.frag.jetzt.backend.model.command.CreateLivepollSessionPayload;
import de.thm.arsnova.frag.jetzt.backend.model.command.DeleteLivepollSession;
import de.thm.arsnova.frag.jetzt.backend.model.command.DeleteLivepollSessionPayload;
import de.thm.arsnova.frag.jetzt.backend.model.command.DeleteLivepollVotes;
import de.thm.arsnova.frag.jetzt.backend.model.command.DeleteLivepollVotesPayload;
import de.thm.arsnova.frag.jetzt.backend.model.command.PatchLivepollSession;
import de.thm.arsnova.frag.jetzt.backend.model.command.PatchLivepollSessionPayload;
import de.thm.arsnova.frag.jetzt.backend.model.command.VoteLivepoll;
import de.thm.arsnova.frag.jetzt.backend.model.command.VoteLivepollPayload;
import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollSession;
import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollVote;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.service.LivepollSessionService;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("LivepollController")
@RequestMapping(LivepollController.REQUEST_MAPPING)
public class LivepollController extends AbstractEntityController {

  private static final Logger logger = LoggerFactory.getLogger(
    LivepollController.class
  );

  private static class Vote {

    private int voteIndex;

    public int getVoteIndex() {
      return voteIndex;
    }

    public void setVoteIndex(int voteIndex) {
      this.voteIndex = voteIndex;
    }
  }

  protected static final String REQUEST_MAPPING = "/livepoll";
  private static final String SESSION_MAPPING = "/session";
  private static final String VOTE_MAPPING = "/vote";
  private static final String VOTES_MAPPING = "/votes";
  private static final String RESET_MAPPING = "/reset";

  private final LivepollCommandHandler commandHandler;
  private final LivepollSessionService sessionService;

  public LivepollController(
    final LivepollCommandHandler commandHandler,
    final LivepollSessionService sessionService
  ) {
    this.commandHandler = commandHandler;
    this.sessionService = sessionService;
  }

  @PostMapping(SESSION_MAPPING)
  public Mono<LivepollSession> create(
    @RequestBody final LivepollSession session
  ) {
    CreateLivepollSessionPayload p = new CreateLivepollSessionPayload(session);
    CreateLivepollSession command = new CreateLivepollSession(p);

    return commandHandler.handle(command);
  }

  @PostMapping(DELETE_MAPPING)
  public Mono<Void> deleteSession(@PathVariable final UUID id) {
    DeleteLivepollSessionPayload p = new DeleteLivepollSessionPayload(id);
    DeleteLivepollSession command = new DeleteLivepollSession(p);

    return commandHandler.handle(command);
  }

  @PatchMapping(SESSION_MAPPING + PATCH_MAPPING)
  public Mono<LivepollSession> patch(
    @PathVariable final UUID id,
    @RequestBody final HashMap<String, Object> changes
  ) {
    PatchLivepollSessionPayload p = new PatchLivepollSessionPayload(
      id,
      changes
    );
    PatchLivepollSession command = new PatchLivepollSession(p);
    return commandHandler.handle(command);
  }

  @DeleteMapping(RESET_MAPPING + DELETE_MAPPING)
  public Mono<Void> deleteVotes(@PathVariable final UUID id) {
    DeleteLivepollVotesPayload p = new DeleteLivepollVotesPayload(id);
    DeleteLivepollVotes command = new DeleteLivepollVotes(p);
    return commandHandler.handle(command);
  }

  @PostMapping(FIND_MAPPING)
  public Flux<LivepollSession> find(
    @RequestBody final FindQuery<LivepollSession> findQuery
  ) {
    logger.debug("Resolving find query: {}", findQuery);

    return this.sessionService.getAllByRoomId(
        findQuery.getProperties().getRoomId()
      );
  }

  @GetMapping(VOTE_MAPPING + GET_MAPPING)
  public Mono<LivepollVote> get(@PathVariable final UUID id) {
    return sessionService.getVoteBySessionId(id);
  }

  @PostMapping(VOTE_MAPPING + PATCH_MAPPING)
  public Mono<Void> post(@PathVariable final UUID id, @RequestBody Vote v) {
    VoteLivepollPayload payload = new VoteLivepollPayload(id, v.getVoteIndex());
    return commandHandler.handle(new VoteLivepoll(payload));
  }

  @DeleteMapping(VOTE_MAPPING + DELETE_MAPPING)
  public Mono<Void> delete(@PathVariable final UUID id) {
    VoteLivepollPayload payload = new VoteLivepollPayload(id, null);
    return commandHandler.handle(new VoteLivepoll(payload));
  }

  @GetMapping(VOTES_MAPPING + GET_MAPPING)
  public Mono<List<Integer>> getResults(@PathVariable final UUID id) {
    return sessionService.getResults(id);
  }
}
