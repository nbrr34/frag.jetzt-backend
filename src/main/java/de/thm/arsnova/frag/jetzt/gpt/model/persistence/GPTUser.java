package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.Nullable;

@Table
public class GPTUser implements Persistable<UUID> {

  @Id
  private UUID id;

  private UUID accountId;
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;
  private boolean blocked;
  private Timestamp blockedEndTimestamp = null;
  private Boolean consented;

  public GPTUser() {}

  public GPTUser(UUID accountId) {
    this.accountId = accountId;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  @Nullable
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public Boolean getConsented() {
    return consented;
  }

  public void setConsented(Boolean consented) {
    this.consented = consented;
  }

  public boolean isBlocked() {
    return blocked;
  }

  public void setBlocked(boolean blocked) {
    this.blocked = blocked;
  }

  public Timestamp getBlockedEndTimestamp() {
    return blockedEndTimestamp;
  }

  public void setBlockedEndTimestamp(Timestamp blockedEndTimestamp) {
    this.blockedEndTimestamp = blockedEndTimestamp;
  }

  @JsonIgnore
  public boolean isRequestForbidden() {
    return (
      blocked ||
      (
        blockedEndTimestamp != null &&
        blockedEndTimestamp.compareTo(Timestamp.from(Instant.now())) <= 0
      )
    );
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + (blocked ? 1231 : 1237);
    result =
      prime *
      result +
      ((blockedEndTimestamp == null) ? 0 : blockedEndTimestamp.hashCode());
    result = prime * result + ((consented == null) ? 0 : consented.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTUser other = (GPTUser) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (blocked != other.blocked) return false;
    if (blockedEndTimestamp == null) {
      if (other.blockedEndTimestamp != null) return false;
    } else if (
      !blockedEndTimestamp.equals(other.blockedEndTimestamp)
    ) return false;
    if (consented == null) {
      if (other.consented != null) return false;
    } else if (!consented.equals(other.consented)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTUser [id=" +
      id +
      ", accountId=" +
      accountId +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", blocked=" +
      blocked +
      ", blockedEndTimestamp=" +
      blockedEndTimestamp +
      ", consented=" +
      consented +
      "]"
    );
  }
}
