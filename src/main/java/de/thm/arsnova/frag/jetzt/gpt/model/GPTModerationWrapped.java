package de.thm.arsnova.frag.jetzt.gpt.model;

import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModerationResponse;

public class GPTModerationWrapped<T> {

  private T completion;
  private GPTModerationResponse response;

  public GPTModerationWrapped() {}

  public GPTModerationWrapped(T completion, GPTModerationResponse response) {
    this.completion = completion;
    this.response = response;
  }

  public T getCompletion() {
    return completion;
  }

  public void setCompletion(T completion) {
    this.completion = completion;
  }

  public GPTModerationResponse getResponse() {
    return response;
  }

  public void setResponse(GPTModerationResponse response) {
    this.response = response;
  }
}
