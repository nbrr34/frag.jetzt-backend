package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import java.util.UUID;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTUser;
import reactor.core.publisher.Mono;

@Repository
public interface GPTUserRepository extends ReactiveCrudRepository<GPTUser, UUID> {

    Mono<GPTUser> findByAccountId(UUID accountId);
    
}
