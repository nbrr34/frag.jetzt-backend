package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Service
public class RoomFindQueryService {
    private final RoomService roomService;
    private final RoomAccessService roomAccessService;

    @Autowired
    public RoomFindQueryService(final RoomService roomService, final RoomAccessService roomAccessService) {
        this.roomService = roomService;
        this.roomAccessService = roomAccessService;
    }

    public Flux<Room> resolveQuery(final FindQuery<Room> findQuery) {
        if (findQuery.getExternalFilters().get("inHistoryOfUserId") instanceof String) {
            return roomAccessService.getByAccountId(UUID.fromString((String) findQuery.getExternalFilters().get("inHistoryOfUserId")))
                    .flatMap(roomHistory -> roomService.get(roomHistory.getRoomId()));
        }
        if (findQuery.getExternalFilters().get("moderatedByUserId") instanceof String) {
            return roomAccessService.getByAccountId(UUID.fromString((String) findQuery.getExternalFilters().get("moderatedByUserId")))
                    .flatMap(roomAccess -> roomService.get(roomAccess.getRoomId()));
        }
        if (findQuery.getProperties().getOwnerId() != null) {
            return roomService.getByOwnerId(findQuery.getProperties().getOwnerId())
                    .filter(room -> room.getModeratorRoomReference() == null);
        }
        return Flux.empty();
    }
}
