package de.thm.arsnova.frag.jetzt.backend.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * InternalServerErrorException means status code 500.
 */
public class InternalServerErrorException extends ResponseStatusException {

    public InternalServerErrorException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public InternalServerErrorException(final String message) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, message);
    }

    public InternalServerErrorException(final String message, final Throwable e) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, message, e);
    }
}