package de.thm.arsnova.frag.jetzt.gpt.util;

import java.util.UUID;

public class EndpointIdentifier {

  private final UUID accountId;
  private final String endpoint;

  public EndpointIdentifier(UUID accountId, String endpoint) {
    this.accountId = accountId;
    this.endpoint = endpoint;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public String getEndpoint() {
    return endpoint;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + ((endpoint == null) ? 0 : endpoint.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    EndpointIdentifier other = (EndpointIdentifier) obj;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (endpoint == null) {
      if (other.endpoint != null) return false;
    } else if (!endpoint.equals(other.endpoint)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "EndpointIdentifier [accountId=" +
      accountId +
      ", endpoint=" +
      endpoint +
      "]"
    );
  }
}
