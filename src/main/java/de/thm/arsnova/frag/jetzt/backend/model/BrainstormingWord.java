package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Table
public class BrainstormingWord implements Persistable<UUID> {

    @Id
    private UUID id;
    private UUID sessionId;
    private String word;
    private String correctedWord;
    private int upvotes;
    private int downvotes;
    private boolean banned;
    private UUID categoryId;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    public BrainstormingWord() {

    }

    public BrainstormingWord(UUID sessionId, String word, int upvotes, int downvotes) {
        this.sessionId = sessionId;
        this.word = word;
        this.upvotes = upvotes;
        this.downvotes = downvotes;
        this.banned = false;
        this.categoryId = null;
        this.correctedWord = null;
    }

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getSessionId() {
        return sessionId;
    }

    public void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(int upvotes) {
        this.upvotes = upvotes;
    }

    public int getDownvotes() {
        return downvotes;
    }

    public void setDownvotes(int downvotes) {
        this.downvotes = downvotes;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public String getCorrectedWord() {
        return correctedWord;
    }

    public void setCorrectedWord(String correctedWord) {
        this.correctedWord = correctedWord;
    }

    @Override
    public String toString() {
        return "BrainstormingWord{" +
                "id=" + id +
                ", banned=" + banned +
                ", categoryId=" + categoryId +
                ", sessionId=" + sessionId +
                ", word='" + word + '\'' +
                ", correctedWord='" + correctedWord + '\'' +
                ", upvotes=" + upvotes +
                ", downvotes=" + downvotes +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingWord that = (BrainstormingWord) o;
        return upvotes == that.upvotes &&
                downvotes == that.downvotes &&
                banned == that.banned &&
                Objects.equals(categoryId, that.categoryId) &&
                Objects.equals(id, that.id) &&
                Objects.equals(sessionId, that.sessionId) &&
                Objects.equals(word, that.word) &&
                Objects.equals(correctedWord, that.correctedWord) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(updatedAt, that.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sessionId, word, correctedWord, upvotes, downvotes, createdAt, updatedAt, banned, categoryId);
    }
}
