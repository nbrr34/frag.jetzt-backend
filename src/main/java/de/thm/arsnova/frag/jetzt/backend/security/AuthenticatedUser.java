package de.thm.arsnova.frag.jetzt.backend.security;

import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class AuthenticatedUser implements Authentication {

    private final String accountId, token, type, email;
    private final List<RoomAccess> roomAccesses;
    private final Collection<? extends GrantedAuthority> roles;
    private boolean authenticated = true;

    public AuthenticatedUser(String token, String accountId, String type, String email, List<RoomAccess> roomAccesses, Collection<? extends GrantedAuthority> roles) {
        this.token = token;
        this.accountId = accountId;
        this.type = type;
        this.email = email;
        this.roomAccesses = roomAccesses;
        this.roles = roles;
    }

    public AuthenticatedUser(AuthenticatedUser toCopy, String newToken) {
        token = newToken;
        accountId = toCopy.accountId;
        type = toCopy.type;
        email = toCopy.email;
        roomAccesses = toCopy.roomAccesses;
        roles = toCopy.roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public Object getCredentials() {
        return accountId;
    }

    @Override
    public Object getDetails() {
        return token;
    }

    @Override
    public Object getPrincipal() {
        return accountId;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        authenticated = b;
    }

    @Override
    public String getName() {
        return email;
    }

    public String getType() {
        return type;
    }

    public UUID getAccountId() {
        return UUID.fromString(accountId);
    }

    public List<RoomAccess> getRoomAccesses() {
        return roomAccesses;
    }

    @Override
    public String toString() {
        return "AuthenticatedUser{" +
                "userId='" + accountId + '\'' +
                ", token='" + token + '\'' +
                ", type='" + type + '\'' +
                ", roles=" + roles +
                ", authenticated=" + authenticated +
                '}';
    }
}
