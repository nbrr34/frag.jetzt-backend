package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import de.thm.arsnova.frag.jetzt.backend.model.CommentNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class CommentNotificationFindQueryService {

    private static final Logger logger = LoggerFactory.getLogger(CommentNotificationFindQueryService.class);

    private final CommentNotificationService service;

    @Autowired
    public CommentNotificationFindQueryService(
            final CommentNotificationService service
    ) {
        this.service = service;
    }

    public Flux<CommentNotification> resolveQuery(final FindQuery<CommentNotification> findQuery) {
        if (findQuery.getProperties().getRoomId() != null) {
            return service.getByRoomId(findQuery.getProperties().getRoomId());
        }
        return Flux.empty();
    }
}
