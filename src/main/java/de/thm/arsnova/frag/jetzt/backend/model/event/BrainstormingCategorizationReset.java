package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class BrainstormingCategorizationReset extends WebSocketEvent<BrainstormingCategorizationResetPayload> {

    public BrainstormingCategorizationReset() {
        super(BrainstormingCategorizationReset.class.getSimpleName());
    }

    public BrainstormingCategorizationReset(BrainstormingCategorizationResetPayload payload, UUID roomId) {
        super(BrainstormingCategorizationReset.class.getSimpleName());
        this.payload = payload;
        this.roomId = roomId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingCategorizationReset that = (BrainstormingCategorizationReset) o;
        return this.getPayload().equals(that.getPayload());
    }
}
