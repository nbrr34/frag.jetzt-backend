package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.CommentChange;
import de.thm.arsnova.frag.jetzt.backend.model.CommentChangeSubscription;
import de.thm.arsnova.frag.jetzt.backend.model.RoomCommentChangeSubscription;
import de.thm.arsnova.frag.jetzt.backend.service.*;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController("CommentChangeController")
@RequestMapping(CommentChangeController.REQUEST_MAPPING)
public class CommentChangeController extends AbstractEntityController {

    private static final Logger logger = LoggerFactory.getLogger(CommentChangeController.class);

    protected static final String REQUEST_MAPPING = "/comment-change";
    private static final String COMMENT_SUBSCRIPTION_MAPPING = "/comment-subscribe";
    private static final String COMMENT_SUBSCRIBE_MAPPING = COMMENT_SUBSCRIPTION_MAPPING + DEFAULT_ID_MAPPING;
    private static final String ROOM_SUBSCRIPTION_MAPPING = "/room-subscribe";
    private static final String ROOM_SUBSCRIBE_MAPPING = "/room-subscribe" + DEFAULT_ID_MAPPING;

    private final CommentChangeService service;
    private final CommentChangeFindQueryService findQueryService;
    private final CommentChangeSubscriptionService subscriptionService;
    private final RoomCommentChangeSubscriptionService roomSubscriptionService;

    @Autowired
    public CommentChangeController(
            CommentChangeService service,
            CommentChangeFindQueryService findQueryService,
            CommentChangeSubscriptionService subscriptionService,
            RoomCommentChangeSubscriptionService roomSubscriptionService
    ) {
        this.service = service;
        this.findQueryService = findQueryService;
        this.subscriptionService = subscriptionService;
        this.roomSubscriptionService = roomSubscriptionService;
    }

    @GetMapping(GET_MAPPING)
    public Mono<CommentChange> get(@PathVariable UUID id) {
        return service.get(id).switchIfEmpty(Mono.error(NotFoundException::new));
    }

    @PostMapping(COMMENT_SUBSCRIBE_MAPPING)
    public Mono<CommentChangeSubscription> postSubscribeComment(@PathVariable UUID id) {
        return subscriptionService.create(id);
    }

    @GetMapping(COMMENT_SUBSCRIPTION_MAPPING)
    public Flux<CommentChangeSubscription> getCommentSubscriptions() {
        return subscriptionService.get();
    }

    @DeleteMapping(COMMENT_SUBSCRIBE_MAPPING)
    public Mono<Void> deleteCommentSubscription(@PathVariable UUID id) {
        return subscriptionService.deleteByCommentId(id);
    }

    @PostMapping(ROOM_SUBSCRIBE_MAPPING)
    public Mono<RoomCommentChangeSubscription> postSubscribeRoom(@PathVariable UUID id) {
        return roomSubscriptionService.create(id);
    }

    @GetMapping(ROOM_SUBSCRIPTION_MAPPING)
    public Flux<RoomCommentChangeSubscription> getRoomSubscriptions() {
        return roomSubscriptionService.get();
    }

    @DeleteMapping(ROOM_SUBSCRIBE_MAPPING)
    public Mono<Void> deleteRoomSubscription(@PathVariable UUID id) {
        return roomSubscriptionService.deleteByRoomId(id);
    }

    @PostMapping(FIND_MAPPING)
    public Flux<CommentChange> find(@RequestBody final FindQuery<CommentChange> findQuery) {
        logger.debug("Resolving find query: {}", findQuery);
        return findQueryService.resolveQuery(findQuery);
    }
}
