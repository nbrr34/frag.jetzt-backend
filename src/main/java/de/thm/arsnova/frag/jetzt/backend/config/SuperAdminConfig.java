package de.thm.arsnova.frag.jetzt.backend.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
public class SuperAdminConfig {

    @Bean
    @ConfigurationProperties(prefix = "app.super-admin-emails")
    protected List<String> superAdminEmails() {
        return new ArrayList<>();
    }

    @Bean
    public List<String> getSuperAdminList() {
        ArrayList<String> superAdminEmails = new ArrayList<>(superAdminEmails());
        superAdminEmails.replaceAll(String::toLowerCase);
        return Collections.unmodifiableList(superAdminEmails);
    }

}
