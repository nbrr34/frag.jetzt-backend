package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@JsonInclude(Include.NON_NULL)
public class GPTChatCompletionRequest implements IGPTRequest {

  private UUID roomId;
  private String apiKey;
  private String apiOrganization;
  private String model = "gpt-3.5-turbo";
  private List<GPTChatCompletionMessage> messages;
  /** Defaults to 1 */
  private Double temperature = null;
  /** Defaults to 1 */
  private Double topP = null;
  /** Defaults to 1 */
  private Integer n = null;
  /** Defaults to false */
  private Boolean stream = null;
  /** Defaults to null */
  private List<String> stop = null;
  /** Defaults to infinity (Integer.MAX_VALUE) */
  private Integer maxTokens = null;
  /** Defaults to 0 */
  private Double presencePenalty = null;
  /** Defaults to 0 */
  private Double frequencyPenalty = null;
  /** Defaults to null */
  private Map<String, Integer> logitBias = null;
  /** Defaults to null */
  private String user = null;

  public static String combineMessages(
    List<GPTChatCompletionMessage> messages
  ) {
    StringBuilder b = new StringBuilder();
    for (GPTChatCompletionMessage m : messages) {
      b.append(m.wrap());
    }
    return b.toString();
  }

  public GPTChatCompletionRequest() {
    ArrayList<GPTChatCompletionMessage> list = new ArrayList<>();
    list.add(new GPTChatCompletionMessage());
    messages = Collections.unmodifiableList(list);
  }

  @JsonIgnore
  @Override
  public UUID getRoomId() {
    return roomId;
  }

  @JsonProperty("roomId")
  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  @JsonIgnore
  @Override
  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  @JsonIgnore
  @Override
  public String getApiOrganization() {
    return apiOrganization;
  }

  public void setApiOrganization(String apiOrganization) {
    this.apiOrganization = apiOrganization;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    if (model == null) {
      throw new IllegalArgumentException("model can not be null!");
    }
    this.model = model;
  }

  public List<GPTChatCompletionMessage> getMessages() {
    return messages;
  }

  public void setMessages(List<GPTChatCompletionMessage> messages) {
    if (messages == null || messages.size() < 1) {
      throw new IllegalArgumentException("messages can not be empty!");
    }
    this.messages = Collections.unmodifiableList(messages);
  }

  public Double getTemperature() {
    return temperature;
  }

  public void setTemperature(Double temperature) {
    if (temperature == null) {
      this.temperature = null;
      return;
    }
    if (temperature < 0 || temperature > 2) {
      throw new IllegalArgumentException("temperature must be within [0, 2]");
    }
    this.topP = null;
    this.temperature = temperature;
  }

  @JsonProperty("top_p")
  public Double getTopP() {
    return topP;
  }

  public void setTopP(Double topP) {
    if (topP == null) {
      this.topP = null;
      return;
    }
    if (topP < 0 || topP > 1) {
      throw new IllegalArgumentException(
        "topP must be within [0, 1] (probability range)"
      );
    }
    this.temperature = null;
    this.topP = topP;
  }

  public Integer getN() {
    return n;
  }

  public void setN(Integer n) {
    if (n == null) {
      this.n = null;
      return;
    }
    if (n < 1) {
      throw new IllegalArgumentException("n needs to be at least 1!");
    }
    this.n = n;
  }

  public Boolean getStream() {
    return stream;
  }

  public void setStream(Boolean stream) {
    this.stream = stream;
  }

  public List<String> getStop() {
    return stop;
  }

  public void setStop(List<String> stop) {
    if (stop == null) {
      this.stop = null;
      return;
    }
    this.stop = Collections.unmodifiableList(stop);
  }

  @JsonProperty("max_tokens")
  public Integer getMaxTokens() {
    return maxTokens;
  }

  public void setMaxTokens(Integer maxTokens) {
    if (maxTokens == null) {
      this.maxTokens = null;
      return;
    }
    if (maxTokens < 1) {
      throw new IllegalArgumentException("maxTokens can not be lower than 1!");
    }
    this.maxTokens = maxTokens;
  }

  @JsonProperty("presence_penalty")
  public Double getPresencePenalty() {
    return presencePenalty;
  }

  public void setPresencePenalty(Double presencePenalty) {
    if (presencePenalty == null) {
      this.presencePenalty = null;
      return;
    }
    if (presencePenalty < -2 || presencePenalty > 2) {
      throw new IllegalArgumentException(
        "presencePenalty must be within [-2, 2]"
      );
    }
    this.presencePenalty = presencePenalty;
  }

  @JsonProperty("frequency_penalty")
  public Double getFrequencyPenalty() {
    return frequencyPenalty;
  }

  public void setFrequencyPenalty(Double frequencyPenalty) {
    if (frequencyPenalty == null) {
      this.frequencyPenalty = null;
      return;
    }
    if (frequencyPenalty < -2 || frequencyPenalty > 2) {
      throw new IllegalArgumentException(
        "frequencyPenalty must be within [-2, 2]"
      );
    }
    this.frequencyPenalty = frequencyPenalty;
  }

  @JsonProperty("logit_bias")
  public Map<String, Integer> getLogitBias() {
    return logitBias;
  }

  public void setLogitBias(Map<String, Integer> logitBias) {
    if (logitBias == null) {
      this.logitBias = null;
      return;
    }
    logitBias.forEach((key, value) -> {
      if (value == null || value < -100 || value > 100) {
        throw new IllegalArgumentException(
          "A logitBias value is not within [-100, 100]!"
        );
      }
    });
    this.logitBias = Collections.unmodifiableMap(logitBias);
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }
}
