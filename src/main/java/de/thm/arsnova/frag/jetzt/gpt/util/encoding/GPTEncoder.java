package de.thm.arsnova.frag.jetzt.gpt.util.encoding;

import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This class represents a part of the OpenAI Tokenizer (tiktoken) Project. See
 * https://github.com/openai/tiktoken.
 */
public class GPTEncoder {

  private static class PositionRank {

    int start;
    int rank;

    PositionRank(int start, int rank) {
      this.start = start;
      this.rank = rank;
    }
  }

  /**
   * Name of the encoding
   */
  private String name;
  /**
   * Pattern is used to split the encoding input
   */
  private Pattern stringSplitter;
  /**
   * Maps token bytes to their rank. Ranks must correspond to merge priority
   * <br/><br/>
   * Also known as encoder
   */
  private HashMap<GPTTokenByte, Integer> mergeRank = new HashMap<>();
  /**
   * Maps special token strings to their token values
   */
  private HashMap<String, Integer> specialTokenMapping = new HashMap<>();
  /**
   * Number of tokens in the vocab. Asserts size if given.
   */
  private Integer explicitVocabSize;
  /**
   * The highest token rank in the vocabulary
   */
  private final int maxTokenValue;
  /**
   * Helps for finding special characters
   */
  private final Pattern specialCharacterPattern;

  public GPTEncoder(
    String name,
    Pattern stringSplitter,
    HashMap<GPTTokenByte, Integer> mergeRank,
    HashMap<String, Integer> specialTokenMapping,
    Integer explicitVocabSize
  ) {
    this.name = name;
    this.stringSplitter = stringSplitter;
    this.mergeRank = mergeRank;
    this.specialTokenMapping = specialTokenMapping;
    this.explicitVocabSize = explicitVocabSize;
    int maxTokenValue = 0;
    for (Integer i : mergeRank.values()) {
      if (i > maxTokenValue) {
        maxTokenValue = i;
      }
    }
    for (Integer i : specialTokenMapping.values()) {
      if (i > maxTokenValue) {
        maxTokenValue = i;
      }
    }
    this.maxTokenValue = maxTokenValue;
    if (explicitVocabSize != null) {
      if (mergeRank.size() + specialTokenMapping.size() != explicitVocabSize) {
        throw new IllegalStateException(
          "mergeRank size and specialTokenMapping size must equal explicitVocabSize"
        );
      }
      if (this.maxTokenValue != explicitVocabSize - 1) {
        throw new IllegalStateException(
          "Some indexes/token ranks are messed up"
        );
      }
    }
    specialCharacterPattern =
      Pattern.compile(
        specialTokenMapping
          .keySet()
          .stream()
          .map(s -> Pattern.quote(s))
          .collect(Collectors.joining("|")),
        Pattern.UNICODE_CASE
      );
  }

  public String getName() {
      return name;
  }

  public Integer getExplicitVocabSize() {
      return explicitVocabSize;
  }

  public int getEncodeCount(String text) {
    char[] chars = text.toCharArray();
    int tokens = 0;
    final Matcher matcher = specialCharacterPattern.matcher(text);

    int endLast = 0;
    while (matcher.find()) {
      int end = matcher.start();

      final Matcher subMatcher =
        this.stringSplitter.matcher(
            CharBuffer.wrap(chars, endLast, end - endLast)
          );
      while (subMatcher.find()) {
        byte[] data = matcher.group(0).getBytes(StandardCharsets.UTF_8);
        GPTTokenByte tb = new GPTTokenByte(data);
        Integer token = mergeRank.get(tb);
        if (token != null) {
            tokens += 1;
          continue;
        }
        tokens += bytePairEncodeCount(tb, data);
      }

      endLast = matcher.end();
      specialTokenMapping.get(matcher.group(0)).intValue();
      tokens += 1;
    }
    return tokens;
  }

  private int bytePairEncodeCount(
    GPTTokenByte tb,
    byte[] data
  ) {
    if (data.length == 1) {
      Integer i = mergeRank.get(tb);
      if (i == null) {
        throw new IllegalArgumentException("Value not in vocabulary");
      }
      return 1;
    }
    return bytePairMergeCount(data);
  }

  private int bytePairMergeCount(byte[] data) {
    ArrayList<PositionRank> parts = new ArrayList<>(data.length + 1);
    for (int i = 0; i <= data.length; i++) {
      parts.add(new PositionRank(i, Integer.MAX_VALUE));
    }

    for (int i = 0; i < parts.size() - 2; i++) {
      Integer rank = bytePairMergeGetRank(parts, data, i, 0);
      if (rank == null) {
        continue;
      }
      if (rank == Integer.MAX_VALUE) {
        throw new IllegalStateException(
          "Should not get max int, since it is default property"
        );
      }
      parts.get(i).rank = rank;
    }

    while (true) {
      if (parts.size() == 1) {
        break;
      }

      PositionRank minRank = new PositionRank(0, Integer.MAX_VALUE);
      for (int i = 0; i < parts.size() - 1; i++) {
        PositionRank current = parts.get(i);
        if (current.rank < minRank.rank) {
          minRank.rank = current.rank;
          minRank.start = current.start;
        }
      }

      if (minRank.rank == Integer.MAX_VALUE) {
        break;
      }

      final int i = minRank.start;
      Integer temp = bytePairMergeGetRank(parts, data, i, 1);
      parts.get(i).rank = temp != null ? temp : Integer.MAX_VALUE;
      if (i > 0) {
        temp = bytePairMergeGetRank(parts, data, i - 1, 1);
        parts.get(i - 1).rank = temp != null ? temp : Integer.MAX_VALUE;
      }
      parts.remove(i + 1);
    }

    int count = 0;
    for (int i = 0; i < parts.size() - 1; i++) {
      int arrStart = parts.get(i).start;
      int arrEnd = parts.get(i + 1).start;
      byte[] newData = new byte[arrEnd - arrStart];
      System.arraycopy(data, arrStart, newData, 0, newData.length);
      Integer tokenValue = mergeRank.get(new GPTTokenByte(newData));
      if (tokenValue == null) {
        throw new IllegalArgumentException("tokenValue should not be null");
      }
      count++;
    }
    return count;
  }

  private Integer bytePairMergeGetRank(
    ArrayList<PositionRank> parts,
    byte[] data,
    int start,
    int skip
  ) {
    final int end = start + skip + 2;
    if (end < parts.size()) {
      int arrStart = parts.get(start).start;
      int arrEnd = parts.get(end).start;
      byte[] newData = new byte[arrEnd - arrStart];
      System.arraycopy(data, arrStart, newData, 0, newData.length);
      return mergeRank.get(new GPTTokenByte(newData)).intValue();
    }
    return null;
  }
}
