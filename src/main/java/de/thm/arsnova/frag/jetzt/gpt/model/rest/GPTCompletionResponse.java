package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.Timestamp;
import java.util.List;

public class GPTCompletionResponse<T extends IGPTCompletionChoice> implements IGPTStreamObject {

  public static class GPTCompletionResponseUsage {

    private int promptTokens;
    private int completionTokens;
    private int totalTokens;

    @JsonProperty("prompt_tokens")
    public int getPromptTokens() {
      return promptTokens;
    }

    public void setPromptTokens(int promptTokens) {
      if (promptTokens < 0) {
        throw new IllegalArgumentException(
          "promptTokens can only be positive!"
        );
      }
      this.promptTokens = promptTokens;
    }

    @JsonProperty("completion_tokens")
    public int getCompletionTokens() {
      return completionTokens;
    }

    public void setCompletionTokens(int completionTokens) {
      if (completionTokens < 0) {
        throw new IllegalArgumentException(
          "completionTokens can only be positive!"
        );
      }
      this.completionTokens = completionTokens;
    }

    @JsonProperty("total_tokens")
    public int getTotalTokens() {
      return totalTokens;
    }

    public void setTotalTokens(int totalTokens) {
      if (totalTokens < 0) {
        throw new IllegalArgumentException("totalTokens can only be positive!");
      }
      this.totalTokens = totalTokens;
    }
  }

  /**
   * Should be present in sync completions and streams
   */
  private String id;
  /**
   * Should be present in sync completions and streams<br>
   * One of:
   * <ul>
   *   <li>text_completion</li>
   *   <li>chat.completion</li>
   * </ul>
   */
  private String object;
  /**
   * Should be present in sync completions and streams
   */
  private Timestamp created;
  /**
   * Should be present in sync completions and streams
   */
  private String model;
  /**
   * Only present in sync completions<br>
   * In streaming mode calculate tokens yourself
   */
  private GPTCompletionResponseUsage usage;
  /**
   * Always present in sync completions and streams
   */
  private List<T> choices;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getObject() {
    return object;
  }

  public void setObject(String object) {
    this.object = object;
  }

  public Timestamp getCreated() {
    return created;
  }

  public void setCreated(Timestamp created) {
    this.created = created;
  }

  public String getModel() {
      return model;
  }

  public void setModel(String model) {
      this.model = model;
  }

  public GPTCompletionResponseUsage getUsage() {
      return usage;
  }

  public void setUsage(GPTCompletionResponseUsage usage) {
      this.usage = usage;
  }

  public List<T> getChoices() {
      return choices;
  }

  public void setChoices(List<T> choices) {
      this.choices = choices;
  }
}
