package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Table
public class BrainstormingCategory implements Persistable<UUID> {

    @Id
    private UUID id;
    private UUID roomId;
    private String name;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "BrainstormingCategory{" +
                "id=" + id +
                ", roomId=" + roomId +
                ", name='" + name + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingCategory category = (BrainstormingCategory) o;
        return Objects.equals(id, category.id) &&
                Objects.equals(roomId, category.roomId) &&
                Objects.equals(name, category.name) &&
                Objects.equals(createdAt, category.createdAt) &&
                Objects.equals(updatedAt, category.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomId, name, createdAt, updatedAt);
    }

}