package de.thm.arsnova.frag.jetzt.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;

public class FragJetztCredentials implements Persistable<UUID> {

  @Id
  private UUID id;

  private String email;
  private String password;
  private String activationKey;
  private Timestamp activationKeyTime;
  private String passwordResetKey;
  private Timestamp passwordResetTime;
  private Timestamp passwordExpirationDate;
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  @Override
  public boolean isNew() {
    return id == null;
  }

  public FragJetztCredentials(
    String email,
    String password,
    String activationKey,
    Timestamp activationKeyTime,
    String passwordResetKey,
    Timestamp passwordResetTime
  ) {
    this.email = email;
    this.password = password;
    this.activationKey = activationKey;
    this.activationKeyTime = activationKeyTime;
    this.passwordResetKey = passwordResetKey;
    this.passwordResetTime = passwordResetTime;
    this.resetPasswordExpiry();
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getActivationKey() {
    return activationKey;
  }

  public void setActivationKey(String activationKey) {
    this.activationKey = activationKey;
  }

  public Timestamp getActivationKeyTime() {
    return activationKeyTime;
  }

  public void setActivationKeyTime(Timestamp activationKeyTime) {
    this.activationKeyTime = activationKeyTime;
  }

  public String getPasswordResetKey() {
    return passwordResetKey;
  }

  public void setPasswordResetKey(String passwordResetKey) {
    this.passwordResetKey = passwordResetKey;
  }

  public Timestamp getPasswordResetTime() {
    return passwordResetTime;
  }

  public void setPasswordResetTime(Timestamp passwordResetTime) {
    this.passwordResetTime = passwordResetTime;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public Timestamp getPasswordExpirationDate() {
    return passwordExpirationDate;
  }

  public void setPasswordExpirationDate(Timestamp passwordExpirationDate) {
    this.passwordExpirationDate = passwordExpirationDate;
  }

  @JsonIgnore
  public void resetPasswordExpiry() {
    this.passwordExpirationDate =
      Timestamp.from(Instant.now().plus(180, ChronoUnit.DAYS));
  }

  @JsonIgnore
  public boolean isPasswordExpired() {
    return this.passwordExpirationDate.toInstant().isBefore(Instant.now());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((email == null) ? 0 : email.hashCode());
    result = prime * result + ((password == null) ? 0 : password.hashCode());
    result =
      prime * result + ((activationKey == null) ? 0 : activationKey.hashCode());
    result =
      prime *
      result +
      ((activationKeyTime == null) ? 0 : activationKeyTime.hashCode());
    result =
      prime *
      result +
      ((passwordResetKey == null) ? 0 : passwordResetKey.hashCode());
    result =
      prime *
      result +
      ((passwordResetTime == null) ? 0 : passwordResetTime.hashCode());
    result =
      prime *
      result +
      (
        (passwordExpirationDate == null) ? 0 : passwordExpirationDate.hashCode()
      );
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    FragJetztCredentials other = (FragJetztCredentials) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (email == null) {
      if (other.email != null) return false;
    } else if (!email.equals(other.email)) return false;
    if (password == null) {
      if (other.password != null) return false;
    } else if (!password.equals(other.password)) return false;
    if (activationKey == null) {
      if (other.activationKey != null) return false;
    } else if (!activationKey.equals(other.activationKey)) return false;
    if (activationKeyTime == null) {
      if (other.activationKeyTime != null) return false;
    } else if (!activationKeyTime.equals(other.activationKeyTime)) return false;
    if (passwordResetKey == null) {
      if (other.passwordResetKey != null) return false;
    } else if (!passwordResetKey.equals(other.passwordResetKey)) return false;
    if (passwordResetTime == null) {
      if (other.passwordResetTime != null) return false;
    } else if (!passwordResetTime.equals(other.passwordResetTime)) return false;
    if (passwordExpirationDate == null) {
      if (other.passwordExpirationDate != null) return false;
    } else if (
      !passwordExpirationDate.equals(other.passwordExpirationDate)
    ) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "FragJetztCredentials [id=" +
      id +
      ", email=" +
      email +
      ", password=" +
      password +
      ", activationKey=" +
      activationKey +
      ", activationKeyTime=" +
      activationKeyTime +
      ", passwordResetKey=" +
      passwordResetKey +
      ", passwordResetTime=" +
      passwordResetTime +
      ", passwordExpirationDate=" +
      passwordExpirationDate +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
