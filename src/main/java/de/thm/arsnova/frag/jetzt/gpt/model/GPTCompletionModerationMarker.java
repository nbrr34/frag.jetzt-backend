package de.thm.arsnova.frag.jetzt.gpt.model;

import de.thm.arsnova.frag.jetzt.gpt.model.rest.GPTModerationResponse;
import de.thm.arsnova.frag.jetzt.gpt.model.rest.IGPTStreamObject;

public class GPTCompletionModerationMarker implements IGPTStreamObject {

  private boolean done = true;
  private GPTModerationResponse response;

  public boolean isDone() {
    return done;
  }

  public void setDone(boolean done) {
    this.done = done;
  }

  public GPTModerationResponse getResponse() {
    return response;
  }

  public void setResponse(GPTModerationResponse response) {
    this.response = response;
  }
}
