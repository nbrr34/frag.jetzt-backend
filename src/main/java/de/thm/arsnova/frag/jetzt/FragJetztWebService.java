package de.thm.arsnova.frag.jetzt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@PropertySource(
        value = {"classpath:application.yml"},
        ignoreResourceNotFound = true,
        encoding = "UTF-8"
)
@EnableR2dbcRepositories
@EnableScheduling
public class FragJetztWebService {

    public static void main(String[] args) {
        SpringApplication.run(FragJetztWebService.class, args);
    }
}
