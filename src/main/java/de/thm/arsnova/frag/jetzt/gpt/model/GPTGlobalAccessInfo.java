package de.thm.arsnova.frag.jetzt.gpt.model;

public class GPTGlobalAccessInfo {

  private final boolean blocked;
  private final boolean registered;
  private final boolean apiKeyPresent;
  private final boolean apiEnabled;
  private final boolean globalActive;
  private final boolean apiExpired;
  private final boolean consented;
  private final boolean restricted;

  public GPTGlobalAccessInfo(
    boolean blocked,
    boolean registered,
    boolean apiKeyPresent,
    boolean apiEnabled,
    boolean globalActive,
    boolean apiExpired,
    boolean consented
  ) {
    this.blocked = blocked;
    this.registered = registered;
    this.apiKeyPresent = apiKeyPresent;
    this.apiEnabled = apiEnabled;
    this.globalActive = globalActive;
    this.apiExpired = apiExpired;
    this.consented = consented;
    this.restricted = blocked || !apiKeyPresent || !apiEnabled || apiExpired;
  }

  public boolean isBlocked() {
    return blocked;
  }

  public boolean isRegistered() {
    return registered;
  }

  public boolean isApiKeyPresent() {
    return apiKeyPresent;
  }

  public boolean isApiEnabled() {
    return apiEnabled;
  }

  public boolean isGlobalActive() {
    return globalActive;
  }

  public boolean isApiExpired() {
    return apiExpired;
  }

  public boolean isRestricted() {
    return restricted;
  }

  public boolean hasConsented() {
    return consented;
  }

  @Override
  public String toString() {
    return (
      "GPTGlobalAccessInfo [blocked=" +
      blocked +
      ", registered=" +
      registered +
      ", apiKeyPresent=" +
      apiKeyPresent +
      ", apiEnabled=" +
      apiEnabled +
      ", globalActive=" +
      globalActive +
      ", apiExpired=" +
      apiExpired +
      ", consented=" +
      consented +
      ", restricted=" +
      restricted +
      "]"
    );
  }
}
