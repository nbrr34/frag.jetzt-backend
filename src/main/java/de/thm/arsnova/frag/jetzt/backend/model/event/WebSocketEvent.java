package de.thm.arsnova.frag.jetzt.backend.model.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketMessage;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.UUID;

public class WebSocketEvent<P extends WebSocketPayload> extends WebSocketMessage<P> {
    // roomId of the entity the event is based on
    protected UUID roomId;

    public WebSocketEvent(String type) {
        super(type);
    }

    public WebSocketEvent(String type, UUID roomId) {
        super(type);
        this.roomId = roomId;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("roomId")
    public UUID getRoomId() {
        return roomId;
    }

    @JsonProperty("roomId")
    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    @Override
    public String toString() {
        return "WebSocketEvent{" +
                "type='" + type + '\'' +
                ", roomId='" + roomId+ '\'' +
                ", payload=" + payload.toString() +
                '}';
    }
}
