package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.model.Bookmark;
import de.thm.arsnova.frag.jetzt.backend.service.BookmarkFindQueryService;
import de.thm.arsnova.frag.jetzt.backend.service.BookmarkService;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController("BookmarkController")
@RequestMapping(BookmarkController.REQUEST_MAPPING)
public class BookmarkController extends AbstractEntityController {
    private static final Logger logger = LoggerFactory.getLogger(BookmarkController.class);

    protected static final String REQUEST_MAPPING = "/bookmark";

    private final BookmarkService service;
    private final BookmarkFindQueryService findQueryService;

    @Autowired
    public BookmarkController(
            BookmarkService service,
            BookmarkFindQueryService findQueryService
    ) {
        this.service = service;
        this.findQueryService = findQueryService;
    }

    @GetMapping(GET_MAPPING)
    public Mono<Bookmark> get(@PathVariable UUID id) {
        return this.service.getById(id);
    }

    @PostMapping(POST_MAPPING)
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Bookmark> post(@RequestBody Bookmark bookmark) {
        return this.service.create(bookmark);
    }

    @DeleteMapping(DELETE_MAPPING)
    public Mono<Void> delete(@PathVariable UUID id) {
        return this.service.delete(id);
    }

    @PostMapping(FIND_MAPPING)
    public Flux<Bookmark> find(@RequestBody final FindQuery<Bookmark> findQuery) {
        logger.debug("Resolving find query: {}", findQuery);
        return findQueryService.resolveQuery(findQuery);
    }

}
