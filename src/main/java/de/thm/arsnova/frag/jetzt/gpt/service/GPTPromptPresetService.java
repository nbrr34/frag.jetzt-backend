package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTPromptPreset;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTPromptPresetRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class GPTPromptPresetService {

  private final AuthorizationHelper helper;
  private final GPTPromptPresetRepository repository;

  @Autowired
  public GPTPromptPresetService(
    AuthorizationHelper helper,
    GPTPromptPresetRepository repository
  ) {
    this.helper = helper;
    this.repository = repository;
  }

  public Mono<GPTPromptPreset> add(boolean global, GPTPromptPreset prompt) {
    return helper
      .getCurrentUser()
      .filter(user -> !global || helper.isSuperAdmin(user))
      .switchIfEmpty(
        Mono.error(
          new ForbiddenException(
            "Global prompts can only be changed by administrators."
          )
        )
      )
      .flatMap(user -> {
        prompt.setAccountId(global ? null : user.getAccountId());
        return this.repository.save(prompt);
      });
  }

  public Mono<List<GPTPromptPreset>> getList(boolean global) {
    return Mono
      .zip(
        repository.findAllByAccountId(null).collectList(),
        global
          ? Mono.just(new ArrayList<GPTPromptPreset>())
          : helper
            .getCurrentUser()
            .flatMap(user ->
              repository.findAllByAccountId(user.getAccountId()).collectList()
            )
      )
      .map(tuple -> {
        ArrayList<GPTPromptPreset> data = new ArrayList<>(
          tuple.getT1().size() + tuple.getT2().size()
        );
        data.addAll(tuple.getT1());
        data.addAll(tuple.getT2());
        return data;
      });
  }

  public Mono<GPTPromptPreset> patch(UUID id, Map<String, Object> changes) {
    return Mono
      .zip(helper.getCurrentUser(), repository.findById(id))
      .switchIfEmpty(Mono.error(new NotFoundException()))
      .filter(tuple ->
        (
          tuple.getT2().getAccountId() == null &&
          helper.isSuperAdmin(tuple.getT1())
        ) ||
        tuple.getT1().getAccountId().equals(tuple.getT2().getAccountId())
      )
      .switchIfEmpty(
        Mono.error(new ForbiddenException("You do not own this preset"))
      )
      .map(tuple -> parseChanges(tuple.getT2(), changes))
      .flatMap(this::verify)
      .flatMap(repository::save);
  }

  public Mono<Void> delete(UUID id) {
    return Mono
      .zip(helper.getCurrentUser(), repository.findById(id))
      .switchIfEmpty(Mono.error(new NotFoundException()))
      .filter(tuple ->
        (
          tuple.getT2().getAccountId() == null &&
          helper.isSuperAdmin(tuple.getT1())
        ) ||
        tuple.getT1().getAccountId().equals(tuple.getT2().getAccountId())
      )
      .switchIfEmpty(
        Mono.error(new ForbiddenException("You do not own this preset"))
      )
      .flatMap(tuple -> repository.deleteById(id));
  }

  private Mono<GPTPromptPreset> verify(GPTPromptPreset preset) {
    return Mono
      .just(preset)
      .filter(prompt -> prompt.getAct() != null)
      .switchIfEmpty(Mono.error(new BadRequestException("Act can not be null")))
      .filter(prompt -> prompt.getPrompt() != null)
      .switchIfEmpty(
        Mono.error(new BadRequestException("Prompt can not be null"))
      );
  }

  private GPTPromptPreset parseChanges(
    GPTPromptPreset entity,
    Map<String, Object> changes
  ) {
    changes.forEach((key, value) -> {
      switch (key) {
        case "act":
          entity.setAct((String) value);
          break;
        case "prompt":
          entity.setPrompt((String) value);
          break;
        case "temperature":
          entity.setTemperature(((Number) value).floatValue());
          break;
        case "presencePenalty":
          entity.setPresencePenalty(((Number) value).floatValue());
          break;
        case "frequencyPenalty":
          entity.setFrequencyPenalty(((Number) value).floatValue());
          break;
        case "topP":
          entity.setTopP(((Number) value).floatValue());
          break;
        case "language":
          entity.setLanguage((String) value);
          break;
        case "id":
        case "accountId":
        case "createdAt":
        case "updatedAt":
          throw new ForbiddenException(
            "You are not allowed to change the " + key
          );
        default:
          throw new BadRequestException(
            "Invalid ChangeAttribute provided (" + key + ")"
          );
      }
    });
    return entity;
  }
}
