package de.thm.arsnova.frag.jetzt.backend.handler;

import de.thm.arsnova.frag.jetzt.backend.LivepollEventSource;
import de.thm.arsnova.frag.jetzt.backend.RoomEventSource;
import de.thm.arsnova.frag.jetzt.backend.model.command.CreateLivepollSession;
import de.thm.arsnova.frag.jetzt.backend.model.command.CreateLivepollSessionPayload;
import de.thm.arsnova.frag.jetzt.backend.model.command.CreateLivepollSessionPayload.CustomEntry;
import de.thm.arsnova.frag.jetzt.backend.model.command.DeleteLivepollSession;
import de.thm.arsnova.frag.jetzt.backend.model.command.DeleteLivepollSessionPayload;
import de.thm.arsnova.frag.jetzt.backend.model.command.DeleteLivepollVotes;
import de.thm.arsnova.frag.jetzt.backend.model.command.DeleteLivepollVotesPayload;
import de.thm.arsnova.frag.jetzt.backend.model.command.PatchLivepollSession;
import de.thm.arsnova.frag.jetzt.backend.model.command.PatchLivepollSessionPayload;
import de.thm.arsnova.frag.jetzt.backend.model.command.VoteLivepoll;
import de.thm.arsnova.frag.jetzt.backend.model.command.VoteLivepollPayload;
import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollCustomTemplateEntry;
import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollSession;
import de.thm.arsnova.frag.jetzt.backend.service.LivepollSessionService;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class LivepollCommandHandler {

  private static final Logger logger = LoggerFactory.getLogger(
    LivepollCommandHandler.class
  );

  private final LivepollSessionService service;
  private final RoomEventSource roomEventer;
  private final LivepollEventSource livepollEventer;

  public LivepollCommandHandler(
    final LivepollSessionService service,
    final RoomEventSource roomEventer,
    final LivepollEventSource livepollEventer
  ) {
    this.service = service;
    this.roomEventer = roomEventer;
    this.livepollEventer = livepollEventer;
  }

  public Mono<Void> handle(DeleteLivepollSession command) {
    logger.trace("got new command: " + command.toString());

    DeleteLivepollSessionPayload payload = command.getPayload();

    return service
      .delete(payload.getLivepollId())
      .doOnSuccess(tuple ->
        roomEventer.livepollDeleted(tuple.getT1(), tuple.getT2())
      )
      .then();
  }

  public Mono<Void> handle(DeleteLivepollVotes command) {
    logger.trace("got new command: " + command.toString());

    DeleteLivepollVotesPayload payload = command.getPayload();

    return service
      .deleteVotes(payload.getLivepollId())
      .doOnSuccess(ignore ->
        livepollEventer.sendScoreUpdated(payload.getLivepollId())
      )
      .then();
  }

  public Mono<LivepollSession> handle(CreateLivepollSession command) {
    logger.trace("got new command: " + command.toString());

    CreateLivepollSessionPayload payload = command.getPayload();
    LivepollSession s = new LivepollSession(
      payload.getRoomId(),
      payload.getTemplate(),
      payload.getTitle(),
      payload.isResultVisible(),
      payload.isViewsVisible(),
      payload.getAnswerCount()
    );
    if (payload.getCustomEntries() != null) {
      ArrayList<LivepollCustomTemplateEntry> list = new ArrayList<>(
        payload.getCustomEntries().size()
      );
      for (int i = 0; i < payload.getCustomEntries().size(); i++) {
        CustomEntry e = payload.getCustomEntries().get(i);
        list.add(new LivepollCustomTemplateEntry(i, e.getIcon(), e.getText()));
      }
      s.setCustomEntries(list);
    }
    return service.create(s).doOnSuccess(roomEventer::livepollCreated);
  }

  public Mono<Void> handle(VoteLivepoll vote) {
    VoteLivepollPayload p = vote.getPayload();
    if (p.getVoteIndex() == null) {
      return service
        .deleteVoteBySessionId(p.getSessionId())
        .doOnSuccess(ignore -> {
          livepollEventer.sendScoreUpdated(p.getSessionId());
        });
    }
    return service
      .createVoteBySessionId(p.getSessionId(), p.getVoteIndex())
      .doOnSuccess(ignore -> {
        livepollEventer.sendScoreUpdated(p.getSessionId());
      });
  }

  public Mono<LivepollSession> handle(PatchLivepollSession command) {
    logger.trace("got new command: " + command.toString());

    PatchLivepollSessionPayload payload = command.getPayload();
    return service
      .get(payload.getId())
      .switchIfEmpty(Mono.error(new NotFoundException("Entity not found")))
      .flatMap(entity -> service.patch(entity, payload.getChanges()))
      .doOnSuccess(livepoll ->
        roomEventer.livepollPatched(
          livepoll.getId(),
          livepoll.getRoomId(),
          payload.getChanges()
        )
      );
  }
}
