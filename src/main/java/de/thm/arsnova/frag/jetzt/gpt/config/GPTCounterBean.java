package de.thm.arsnova.frag.jetzt.gpt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class GPTCounterBean {

  @Bean
  public GPTCounters getCounters() {
    Runtime.getRuntime().addShutdownHook(new Thread(this::saveCounters));
    return GPTCounters.getInstance();
  }

  // Every Minute and on gracefully shutdown
  @Scheduled(fixedDelay = 60000, initialDelay = 60000)
  public void saveCounters() {
    GPTCounters.getInstance().save();
  }
}
