package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.BonusToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class BonusTokenFindQueryService {
    private final BonusTokenService bonusTokenService;

    @Autowired
    public BonusTokenFindQueryService(
            BonusTokenService bonusTokenService
    ) {
        this.bonusTokenService = bonusTokenService;
    }

    public Flux<BonusToken> resolveQuery(final FindQuery<BonusToken> findQuery) {
        if (findQuery.getProperties().getRoomId() != null) {
            return bonusTokenService.getByRoomId(findQuery.getProperties().getRoomId());

        } else if (findQuery.getProperties().getAccountId() != null) {
            return bonusTokenService.getByUserId(findQuery.getProperties().getAccountId());
        }
        return Flux.empty();
    }
}
