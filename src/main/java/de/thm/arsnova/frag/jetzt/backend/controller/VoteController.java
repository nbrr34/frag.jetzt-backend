package de.thm.arsnova.frag.jetzt.backend.controller;

import de.thm.arsnova.frag.jetzt.backend.handler.VoteCommandHandler;
import de.thm.arsnova.frag.jetzt.backend.model.AbstractVote;
import de.thm.arsnova.frag.jetzt.backend.model.command.Downvote;
import de.thm.arsnova.frag.jetzt.backend.model.command.ResetVote;
import de.thm.arsnova.frag.jetzt.backend.model.command.Upvote;
import de.thm.arsnova.frag.jetzt.backend.model.command.VotePayload;
import de.thm.arsnova.frag.jetzt.backend.service.FindQuery;
import de.thm.arsnova.frag.jetzt.backend.service.VoteFindQueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.UUID;

@RestController("VoteController")
@RequestMapping("/vote")
public class VoteController extends AbstractEntityController {
    private static final Logger logger = LoggerFactory.getLogger(VoteController.class);

    protected static final String REQUEST_MAPPING = "/vote";

    private final VoteCommandHandler commandHandler;
    private final VoteFindQueryService findQueryService;

    public static final class WebServiceVote {
        private int vote;
        private UUID accountId;
        private UUID commentId;

        public WebServiceVote() {
        }

        public WebServiceVote(int vote, UUID accountId, UUID commentId) {
            this.vote = vote;
            this.accountId = accountId;
            this.commentId = commentId;
        }

        public WebServiceVote(AbstractVote vote) {
            if (vote instanceof de.thm.arsnova.frag.jetzt.backend.model.Upvote) {
                this.vote = 1;
            } else if (vote instanceof de.thm.arsnova.frag.jetzt.backend.model.Downvote) {
                this.vote = -1;
            } else {
                this.vote = 0;
            }
            accountId = vote.getAccountId();
            commentId = vote.getCommentId();
        }

        public int getVote() {
            return vote;
        }

        public void setVote(int vote) {
            this.vote = vote;
        }

        public UUID getAccountId() {
            return accountId;
        }

        public void setAccountId(UUID accountId) {
            this.accountId = accountId;
        }

        public UUID getCommentId() {
            return commentId;
        }

        public void setCommentId(UUID commentId) {
            this.commentId = commentId;
        }

        @Override
        public String toString() {
            return "WebServiceVote{" +
                    "vote=" + vote +
                    ", accountId=" + accountId +
                    ", commentId=" + commentId +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            WebServiceVote that = (WebServiceVote) o;
            return vote == that.vote &&
                    Objects.equals(accountId, that.accountId) &&
                    Objects.equals(commentId, that.commentId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(vote, accountId, commentId);
        }
    }

    @Autowired
    public VoteController(
            final VoteCommandHandler commandHandler,
            final VoteFindQueryService findQueryService
    ) {
        this.commandHandler = commandHandler;
        this.findQueryService = findQueryService;
    }

    @PostMapping(POST_MAPPING)
    public Mono<WebServiceVote> post(
            @RequestBody final WebServiceVote vote
    ) {
        final VotePayload payload = new VotePayload(vote.getAccountId(), vote.getCommentId());
        if (vote.getVote() == 1)
            return commandHandler.handle(new Upvote(payload)).map(upvote -> vote);
        else if (vote.getVote() == -1)
            return commandHandler.handle(new Downvote(payload)).map(downvote -> vote);
        else
            return commandHandler.handle(new ResetVote(payload)).map(aVoid -> vote);
    }

    @PostMapping(FIND_MAPPING)
    public Flux<WebServiceVote> find(@RequestBody final FindQuery<WebServiceVote> findQuery) {
        logger.debug("Resolving find query: {}", findQuery);
        return findQueryService.resolveQuery(findQuery).map(WebServiceVote::new);
    }
}
