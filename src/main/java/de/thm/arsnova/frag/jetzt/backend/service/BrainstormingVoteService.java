package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession;
import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingVote;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.BrainstormingSessionRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.BrainstormingVoteRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.BrainstormingWordRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class BrainstormingVoteService {

    private static final Logger logger = LoggerFactory.getLogger(BrainstormingVoteService.class);

    private final AuthorizationHelper authorizationHelper;
    private final BrainstormingVoteRepository repository;
    private final BrainstormingWordRepository wordRepository;
    private final BrainstormingSessionRepository sessionRepository;
    private final RoomRepository roomRepository;
    private final AccountService accountService;

    @Autowired
    public BrainstormingVoteService(
            AuthorizationHelper authorizationHelper,
            BrainstormingVoteRepository repository,
            AccountService accountService,
            BrainstormingWordRepository wordRepository,
            BrainstormingSessionRepository sessionRepository,
            RoomRepository roomRepository
    ) {
        this.authorizationHelper = authorizationHelper;
        this.repository = repository;
        this.accountService = accountService;
        this.wordRepository = wordRepository;
        this.sessionRepository = sessionRepository;
        this.roomRepository = roomRepository;
    }

    public Mono<BrainstormingVote> create(BrainstormingVote vote) {
        vote.setId(null);
        return this.authorizationHelper.getCurrentUser()
                .switchIfEmpty(Mono.error(new NotFoundException("You are not logged in!")))
                .flatMap(user -> accountService.refreshLastActive(user.getAccountId(), Mono.just(user)))
                .flatMap(user -> {
                    vote.setAccountId(user.getAccountId());
                    return Mono.just(vote);
                })
                .flatMap(v -> this.repository.findByAccountIdAndWordId(v.getAccountId(), v.getWordId()))
                .flatMap(v1 -> {
                    vote.setId(v1.getId());
                    return Mono.just(vote);
                })
                .switchIfEmpty(Mono.just(vote))
                .flatMap(repository::save);
    }

    public Flux<Void> deleteAllBySessionId(UUID sessionId) {
        return this.sessionRepository.findById(sessionId)
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(this::checkRights)
                .flatMapMany(session -> this.wordRepository.findBySessionId(sessionId))
                .flatMap(word -> repository.deleteByWordId(word.getId()));
    }

    public Mono<Void> deleteByWordId(UUID wordId) {
        return this.authorizationHelper.getCurrentUser()
                .switchIfEmpty(Mono.error(new NotFoundException("You are not logged in!")))
                .flatMap(user -> accountService.refreshLastActive(user.getAccountId(), Mono.just(user)))
                .flatMap(user -> this.repository.deleteByAccountIdAndWordId(user.getAccountId(), wordId));
    }

    private Mono<BrainstormingSession> checkRights(BrainstormingSession session) {
        return Mono.zip(
                        authorizationHelper.getCurrentUser(),
                        roomRepository.findById(session.getRoomId())
                                .switchIfEmpty(Mono.error(NotFoundException::new))
                )
                .filter(tuple2 -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple2.getT1(), tuple2.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .map(tuple2 -> session);
    }
}
