package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class LivepollSessionCreated extends WebSocketEvent<LivepollSessionCreatedPayload> {
 
    public LivepollSessionCreated() {
        super(LivepollSessionCreated.class.getSimpleName());
    }

    public LivepollSessionCreated(LivepollSessionCreatedPayload payload, UUID roomId) {
        super(LivepollSessionCreated.class.getSimpleName(), roomId);
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LivepollSessionCreated that = (LivepollSessionCreated) o;
        return this.getPayload().equals(that.getPayload());
    }
    
}
