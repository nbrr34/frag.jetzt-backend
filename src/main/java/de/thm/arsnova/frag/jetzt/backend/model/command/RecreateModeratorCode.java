package de.thm.arsnova.frag.jetzt.backend.model.command;

public class RecreateModeratorCode extends WebSocketCommand<RecreateModeratorCodePayload> {

    public RecreateModeratorCode() {
        super(RecreateModeratorCodePayload.class.getSimpleName());
    }

    public RecreateModeratorCode(RecreateModeratorCodePayload payload) {
        super(RecreateModeratorCodePayload.class.getSimpleName());
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecreateModeratorCode that = (RecreateModeratorCode) o;
        return this.getPayload().equals(that.getPayload());
    }
}
