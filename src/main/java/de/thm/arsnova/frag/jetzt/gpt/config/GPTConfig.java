package de.thm.arsnova.frag.jetzt.gpt.config;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTMultiQuotaLimiter;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTMultiQuotaLimiter.MultiQuotaEntry;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTQuotaUnit;
import de.thm.arsnova.frag.jetzt.gpt.util.GPTQuotaType;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GPTConfig {

  private static final Logger logger = LoggerFactory.getLogger(GPTConfig.class);
  private static final ObjectMapper mapper = new ObjectMapper(
    new YAMLFactory()
  );
  private static final GPTConfig instance;

  public static GPTConfig getInstance() {
    return instance;
  }

  static {
    instance = load();
  }

  private static GPTConfig load() {
    File f = loadFile();
    if (f == null) {
      logger.warn("Error with file loading: Loading default ChatGPT Config");
      return new GPTConfig();
    }
    try {
      GPTConfig config = mapper.readValue(f, GPTConfig.class);
      config.file = f;
      return config;
    } catch (IOException e) {
      logger.error("Failed to parse GPTConfig file, using default!", e);
      return new GPTConfig();
    }
  }

  private static File loadFile() {
    File f = Paths.get("gpt_config.yml").toAbsolutePath().toFile();
    if (f.exists()) {
      return f;
    }
    InputStream is =
      GPTConfig.class.getClassLoader().getResourceAsStream("gpt_config.yml");
    if (is == null) {
      logger.error("Failed to load GPT Config! Config not found.");
      return null;
    }
    try {
      f.createNewFile();
      FileCopyUtils.copy(is, new FileOutputStream(f));
    } catch (IOException e) {
      logger.error("Failed to copy GPT Config!", e);
      return null;
    }
    return f;
  }

  public static class GPTActivationCode {

    private String code;
    private GPTMultiQuotaLimiter limiter;
    private UUID activatedRoomId;

    public GPTActivationCode() {
      code = "";
      ArrayList<MultiQuotaEntry> list = new ArrayList<>();
      list.add(
        new MultiQuotaEntry(
          GPTQuotaType.MONTHLY,
          new GPTQuotaUnit(0, 2),
          new GPTQuotaUnit(0, 8)
        )
      );
      limiter =
        new GPTMultiQuotaLimiter(
          LocalDateTime.now(),
          Collections.unmodifiableList(list)
        );
    }

    public GPTActivationCode(String code, int maximalCost) {
      this.code = code;
      ArrayList<MultiQuotaEntry> list = new ArrayList<>();
      list.add(
        new MultiQuotaEntry(
          GPTQuotaType.MONTHLY,
          new GPTQuotaUnit(maximalCost, 2),
          new GPTQuotaUnit(0, 8)
        )
      );
      limiter =
        new GPTMultiQuotaLimiter(
          LocalDateTime.now(),
          Collections.unmodifiableList(list)
        );
    }

    @JsonCreator
    public GPTActivationCode(
      @JsonProperty("code") String code,
      @JsonProperty("maximalCost") GPTQuotaUnit maximalCost,
      @JsonProperty("costCounter") GPTQuotaUnit costCounter,
      @JsonProperty("lastUse") Timestamp lastUse,
      @JsonProperty("activatedRoomId") UUID activatedRoomId
    ) {
      this.code = code;
      ArrayList<MultiQuotaEntry> list = new ArrayList<>();
      list.add(
        new MultiQuotaEntry(GPTQuotaType.MONTHLY, maximalCost, costCounter)
      );
      limiter =
        new GPTMultiQuotaLimiter(
          lastUse.toLocalDateTime(),
          Collections.unmodifiableList(list)
        );
      this.activatedRoomId = activatedRoomId;
    }

    public String getCode() {
      return code;
    }

    public GPTQuotaUnit getMaximalCost() {
      return limiter.getLimits().get(0).getLimit();
    }

    public UUID getActivatedRoomId() {
      return activatedRoomId;
    }

    public GPTQuotaUnit getCostCounter() {
      return limiter.getLimits().get(0).getCounter();
    }

    public Timestamp getLastUse() {
      return Timestamp.valueOf(limiter.getLastUpdate());
    }

    @JsonIgnore
    public GPTMultiQuotaLimiter getLimiter() {
      return limiter;
    }

    @JsonIgnore
    public boolean claim(UUID roomId) {
      synchronized (this) {
        if (activatedRoomId == null) {
          activatedRoomId = roomId;
          return true;
        }
        return false;
      }
    }

    @JsonIgnore
    public void unclaim() {
      synchronized (this) {
        activatedRoomId = null;
      }
    }
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class GPTRestrictions {

    private boolean active = false;
    private int globalAccumulatedQuota = 500;
    private boolean globalActive = false;
    private Timestamp endDate = Timestamp.from(Instant.now());
    private ArrayList<GPTActivationCode> platformCodes = new ArrayList<>();

    @JsonIgnore
    public GPTActivationCode getRoomCode(UUID roomId) {
      for (GPTActivationCode code : platformCodes) {
        if (roomId.equals(code.activatedRoomId)) {
          return code;
        }
      }
      return null;
    }

    @JsonIgnore
    public boolean canBeAccessed() {
      return (
        active &&
        (endDate == null || endDate.toInstant().isAfter(Instant.now()))
      );
    }

    public Timestamp getEndDate() {
      return endDate;
    }

    public void setEndDate(Timestamp endDate) {
      this.endDate = endDate;
    }

    public boolean isGlobalActive() {
      return globalActive;
    }

    public void setGlobalActive(boolean globalActive) {
      this.globalActive = globalActive;
    }

    public boolean isActive() {
      return active;
    }

    public void setActive(boolean active) {
      this.active = active;
    }

    public ArrayList<GPTActivationCode> getPlatformCodes() {
      return platformCodes;
    }

    public void setPlatformCodes(ArrayList<GPTActivationCode> platformCodes) {
      this.platformCodes = platformCodes;
    }

    public int getGlobalAccumulatedQuota() {
      return this.globalAccumulatedQuota;
    }

    public void setGlobalAccumulatedQuota(int quota) {
      this.globalAccumulatedQuota = quota;
    }

    @Override
    public String toString() {
      return (
        "GPTRestrictions [active=" +
        active +
        ", globalActive=" +
        globalActive +
        ", globalAccumulatedQuota=" +
        globalAccumulatedQuota +
        ", endDate=" +
        endDate +
        ", platformCodes=" +
        platformCodes +
        "]"
      );
    }

    public GPTRestrictions clone() {
      GPTRestrictions cloned = new GPTRestrictions();
      cloned.active = active;
      cloned.globalAccumulatedQuota = globalAccumulatedQuota;
      cloned.globalActive = globalActive;
      cloned.endDate = endDate;
      cloned.platformCodes = new ArrayList<>(platformCodes);
      return cloned;
    }
  }

  @JsonIgnore
  private File file = null;

  private String apiKey = null;
  private String organization = null;
  private GPTRestrictions restrictions = new GPTRestrictions();

  public GPTRestrictions getRestrictions() {
    return restrictions;
  }

  public void setRestrictions(GPTRestrictions restrictions) {
    this.restrictions = restrictions;
  }

  public String getOrganization() {
    return organization;
  }

  public void setOrganization(String organization) {
    this.organization = organization;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public void save() {
    if (this.file == null) {
      return;
    }
    try {
      mapper.writeValue(file, this);
    } catch (IOException e) {
      logger.error("Could not save ChatGPT Config!", e);
    }
  }

  @Override
  public String toString() {
    return (
      "GPTConfig [file=" +
      file +
      ", apiKey=" +
      apiKey +
      ", organization=" +
      organization +
      ", restrictions=" +
      restrictions +
      "]"
    );
  }

  public GPTConfig clone() {
    GPTConfig cloned = new GPTConfig();
    cloned.apiKey =
      apiKey == null
        ? null
        : "***************************************************";
    cloned.organization = organization;
    cloned.restrictions = restrictions.clone();
    return cloned;
  }
}
