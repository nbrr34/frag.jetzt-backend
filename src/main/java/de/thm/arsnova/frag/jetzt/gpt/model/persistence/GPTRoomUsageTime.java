package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.Nullable;

@Table
public class GPTRoomUsageTime implements Persistable<UUID> {

    public enum GPTRoomUsageRepeatUnit {
        HOUR {
            @Override
            public long toOffset(int multiplicator, Timestamp timestamp) {
                return timestamp.getTime() % (3_600_000L * multiplicator);
            }
        },
        DAY {
            @Override
            public long toOffset(int multiplicator, Timestamp timestamp) {
                return timestamp.getTime() % (86_400_000L * multiplicator);
            }
        },
        WEEK {
            @Override
            public long toOffset(int multiplicator, Timestamp timestamp) {
                return timestamp.getTime() % (604_800_000L * multiplicator);
            }
        },
        MONTH {
            @Override
            public long toOffset(int multiplicator, Timestamp timestamp) {
                final LocalDateTime time = timestamp.toLocalDateTime();
                long additionalMonths = (time.getYear() - 1970) * 12L + time.getMonth().getValue() - 1;
                additionalMonths = (additionalMonths / multiplicator) * multiplicator; // remove partial part
                final int year = (int) (additionalMonths / 12);
                final int month = (int) (additionalMonths % 12 + 1);
                final long ref = LocalDateTime.of(year, Month.of(month), 1, 0, 0, 0)
                        .atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                return timestamp.getTime() - ref;
            }
        },
        YEAR {
            @Override
            public long toOffset(int multiplicator, Timestamp timestamp) {
                final LocalDateTime time = timestamp.toLocalDateTime();
                final long ref = LocalDateTime
                        .of((time.getYear() / multiplicator) * multiplicator, 1, 1, 0, 0, 0)
                        .atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                return timestamp.getTime() - ref;
            }
        };

        public abstract long toOffset(int multiplicator, Timestamp timestamp);
    }

    @Id
    private UUID id;
    private UUID settingId;
    private Integer repeatDuration;
    private GPTRoomUsageRepeatUnit repeatUnit;
    private Timestamp startDate;
    private Timestamp endDate;
    // meta information
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    public GPTRoomUsageTime() {

    }

    public GPTRoomUsageTime(UUID settingId, Integer repeatDuration, GPTRoomUsageRepeatUnit repeatUnit,
            Timestamp startDate, Timestamp endDate) {
        if (startDate == null || endDate == null) {
            throw new IllegalArgumentException("startDate and endDate should not be null!");
        }
        this.settingId = settingId;
        this.repeatDuration = repeatDuration;
        this.repeatUnit = repeatUnit;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    @Nullable
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getSettingId() {
        return settingId;
    }

    public void setSettingId(UUID settingId) {
        this.settingId = settingId;
    }

    public Integer getRepeatDuration() {
        return repeatDuration;
    }

    public void setRepeatDuration(Integer repeatDuration) {
        this.repeatDuration = repeatDuration;
    }

    public GPTRoomUsageRepeatUnit getRepeatUnit() {
        return repeatUnit;
    }

    public void setRepeatUnit(GPTRoomUsageRepeatUnit repeatUnit) {
        this.repeatUnit = repeatUnit;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((settingId == null) ? 0 : settingId.hashCode());
        result = prime * result + ((repeatDuration == null) ? 0 : repeatDuration.hashCode());
        result = prime * result + ((repeatUnit == null) ? 0 : repeatUnit.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GPTRoomUsageTime other = (GPTRoomUsageTime) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (settingId == null) {
            if (other.settingId != null)
                return false;
        } else if (!settingId.equals(other.settingId))
            return false;
        if (repeatDuration == null) {
            if (other.repeatDuration != null)
                return false;
        } else if (!repeatDuration.equals(other.repeatDuration))
            return false;
        if (repeatUnit != other.repeatUnit)
            return false;
        if (startDate == null) {
            if (other.startDate != null)
                return false;
        } else if (!startDate.equals(other.startDate))
            return false;
        if (endDate == null) {
            if (other.endDate != null)
                return false;
        } else if (!endDate.equals(other.endDate))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "GPTRoomUsageTime [id=" + id + ", settingId=" + settingId + ", repeatDuration="
                + repeatDuration + ", repeatUnit=" + repeatUnit + ", startDate=" + startDate
                + ", endDate=" + endDate + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt
                + "]";
    }

}
