package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.CommentChange;
import de.thm.arsnova.frag.jetzt.backend.model.CommentChangeSubscription;
import de.thm.arsnova.frag.jetzt.backend.model.RoomCommentChangeSubscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

@Service
public class CommentChangeFindQueryService {
    private static final Logger logger = LoggerFactory.getLogger(CommentChangeFindQueryService.class);

    private final CommentChangeService service;
    private final CommentChangeSubscriptionService subscriptionService;
    private final RoomCommentChangeSubscriptionService roomSubscriptionService;

    @Autowired
    public CommentChangeFindQueryService(
            final CommentChangeService service,
            final CommentChangeSubscriptionService subscriptionService,
            final RoomCommentChangeSubscriptionService roomSubscriptionService
    ) {
        this.service = service;
        this.subscriptionService = subscriptionService;
        this.roomSubscriptionService = roomSubscriptionService;
    }

    public Flux<CommentChange> resolveQuery(final FindQuery<CommentChange> findQuery) {
        final Map<String, Object> external = findQuery.getExternalFilters();
        if (external != null && external.containsKey("lastAction")) {
            final Timestamp timestamp = new Timestamp(((Number) external.get("lastAction")).longValue());
            return Mono.zip(subscriptionService.get().map(CommentChangeSubscription::getCommentId).collectList(),
                            roomSubscriptionService.get().map(RoomCommentChangeSubscription::getRoomId).collectList())
                    .flatMapMany(tuple2 -> Flux.concat(
                            service.receiveMissedCommentInformationSince(tuple2.getT1(), timestamp),
                            service.receiveMissedRoomInformationSince(tuple2.getT2(), timestamp)
                    ));
        }
        return Flux.empty();
    }
}
