package de.thm.arsnova.frag.jetzt.backend.model.command;

public class VoteLivepoll extends WebSocketCommand<VoteLivepollPayload> {

  public VoteLivepoll() {
    super(VoteLivepoll.class.getSimpleName());
  }

  public VoteLivepoll(VoteLivepollPayload payload) {
    super(VoteLivepoll.class.getSimpleName());
    this.payload = payload;
  }
}
