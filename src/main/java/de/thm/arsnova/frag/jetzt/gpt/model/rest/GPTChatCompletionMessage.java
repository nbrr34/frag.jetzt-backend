package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import de.thm.arsnova.frag.jetzt.gpt.util.encoding.GPTEncoder;

@JsonInclude(Include.NON_NULL)
public class GPTChatCompletionMessage {

  private String role = "user";
  private String content = "";
  private String name = null;

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    boolean isSystem = "system".equals(role);
    if (!isSystem && !"assistant".equals(role) && !"user".equals(role)) {
      throw new IllegalArgumentException(
        "role must be either 'system', 'user' or 'assistant'"
      );
    }
    if (!isSystem) {
      name = null;
    }
    this.role = role;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    if (content == null) {
      throw new IllegalArgumentException("content can not be null!");
    }
    this.content = content;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    if (name != null) {
      this.role = "system";
    }
    this.name = name;
  }

  @JsonIgnore
  public long getTokens(GPTEncoder encoder) {
    /*
     * every message follows <im_start>{role/name}\n{content}<im_end>\n (4 Tokens)
     * <br/><br/>
     * if there's a name, the role is omitted. role is always required and always 1 token.
     */
    return (
      4 +
      encoder.getEncodeCount(name != null ? name : role) +
      encoder.getEncodeCount(content)
    );
  }

  @JsonIgnore
  public String wrap() {
    return new StringBuilder("<im_start>")
      .append(name != null ? name : role)
      .append("\n")
      .append(content)
      .append("<im_end>\n")
      .toString();
  }
}
