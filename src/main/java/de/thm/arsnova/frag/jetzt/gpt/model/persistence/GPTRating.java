package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Table
public class GPTRating implements Persistable<UUID> {

  @Id
  private UUID id;

  private UUID accountId;
  private float rating;
  private String ratingText;
  // meta information
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  public GPTRating() {}

  public GPTRating(UUID accountId, float rating, String ratingText) {
    this.accountId = accountId;
    this.rating = rating;
    this.ratingText = ratingText;
  }

  @JsonIgnore
  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getAccountId() {
    return accountId;
  }

  public void setAccountId(UUID accountId) {
    this.accountId = accountId;
  }

  public float getRating() {
    return rating;
  }

  public void setRating(float rating) {
    this.rating = rating;
  }

  public String getRatingText() {
    return ratingText;
  }

  public void setRatingText(String ratingText) {
    this.ratingText = ratingText;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
    result = prime * result + Float.floatToIntBits(rating);
    result =
      prime * result + ((ratingText == null) ? 0 : ratingText.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTRating other = (GPTRating) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (accountId == null) {
      if (other.accountId != null) return false;
    } else if (!accountId.equals(other.accountId)) return false;
    if (
      Float.floatToIntBits(rating) != Float.floatToIntBits(other.rating)
    ) return false;
    if (ratingText == null) {
      if (other.ratingText != null) return false;
    } else if (!ratingText.equals(other.ratingText)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTRating [id=" +
      id +
      ", accountId=" +
      accountId +
      ", rating=" +
      rating +
      ", ratingText=" +
      ratingText +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      "]"
    );
  }
}
