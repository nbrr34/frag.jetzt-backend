package de.thm.arsnova.frag.jetzt.gpt.model;

import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomPresetTopic;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomSetting.GPTRoomSettingRight;
import java.util.List;
import java.util.stream.Collectors;

public class GPTRoomPreset {

  public static class PresetTopic {

    private String description;
    private boolean active;

    public PresetTopic() {}

    public PresetTopic(GPTRoomPresetTopic topic) {
      this.description = topic.getDescription();
      this.active = topic.isActive();
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(String description) {
      this.description = description;
    }

    public boolean isActive() {
      return active;
    }

    public void setActive(boolean active) {
      this.active = active;
    }
  }

  private String context;
  private List<PresetTopic> topics;
  private String length;
  private boolean disableEnhancedPrompt;
  private boolean disableForwardMessage;
  private String roleInstruction;

  public GPTRoomPreset() {}

  public GPTRoomPreset(
    GPTRoomSetting setting,
    List<GPTRoomPresetTopic> topics
  ) {
    context = setting.getPresetContext();
    this.topics =
      topics
        .stream()
        .map(topic -> new PresetTopic(topic))
        .collect(Collectors.toList());
    length = setting.getPresetLength();
    disableEnhancedPrompt =
      GPTRoomSettingRight.DISABLE_ENHANCED_PROMPT.isRightSet(
        setting.getRightsBitset()
      );
    disableForwardMessage =
      GPTRoomSettingRight.DISABLE_FORWARD_MESSAGE.isRightSet(
        setting.getRightsBitset()
      );
    roleInstruction = setting.getRoleInstruction();
  }

  public String getContext() {
    return context;
  }

  public void setContext(String context) {
    this.context = context;
  }

  public List<PresetTopic> getTopics() {
    return topics;
  }

  public void setTopics(List<PresetTopic> topics) {
    this.topics = topics;
  }

  public String getLength() {
    return length;
  }

  public void setLength(String length) {
    this.length = length;
  }

  public String getRoleInstruction() {
    return roleInstruction;
  }

  public void setRoleInstruction(String roleInstruction) {
    this.roleInstruction = roleInstruction;
  }

  public boolean isDisableEnhancedPrompt() {
    return disableEnhancedPrompt;
  }

  public void setDisableEnhancedPrompt(boolean disableEnhancedPrompt) {
    this.disableEnhancedPrompt = disableEnhancedPrompt;
  }

  public boolean isDisableForwardMessage() {
    return disableForwardMessage;
  }

  public void setDisableForwardMessage(boolean disableForwardMessage) {
    this.disableForwardMessage = disableForwardMessage;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((context == null) ? 0 : context.hashCode());
    result = prime * result + ((topics == null) ? 0 : topics.hashCode());
    result = prime * result + ((length == null) ? 0 : length.hashCode());
    result = prime * result + (disableEnhancedPrompt ? 1231 : 1237);
    result = prime * result + (disableForwardMessage ? 1231 : 1237);
    result =
      prime *
      result +
      ((roleInstruction == null) ? 0 : roleInstruction.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTRoomPreset other = (GPTRoomPreset) obj;
    if (context == null) {
      if (other.context != null) return false;
    } else if (!context.equals(other.context)) return false;
    if (topics == null) {
      if (other.topics != null) return false;
    } else if (!topics.equals(other.topics)) return false;
    if (length == null) {
      if (other.length != null) return false;
    } else if (!length.equals(other.length)) return false;
    if (disableEnhancedPrompt != other.disableEnhancedPrompt) return false;
    if (disableForwardMessage != other.disableForwardMessage) return false;
    if (roleInstruction == null) {
      if (other.roleInstruction != null) return false;
    } else if (!roleInstruction.equals(other.roleInstruction)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTRoomPreset [context=" +
      context +
      ", topics=" +
      topics +
      ", length=" +
      length +
      ", disableEnhancedPrompt=" +
      disableEnhancedPrompt +
      ", disableForwardMessage=" +
      disableForwardMessage +
      ", roleInstruction=" +
      roleInstruction +
      "]"
    );
  }
}
