package de.thm.arsnova.frag.jetzt.backend.service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class MailScheduler {

  protected static final Logger logger = LoggerFactory.getLogger(
    MailScheduler.class
  );

  private static final class Mail implements Serializable {

    private final String subject;
    private final String body;

    public Mail(String subject, String body) {
      this.subject = subject;
      this.body = body;
    }

    public String getSubject() {
      return subject;
    }

    public String getBody() {
      return body;
    }
  }

  private static final class MailQueueEntry implements Serializable {

    private final Mail mail;
    private final String mailAddress;
    private final String domain;

    public MailQueueEntry(Mail mail, String mailAddress) {
      this.mail = mail;
      this.mailAddress = mailAddress;
      int start = mailAddress.indexOf("@");
      if (start < 0) {
        throw new IllegalArgumentException(
          "Mail address '" + mailAddress + "' not valid"
        );
      }
      int temp = mailAddress.lastIndexOf(".");
      if (temp < 0) {
        throw new IllegalArgumentException(
          "Mail address '" + mailAddress + "' not valid"
        );
      }
      int domainStart = mailAddress.indexOf(".", start);
      if (domainStart < 0 || domainStart >= temp) {
        this.domain =
          mailAddress.substring(start + 1, mailAddress.length()).toLowerCase();
      } else {
        this.domain =
          mailAddress
            .substring(
              mailAddress.lastIndexOf(".", temp - 1) + 1,
              mailAddress.length()
            )
            .toLowerCase();
      }
    }

    public Mail getMail() {
      return mail;
    }

    public String getMailAddress() {
      return mailAddress;
    }

    public String getDomain() {
      return domain;
    }
  }

  private static final class MailQueueConstructor {

    final ArrayList<Integer> list;
    final int offset;
    int counter;
    int incCount = 1;

    MailQueueConstructor(ArrayList<Integer> list, int size) {
      this.list = list;
      this.offset = size / (2 * list.size());
      counter = offset;
    }

    int get() {
      return list.get(incCount - 1);
    }

    boolean calculateNext(int size) {
      counter = offset + size * incCount++ / list.size();
      return incCount > list.size();
    }
  }

  private static final class MailQueue extends ArrayList<MailQueueEntry> {

    public synchronized void readData(DataInputStream dis) throws IOException {
      clear();
      final int mailCount = dis.readInt();
      ArrayList<Mail> mails = new ArrayList<>(mailCount);
      for (int i = 0; i < mailCount; i++) {
        final String subject = dis.readUTF();
        final String body = dis.readUTF();
        mails.add(new Mail(subject, body));
      }
      final int entryCount = dis.readInt();
      ensureCapacity(entryCount);
      for (int i = 0; i < entryCount; i++) {
        final int mailIndex = dis.readInt();
        final String mailAddress = dis.readUTF();
        Mail m = mails.get(mailIndex);
        add(new MailQueueEntry(m, mailAddress));
      }
    }

    public synchronized void writeData(DataOutputStream dos)
      throws IOException {
      ArrayList<Mail> mails = new ArrayList<>();
      for (MailQueueEntry e : this) {
        int index = mails.indexOf(e.getMail());
        if (index < 0) {
          index = mails.size();
          mails.add(e.getMail());
        }
      }
      dos.writeInt(mails.size());
      for (Mail m : mails) {
        dos.writeUTF(m.subject);
        dos.writeUTF(m.body);
      }
      dos.writeInt(size());
      for (MailQueueEntry e : this) {
        dos.writeInt(mails.indexOf(e.getMail()));
        dos.writeUTF(e.mailAddress);
      }
    }

    public synchronized void add(
      Collection<String> emails,
      String subject,
      String body
    ) {
      Mail m = new Mail(subject, body);
      this.ensureCapacity(this.size() + emails.size());
      emails
        .stream()
        .map(email -> {
          try {
            return new MailQueueEntry(m, email);
          } catch (IllegalArgumentException e) {
            logger.error("Mail scheduling:", e);
            return null;
          }
        })
        .filter(entry -> entry != null)
        .forEach(entry -> this.add(entry));
      LinkedHashMap<String, ArrayList<Integer>> positions = new LinkedHashMap<>();
      final int size = size();
      for (int i = 0; i < size; i++) {
        positions
          .computeIfAbsent(get(i).getDomain(), k -> new ArrayList<>())
          .add(i);
      }
      final ArrayList<Integer> skip = new ArrayList<>();
      final ArrayList<MailQueueEntry> q = new ArrayList<>(size);
      final LinkedList<MailQueueConstructor> constructors = new LinkedList<>();
      positions.forEach((key, value) -> {
        if (value.size() < 2) {
          return;
        }
        skip.addAll(value);
        constructors.add(new MailQueueConstructor(value, size));
      });
      skip.sort(Integer::compareTo);
      int skipOffset = 0;
      for (int i = 0, k = 0; i < size; i++) {
        Iterator<MailQueueConstructor> it = constructors.iterator();
        while (it.hasNext()) {
          MailQueueConstructor c = it.next();
          if (c.counter == i) {
            q.add(get(c.get()));
            if (c.calculateNext(size)) {
              it.remove();
            }
          }
        }
        while (skipOffset < skip.size() && skip.get(skipOffset) == k) {
          k++;
          skipOffset++;
        }
        if (k < size) {
          q.add(get(k++));
        }
      }
      clear();
      addAll(q);
    }

    public List<MailQueueEntry> getNextTick() {
      return this.subList(0, Math.min(15, this.size()));
    }
  }

  private final MailQueue queue = new MailQueue();
  private final JavaMailSender mailSender;

  @Value("${server.root-url}")
  private String rootUrl;

  @Value("${app.mail.sender-name}")
  private String mailSenderName;

  @Value("${app.mail.sender-address}")
  private String mailSenderAddress;

  @Value("${app.registration.domains}")
  private String[] allowedEmailDomains;

  private int count = 0;

  @Autowired
  public MailScheduler(JavaMailSender mailSender) {
    this.mailSender = mailSender;
    load();
    Runtime.getRuntime().addShutdownHook(new Thread(this::save));
  }

  @Scheduled(fixedDelay = 10000)
  public void onTick() {
    synchronized (queue) {
      List<MailQueueEntry> list = queue.getNextTick();
      for (MailQueueEntry e : list) {
        sendEmail(
          e.getMailAddress(),
          e.getMail().getSubject(),
          e.getMail().getBody()
        );
      }
      list.clear();
    }
    if (++count == 5) {
      count = 0;
      save();
    }
  }

  public void load() {
    File f = new File("mails.db");
    if (!f.exists()) {
      queue.clear();
      return;
    }
    try (DataInputStream dis = new DataInputStream(new FileInputStream(f))) {
      queue.readData(dis);
    } catch (IOException e) {
      logger.error("Cant load mails.db", e);
    }
  }

  public void save() {
    File f = new File("mails.db");
    if (!f.exists()) {
      try {
        f.createNewFile();
      } catch (IOException e) {
        logger.error("Cant save mails.db", e);
      }
    }
    try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(f))) {
      queue.writeData(dos);
    } catch (IOException e) {
      logger.error("Could not save mails.db", e);
    }
  }

  public void addEmails(
    Collection<String> emails,
    String subject,
    String body
  ) {
    queue.add(emails, subject, body);
  }

  private void sendEmail(String destMail, String subject, String body) {
    final MimeMessage msg = mailSender.createMimeMessage();
    final MimeMessageHelper helper = new MimeMessageHelper(
      msg,
      StandardCharsets.UTF_8.displayName()
    );
    try {
      helper.setFrom(mailSenderName + "<" + mailSenderAddress + ">");
      helper.setTo(destMail);
      helper.setSubject(subject);
      helper.setText(body);

      logger.info(
        "Sending mail \"{}\" from \"{}\" to \"{}\"",
        subject,
        msg.getFrom(),
        destMail
      );
      mailSender.send(msg);
    } catch (final MailException | MessagingException e) {
      logger.warn("Mail \"{}\" could not be sent.", subject, e);
    }
  }
}
