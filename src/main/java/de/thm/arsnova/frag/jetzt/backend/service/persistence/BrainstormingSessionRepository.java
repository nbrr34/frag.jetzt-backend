package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.BrainstormingSession;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.UUID;

public interface BrainstormingSessionRepository extends ReactiveCrudRepository<BrainstormingSession, UUID> {

    Flux<BrainstormingSession> findByRoomId(UUID roomId);

}
