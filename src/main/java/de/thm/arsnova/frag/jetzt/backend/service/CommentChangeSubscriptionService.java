package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.CommentChangeSubscription;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.CommentChangeSubscriptionRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class CommentChangeSubscriptionService {

    private static final Logger logger = LoggerFactory.getLogger(CommentChangeSubscriptionService.class);

    private final CommentChangeSubscriptionRepository repository;
    private final AuthorizationHelper authorizationHelper;
    private final CommentService commentService;

    @Autowired
    public CommentChangeSubscriptionService(
            final CommentChangeSubscriptionRepository repository,
            final AuthorizationHelper authorizationHelper,
            final CommentService commentService
    ) {
        this.repository = repository;
        this.authorizationHelper = authorizationHelper;
        this.commentService = commentService;
    }

    public Flux<CommentChangeSubscription> get() {
        return authorizationHelper.getCurrentUser()
                .flatMapMany(user -> repository.findByAccountId(user.getAccountId()));
    }

    public Mono<CommentChangeSubscription> create(UUID commentId) {
        return Mono.zip(authorizationHelper.getCurrentUser(), commentService.get(commentId))
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .map(tuple2 -> {
                    CommentChangeSubscription subscription = new CommentChangeSubscription();
                    subscription.setCommentId(commentId);
                    subscription.setAccountId(tuple2.getT1().getAccountId());
                    subscription.setRoomId(tuple2.getT2().getRoomId());
                    return subscription;
                })
                .flatMap(repository::save);
    }

    public Mono<Void> deleteByCommentId(UUID commentId) {
        return authorizationHelper.getCurrentUser()
                .switchIfEmpty(Mono.error(NotFoundException::new))
                .flatMap(user -> repository.deleteByAccountIdAndCommentId(user.getAccountId(), commentId));
    }

}
