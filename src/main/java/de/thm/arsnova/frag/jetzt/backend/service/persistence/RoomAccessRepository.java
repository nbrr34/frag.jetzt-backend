package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface RoomAccessRepository extends ReactiveCrudRepository<RoomAccess, UUID> {
    Flux<RoomAccess> findByRoomId(UUID roomId);
    Flux<RoomAccess> findByAccountId(UUID accountId);
    Mono<RoomAccess> findByRoomIdAndAccountId(UUID roomId, UUID accountId);
    Mono<Void> deleteByRoomIdAndAccountId(UUID roomId, UUID accountId);
}
