package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.FragJetztCredentials;

import java.util.Date;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface FragJetztCredentialRepository extends ReactiveCrudRepository<FragJetztCredentials, UUID> {
    Mono<FragJetztCredentials> findByEmailIgnoreCase(String email);
    Mono<Void> deleteByEmail(String email);
    Flux<FragJetztCredentials> findByActivationKeyTimeLessThanAndActivationKeyIsNotNull(Date activationKeyTime);
}
