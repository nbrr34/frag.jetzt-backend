package de.thm.arsnova.frag.jetzt.backend.model.event;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class VoteCreatedPayload implements WebSocketPayload {
    private UUID commentId;
    private int vote;

    public VoteCreatedPayload() {
    }

    public UUID getCommentId() {
        return commentId;
    }

    public void setCommentId(UUID commentId) {
        this.commentId = commentId;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }

    @Override
    public String toString() {
        return "VoteCreatedPayload{" +
                ", commentId='" + commentId + '\'' +
                ", vote=" + vote +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoteCreatedPayload that = (VoteCreatedPayload) o;
        return vote == that.vote &&
                Objects.equals(commentId, that.commentId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(commentId, vote);
    }
}
