package de.thm.arsnova.frag.jetzt.backend.model.command;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class ResetBrainstormingVotesPayload implements WebSocketPayload {
    private UUID sessionId;

    public ResetBrainstormingVotesPayload() {
    }

    public ResetBrainstormingVotesPayload(UUID sessionId) {
        this.sessionId = sessionId;
    }

    public UUID getSessionId() {
        return sessionId;
    }

    public void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return "ResetBrainstormingVotesPayload{" +
                "sessionId=" + sessionId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResetBrainstormingVotesPayload that = (ResetBrainstormingVotesPayload) o;
        return Objects.equals(sessionId, that.sessionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionId);
    }
}
