package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Room;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.sql.Timestamp;
import java.util.UUID;

@Repository
public interface RoomRepository extends ReactiveCrudRepository<Room, UUID> {
    Flux<Room> findByOwnerId(UUID roomId);
    Mono<Room> findByShortId(String shortId);
    Mono<Room> findByModeratorRoomReference(UUID moderatorRoomReference);
    Flux<Room> findByCreatedAtLessThanEqualAndLastVisitCreatorLessThanEqual(Timestamp timestamp, Timestamp timestamp2);
}
