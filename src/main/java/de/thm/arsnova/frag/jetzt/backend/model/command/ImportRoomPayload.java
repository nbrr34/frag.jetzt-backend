package de.thm.arsnova.frag.jetzt.backend.model.command;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class ImportRoomPayload implements WebSocketPayload {

    private UUID id;
    private int requiredGuestCount;

    public ImportRoomPayload() {

    }

    public ImportRoomPayload(UUID id, int requiredGuestCount) {
        this.id = id;
        this.requiredGuestCount = requiredGuestCount;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getRequiredGuestCount() {
        return requiredGuestCount;
    }

    public void setRequiredGuestCount(int requiredGuestCount) {
        this.requiredGuestCount = requiredGuestCount;
    }

    @Override
    public String toString() {
        return "ImportRoomPayload{" +
                "id=" + id +
                ", requiredGuestCount=" + requiredGuestCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImportRoomPayload that = (ImportRoomPayload) o;
        return requiredGuestCount == that.requiredGuestCount && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, requiredGuestCount);
    }
}
