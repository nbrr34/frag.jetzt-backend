package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Table
public class Downvote extends AbstractVote {

    public Downvote(final UUID accountId, final UUID commentId) {
        super(accountId, commentId);
    }

}
