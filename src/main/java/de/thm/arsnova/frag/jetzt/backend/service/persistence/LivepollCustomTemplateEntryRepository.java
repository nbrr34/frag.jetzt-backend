package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.livepoll.LivepollCustomTemplateEntry;
import java.util.UUID;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface LivepollCustomTemplateEntryRepository extends ReactiveCrudRepository<LivepollCustomTemplateEntry, UUID> {
  Flux<LivepollCustomTemplateEntry> findBySessionId(UUID sessionId);
}
