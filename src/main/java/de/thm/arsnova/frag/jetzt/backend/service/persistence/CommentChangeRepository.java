package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.CommentChange;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface CommentChangeRepository extends ReactiveCrudRepository<CommentChange, UUID> {

    Flux<CommentChange> findByRoomIdAndCreatedAtGreaterThanEqual(UUID roomId, Timestamp timestamp);

    Flux<CommentChange> findByRoomIdInAndTypeAndCreatedAtGreaterThanEqual(List<UUID> roomIds, CommentChange.CommentChangeType type, Timestamp timestamp);

    Flux<CommentChange> findByCommentIdInAndTypeIsNotAndCreatedAtGreaterThanEqual(List<UUID> commentIds, CommentChange.CommentChangeType type, Timestamp timestamp);

    @Transactional
    Flux<Void> deleteByCreatedAtLessThanEqual(Timestamp timestamp);
}
