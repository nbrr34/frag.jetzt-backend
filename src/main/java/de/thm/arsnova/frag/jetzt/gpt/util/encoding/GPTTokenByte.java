package de.thm.arsnova.frag.jetzt.gpt.util.encoding;

import java.util.Arrays;

public class GPTTokenByte implements Comparable<GPTTokenByte> {

  private int[] intData;

  public GPTTokenByte(byte[] data) {
    intData = new int[(data.length + 3) / 4];
    for (int i = 0, k = 0; i < intData.length; i++) {
      int current = 0;
      for (int j = 0, l = 24; j < 4 && k < data.length; j++, k++, l -= 8) {
        current |= (data[k] & 0xFF) << l;
      }
      intData[i] = current;
    }
  }

  @Override
  public int compareTo(GPTTokenByte a) {
      final int min = Math.min(intData.length, a.intData.length);
      for (int i = 0; i < min; i++) {
        final int compare = Integer.compareUnsigned(intData[i], a.intData[i]);
        if (compare != 0) {
            return compare;
        }
      }
      if (intData.length == a.intData.length) {
        return 0;
      }
      return intData.length < a.intData.length ? -1 : 1;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + Arrays.hashCode(intData);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTTokenByte other = (GPTTokenByte) obj;
    if (!Arrays.equals(intData, other.intData)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "GPTTokenByte [intData=" + Arrays.toString(intData) + "]";
  }
}
