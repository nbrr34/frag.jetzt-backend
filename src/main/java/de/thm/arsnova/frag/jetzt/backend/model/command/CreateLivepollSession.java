package de.thm.arsnova.frag.jetzt.backend.model.command;

public class CreateLivepollSession
  extends WebSocketCommand<CreateLivepollSessionPayload> {

  public CreateLivepollSession() {
    super(CreateLivepollSession.class.getSimpleName());
  }

  public CreateLivepollSession(CreateLivepollSessionPayload p) {
    super(CreateLivepollSession.class.getSimpleName());
    this.payload = p;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CreateLivepollSession that = (CreateLivepollSession) o;
    return this.getPayload().equals(that.getPayload());
  }
}
