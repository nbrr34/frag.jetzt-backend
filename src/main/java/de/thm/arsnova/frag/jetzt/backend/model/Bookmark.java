package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Table
public class Bookmark implements Persistable<UUID> {

    @Id
    private UUID id;
    private UUID accountId;
    private UUID roomId;
    private UUID commentId;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    public Bookmark() {
    }

    public Bookmark(UUID accountId, UUID roomId, UUID commentId) {
        this.accountId = accountId;
        this.roomId = roomId;
        this.commentId = commentId;
    }

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public UUID getCommentId() {
        return commentId;
    }

    public void setCommentId(UUID commentId) {
        this.commentId = commentId;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "Bookmark{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", roomId=" + roomId +
                ", commentId=" + commentId +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bookmark bookmark = (Bookmark) o;
        return Objects.equals(id, bookmark.id) &&
                Objects.equals(accountId, bookmark.accountId) &&
                Objects.equals(roomId, bookmark.roomId) &&
                Objects.equals(commentId, bookmark.commentId) &&
                Objects.equals(createdAt, bookmark.createdAt) &&
                Objects.equals(updatedAt, bookmark.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountId, roomId, commentId, createdAt, updatedAt);
    }
}
