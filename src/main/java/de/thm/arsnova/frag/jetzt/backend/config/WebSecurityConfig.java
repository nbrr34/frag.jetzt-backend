package de.thm.arsnova.frag.jetzt.backend.config;

import de.thm.arsnova.frag.jetzt.backend.security.AuthenticationManager;
import de.thm.arsnova.frag.jetzt.backend.security.SecurityContextRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Mono;

@Configuration
@EnableWebFluxSecurity
public class WebSecurityConfig {

    private final SecurityContextRepository securityContextRepository;
    private final AuthenticationManager authenticationManager;

    private static final String[] AUTH_WHITELIST = {
            "/auth/login/guest",
            "/auth/login/registered",
            "/auth/password-dictionary/{password}",
            "/auth/login",
            "/user/register",
            "/user/{id}/activate",
            "/user/{id}/resetactivation",
            "/user/{id}/resetpassword",
            "/rating/accumulated",
            "/swagger-ui/index.html", 
            "/swagger-resources/**",
            "/v3/api-docs**", 
            "/webjars/**", 
            "/swaggerfox.js", 
            "/swagger-ui/**"
    };

    @Autowired
    public WebSecurityConfig(SecurityContextRepository securityContextRepository, AuthenticationManager authenticationManager) {
        this.securityContextRepository = securityContextRepository;
        this.authenticationManager = authenticationManager;
    }

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http
                .exceptionHandling()
                .authenticationEntryPoint((swe, e) ->
                        Mono.fromRunnable(() ->
                                swe.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED)
                        )
                ).and()
                .csrf().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .securityContextRepository(securityContextRepository)
                .authenticationManager(authenticationManager)
                .authorizeExchange()
                .pathMatchers(HttpMethod.OPTIONS).permitAll()
                .pathMatchers(AUTH_WHITELIST).permitAll()
                .anyExchange().authenticated()
                .and().build();
    }
}