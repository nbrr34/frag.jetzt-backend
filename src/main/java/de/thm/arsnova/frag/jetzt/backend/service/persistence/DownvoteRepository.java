package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Downvote;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

public interface DownvoteRepository extends ReactiveCrudRepository<Downvote, UUID> {
    Flux<Downvote> findByCommentId(UUID commentId);

    Mono<Downvote> findByCommentIdAndAccountId(UUID commentId, UUID accountId);

    Flux<Downvote> findAllByAccountIdAndCommentIdIn(UUID accountId, List<UUID> commentId);

    Mono<Void> deleteByAccountIdAndCommentId(UUID accountId, UUID commentId);
}
