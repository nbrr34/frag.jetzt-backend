package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;

public class BrainstormingWordPatched extends WebSocketEvent<BrainstormingWordPatchedPayload> {

    public BrainstormingWordPatched() {
        super(BrainstormingWordPatched.class.getSimpleName());
    }

    public BrainstormingWordPatched(BrainstormingWordPatchedPayload payload, UUID roomId) {
        super(BrainstormingWordPatched.class.getSimpleName());
        this.payload = payload;
        this.roomId = roomId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingWordPatched that = (BrainstormingWordPatched) o;
        return this.getPayload().equals(that.getPayload());
    }
}
