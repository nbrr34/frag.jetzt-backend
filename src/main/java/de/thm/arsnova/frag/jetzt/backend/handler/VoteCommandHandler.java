package de.thm.arsnova.frag.jetzt.backend.handler;

import de.thm.arsnova.frag.jetzt.backend.CommentEventSource;
import de.thm.arsnova.frag.jetzt.backend.model.command.Downvote;
import de.thm.arsnova.frag.jetzt.backend.model.command.ResetVote;
import de.thm.arsnova.frag.jetzt.backend.model.command.Upvote;
import de.thm.arsnova.frag.jetzt.backend.model.command.VotePayload;
import de.thm.arsnova.frag.jetzt.backend.service.VoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class VoteCommandHandler {
    private static final Logger logger = LoggerFactory.getLogger(VoteCommandHandler.class);

    private final VoteService service;
    private final CommentEventSource eventer;

    @Autowired
    public VoteCommandHandler(
            VoteService service,
            CommentEventSource eventer
    ) {
        this.service = service;
        this.eventer = eventer;
    }

    public Mono<de.thm.arsnova.frag.jetzt.backend.model.Upvote> handle(Upvote vote) {
        logger.trace("got new command: " + vote.toString());

        VotePayload p = vote.getPayload();

        return service.createUpvote(new de.thm.arsnova.frag.jetzt.backend.model.Upvote(p.getUserId(), p.getCommentId()))
                .doOnSuccess(it -> eventer.ScoreChanged(p.getCommentId()));
    }

    public Mono<de.thm.arsnova.frag.jetzt.backend.model.Downvote> handle(Downvote vote) {
        logger.trace("got new command: " + vote.toString());

        VotePayload p = vote.getPayload();

        return service.createDownvote(new de.thm.arsnova.frag.jetzt.backend.model.Downvote(p.getUserId(), p.getCommentId()))
                .doOnSuccess(it -> eventer.ScoreChanged(p.getCommentId()));
    }

    public Mono<Void> handle(ResetVote vote) {
        logger.trace("got new command: " + vote.toString());

        VotePayload p = vote.getPayload();
        return service.resetVote(p.getCommentId(), p.getUserId()).doOnSuccess(e -> eventer.ScoreChanged(p.getCommentId()));
    }
}
