package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.*;
import de.thm.arsnova.frag.jetzt.backend.model.event.CommentChangeEvent;
import de.thm.arsnova.frag.jetzt.backend.model.event.CommentChangeEventPayload;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.BrainstormingSessionRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.CommentRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.util.QuillUtils;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.InternalServerErrorException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@Service
public class CommentService {

  @Value("${app.comment.length}")
  private int maxCommentLength;

  @Value("${app.comment.privileged_length}")
  private int maxCommentPrivilegedLength;

  @Value("${app.comment.taglength}")
  private int maxCommentTagLength;

  @Value("${app.comment.questionernamelength}")
  private int maxCommentQuestionerNameLength;

  private static final Logger logger = LoggerFactory.getLogger(
    CommentService.class
  );

  private final AuthorizationHelper authorizationHelper;
  private final CommentRepository repository;
  private final RoomRepository roomRepository;
  private final BrainstormingSessionRepository sessionRepository;
  private final AccountService accountService;
  private final CommentChangeService commentChangeService;
  private final AmqpTemplate messagingTemplate;

  @Autowired
  public CommentService(
    AuthorizationHelper authorizationHelper,
    CommentRepository repository,
    RoomRepository roomRepository,
    AccountService accountService,
    CommentChangeService commentChangeService,
    AmqpTemplate messagingTemplate,
    BrainstormingSessionRepository sessionRepository
  ) {
    this.authorizationHelper = authorizationHelper;
    this.repository = repository;
    this.roomRepository = roomRepository;
    this.accountService = accountService;
    this.commentChangeService = commentChangeService;
    this.messagingTemplate = messagingTemplate;
    this.sessionRepository = sessionRepository;
  }

  public Mono<Comment> get(UUID id) {
    return repository.findById(id);
  }

  public Flux<Comment> get(List<UUID> ids) {
    List<Mono<Comment>> list = new ArrayList<>();
    ids.forEach(c -> list.add(get(c)));

    return Flux.merge(list);
  }

  public Flux<Comment> getByRoomId(UUID roomId) {
    return repository.findByRoomId(roomId).cast(Comment.class);
  }

  public Mono<Comment> create(Comment c) {
    logger.trace("Creating new comment: " + c.toString());
    c.setId(null);
    return authorizationHelper
      .getCurrentUser()
      .map(authenticatedUser -> {
        c.setCreatorId(authenticatedUser.getAccountId());
        return c;
      })
      .flatMap(this::validateComment)
      .flatMap(repository::save)
      .flatMap(comment ->
        authorizationHelper
          .getCurrentUser()
          .flatMap(user ->
            Mono.zip(
              Mono.just(comment),
              Mono.just(user),
              getUserInformation(comment.getRoomId(), user),
              roomRepository.findById(comment.getRoomId())
            )
          )
      )
      .flatMap(tuple4 ->
        this.emitForCreation(
            tuple4.getT1(),
            tuple4.getT2(),
            tuple4.getT3(),
            tuple4.getT4()
          )
      )
      .flatMap(comment ->
        accountService.refreshLastActive(
          comment.getCreatorId(),
          Mono.just(comment.getId())
        )
      )
      .flatMap(repository::findById);
  }

  public Mono<Void> beginImport() {
    return repository.disableTriggers();
  }

  public Mono<Void> endImport() {
    return repository.enableTriggers();
  }

  public Mono<Comment> importComment(Room r, Comment c) {
    logger.trace("Importing comment: " + c.toString());
    c.setId(null);
    return this.validateComment(c)
      .flatMap(repository::save)
      .flatMap(emit -> {
        this.sendAmqpMessage(
            new CommentChange(
              emit.getId(),
              emit.getRoomId(),
              CommentChange.CommentChangeType.CREATED,
              null,
              null,
              emit.getCreatorId(),
              CommentChange.UserRole.PARTICIPANT
            ),
            emit,
            r
          );
        return repository.findById(emit.getId());
      });
  }

  public Mono<Comment> patch(
    final Comment entity,
    final Map<String, Object> changes
  ) {
    if (entity.getId() == null) return Mono.error(
      new BadRequestException("The commentId must be provided")
    );
    return Mono
      .just(parseCommentChanges(entity, changes))
      .flatMap(this::validateComment)
      .flatMap(comment ->
        authorizationHelper
          .getCurrentUser()
          .flatMap(authenticatedUser ->
            accountService.refreshLastActive(
              authenticatedUser.getAccountId(),
              Mono.just(authenticatedUser)
            )
          )
          .flatMap(authenticatedUser ->
            validateAccessRightsWithCreator(authenticatedUser, comment, true)
          )
          .map(authenticatedUser -> comment)
      )
      .onErrorResume(throwable ->
        throwable instanceof ClassCastException
          ? Mono.error(new BadRequestException("Invalid Value of Change"))
          : Mono.error(throwable)
      )
      .flatMap(comment ->
        Mono.zip(repository.findById(comment.getId()), Mono.just(comment))
      )
      .flatMap(tuple2 ->
        Mono.zip(
          Mono.just(tuple2.getT1()),
          repository
            .save(tuple2.getT2())
            .flatMap(c -> repository.findById(c.getId()))
        )
      )
      .flatMap(tuple2 ->
        authorizationHelper
          .getCurrentUser()
          .flatMap(user ->
            Mono.zip(
              Mono.just(tuple2.getT1()),
              Mono.just(tuple2.getT2()),
              Mono.just(user),
              getUserInformation(tuple2.getT2().getRoomId(), user),
              roomRepository.findById(tuple2.getT2().getRoomId())
            )
          )
      )
      .flatMap(tuple5 ->
        this.emitPatchEvents(
            tuple5.getT1(),
            tuple5.getT2(),
            tuple5.getT3(),
            tuple5.getT4(),
            tuple5.getT5()
          )
      );
  }

  /**
   * @deprecated Use {@link #patch(Comment, Map)} instead
   */
  public Flux<Comment> patch(
    final Iterable<Comment> entities,
    final Map<String, Object> changes
  ) {
    return Flux
      .fromIterable(entities)
      .filter(comment -> comment.getId() != null)
      .switchIfEmpty(
        Mono.error(new BadRequestException("The commentId must be provided"))
      )
      .map(comment -> parseCommentChanges(comment, changes))
      .onErrorResume(throwable ->
        throwable instanceof ClassCastException
          ? Mono.error(new BadRequestException("Invalid Value of Change"))
          : Mono.error(new InternalServerErrorException())
      )
      .flatMap(comment ->
        validateComment(comment)
          .flatMap(c -> authorizationHelper.getCurrentUser())
          .flatMap(authenticatedUser ->
            validateAccessRightsWithCreator(authenticatedUser, comment, true)
          )
          .map(authenticatedUser -> comment)
      )
      .collectList()
      .flatMapMany(repository::saveAll);
  }

  /**
   * @deprecated Use {@link #patch(Comment, Map)} instead
   */
  public Mono<Comment> update(final Comment c) {
    if (c.getId() == null) return Mono.error(
      new BadRequestException("The commentId must be provided")
    );
    return validateComment(c)
      .flatMap(comment ->
        authorizationHelper
          .getCurrentUser()
          .flatMap(authenticatedUser ->
            validateAccessRightsWithCreator(authenticatedUser, comment, true)
          )
          .flatMap(authenticatedUser -> repository.findById(c.getId()))
          .filter(old ->
            old.getCreatorId() == comment.getCreatorId() &&
            old.getRoomId() != comment.getRoomId()
          )
          .switchIfEmpty(
            Mono.error(
              new ForbiddenException(
                "You are not allowed to change these values"
              )
            )
          )
          .map(authenticatedUser -> comment)
      )
      .flatMap(repository::save);
  }

  public Mono<Void> delete(UUID id) {
    return Mono
      .zip(get(id), authorizationHelper.getCurrentUser())
      .flatMap(tuple2 ->
        validateAccessRightsWithCreator(tuple2.getT2(), tuple2.getT1(), false)
          .flatMap(user ->
            Mono.zip(Mono.just(tuple2.getT1()), Mono.just(tuple2.getT2()))
          )
      )
      .flatMap(tuple2 -> {
        tuple2.getT1().markAsDeleted();
        return repository.save(tuple2.getT1());
      })
      .flatMap(comment ->
        authorizationHelper
          .getCurrentUser()
          .flatMap(user ->
            Mono.zip(
              Mono.just(comment),
              Mono.just(user),
              getUserInformation(comment.getRoomId(), user),
              roomRepository.findById(comment.getRoomId())
            )
          )
      )
      .flatMap(tuple4 -> {
        this.sendAmqpMessage(
            new CommentChange(
              tuple4.getT1().getId(),
              tuple4.getT1().getRoomId(),
              CommentChange.CommentChangeType.DELETED,
              null,
              null,
              tuple4.getT2().getAccountId(),
              tuple4.getT3()
            ),
            tuple4.getT1(),
            tuple4.getT4()
          );
        return Mono.empty();
      });
  }

  public Mono<List<Comment>> deleteAllByRoomId(UUID roomId) {
    // No Comment change possible, since references are deleted
    return Mono
      .zip(
        authorizationHelper.getCurrentUser(),
        roomRepository.findById(roomId)
      )
      .filter(tuple ->
        authorizationHelper.checkModeratorOrCreatorOfRoom(
          tuple.getT1(),
          tuple.getT2()
        )
      )
      .switchIfEmpty(Mono.error(ForbiddenException::new))
      .flatMapMany(authenticatedUser -> getByRoomId(roomId))
      .filter(c -> c.getDeletedAt() == null)
      .collectList()
      .flatMap(comments ->
        repository.deleteAllByRoomId(roomId).count().map(l -> comments)
      );
  }

  private Comment parseCommentChanges(
    Comment comment,
    Map<String, Object> changes
  ) throws ClassCastException {
    changes.forEach((key, value) -> {
      switch (key) {
        case "body":
          comment.setBody((String) value);
          break;
        case "read":
          comment.setRead((boolean) value);
          break;
        case "favorite":
          comment.setFavorite((boolean) value);
          break;
        case "bookmark":
          comment.setBookmark((boolean) value);
          break;
        case "correct":
          comment.setCorrect((int) value);
          break;
        case "ack":
          comment.setAck((boolean) value);
          break;
        case "tag":
          comment.setTag((String) value);
          break;
        case "keywordsFromSpacy":
          comment.setKeywordsFromSpacy((String) value);
          break;
        case "keywordsFromQuestioner":
          comment.setKeywordsFromQuestioner((String) value);
          break;
        case "language":
          comment.setLanguage(Comment.Language.valueOf((String) value));
          break;
        case "brainstormingWordId":
          comment.setBrainstormingWordId(UUID.fromString((String) value));
          break;
        case "approved":
          comment.setApproved((Boolean) value);
          break;
        case "roomId":
        case "creatorId":
        case "questionerName":
        case "commentReference":
        case "gptWriterState":
          throw new ForbiddenException(
            "You are not allowed to change these values"
          );
        default:
          throw new BadRequestException("Invalid ChangeAttribute provided");
      }
    });
    return comment;
  }

  private Mono<Comment> validateComment(Comment comment) {
    return Mono
      .zip(
        roomRepository.findById(comment.getRoomId()),
        authorizationHelper.getCurrentUser()
      )
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .flatMap(tuple2 ->
        Mono.zip(
          Mono.just(comment),
          Mono.just(tuple2.getT1()),
          Mono.just(
            authorizationHelper.checkModeratorOrCreatorOfRoom(
              tuple2.getT2(),
              tuple2.getT1()
            )
          ),
          comment.getCommentReference() != null
            ? get(comment.getCommentReference())
            : Mono.just(new Comment())
        )
      )
      .filter(tuple4 -> tuple4.getT3() || !tuple4.getT2().getQuestionsBlocked())
      .switchIfEmpty(
        Mono.error(new BadRequestException("Questions are blocked"))
      )
      .filter(tuple4 ->
        tuple4.getT3() ||
        tuple4.getT4().isNew() ||
        tuple4.getT4().getCommentDepth() +
        1 <=
        tuple4.getT2().getConversationDepth()
      )
      .switchIfEmpty(
        Mono.error(new BadRequestException("Conversation depth limit reached"))
      )
      .filter(tuple4 -> {
        final int length = tuple4.getT3()
          ? maxCommentPrivilegedLength
          : maxCommentLength;
        return (
          tuple4.getT1().getBody() != null &&
          tuple4.getT1().getBody().length() <= length
        );
      })
      .switchIfEmpty(
        Mono.error(
          new BadRequestException("Maximal Comment length exceeded or empty")
        )
      )
      .map(Tuple2::getT1)
      .filter(c ->
        c.getTag() == null || c.getTag().length() <= maxCommentTagLength
      )
      .switchIfEmpty(
        Mono.error(new BadRequestException("Maximal Tag length exceeded"))
      )
      .filter(c ->
        c.getQuestionerName() == null ||
        c.getQuestionerName().length() <= maxCommentQuestionerNameLength
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException("Maximal Questioner name length exceeded")
        )
      )
      .filter(c -> QuillUtils.isValid(c.getBody(), false))
      .switchIfEmpty(Mono.error(new BadRequestException("Body is not json")))
      .filter(c ->
        (c.getBrainstormingSessionId() == null) ==
        (c.getBrainstormingWordId() == null)
      )
      .switchIfEmpty(
        Mono.error(
          new BadRequestException(
            "Brainstorming word and session are not correctly set"
          )
        )
      );
  }

  private Mono<AuthenticatedUser> validateAccessRightsWithCreator(
    AuthenticatedUser authenticatedUser,
    Comment comment,
    boolean checkChanges
  ) {
    return Mono
      .zip(
        checkChanges ? canAnyoneChange(comment) : Mono.just(false),
        checkChanges ? needsModerator(comment) : Mono.just(false)
      )
      .flatMap(accessArray -> {
        boolean canAnyoneChange = accessArray.getT1();
        boolean needsMod = accessArray.getT2();
        if (canAnyoneChange) {
          return Mono.just(authenticatedUser);
        } else if (
          authenticatedUser.getAccountId().equals(comment.getCreatorId()) &&
          !needsMod
        ) {
          return Mono.just(authenticatedUser);
        } else {
          return roomRepository
            .findById(comment.getRoomId())
            .filter(room ->
              authorizationHelper.checkModeratorOrCreatorOfRoom(
                authenticatedUser,
                room
              )
            )
            .map(room -> authenticatedUser);
        }
      })
      .switchIfEmpty(Mono.error(ForbiddenException::new));
  }

  private Mono<Boolean> canAnyoneChange(Comment changedComment) {
    return repository
      .findById(changedComment.getId())
      .map(comment -> {
        comment.setKeywordsFromSpacy(changedComment.getKeywordsFromSpacy());
        comment.setKeywordsFromQuestioner(
          changedComment.getKeywordsFromQuestioner()
        );
        comment.setLanguage(changedComment.getLanguage());
        return changedComment.equals(comment);
      });
  }

  private Mono<Boolean> needsModerator(Comment changedComment) {
    return repository
      .findById(changedComment.getId())
      .map(comment ->
        changedComment.isFavorite() != comment.isFavorite() ||
        changedComment.isBookmark() != comment.isBookmark() ||
        changedComment.isRead() != comment.isRead() ||
        changedComment.getCorrect() != comment.getCorrect() ||
        changedComment.isAck() != comment.isAck() ||
        !Objects.equals(
          changedComment.getBrainstormingWordId(),
          comment.getBrainstormingWordId()
        )
      );
  }

  private Mono<Comment> emitForCreation(
    Comment comment,
    AuthenticatedUser user,
    CommentChange.UserRole role,
    Room room
  ) {
    return commentChangeService
      .create(
        new CommentChange(
          comment.getId(),
          comment.getRoomId(),
          CommentChange.CommentChangeType.CREATED,
          null,
          null,
          user.getAccountId(),
          role
        )
      )
      .map(commentChange -> {
        this.sendAmqpMessage(commentChange, comment, room);
        return comment;
      })
      .flatMap(c -> {
        if (c.getCommentReference() != null) {
          return Mono
            .zip(
              commentChangeService.create(
                new CommentChange(
                  comment.getCommentReference(),
                  comment.getRoomId(),
                  CommentChange.CommentChangeType.ANSWERED,
                  null,
                  comment.getId().toString(),
                  user.getAccountId(),
                  role
                )
              ),
              get(comment.getCommentReference())
            )
            .map(tuple2 -> {
              this.sendAmqpMessage(tuple2.getT1(), tuple2.getT2(), room);
              return c;
            });
        }
        return Mono.just(c);
      });
  }

  private Mono<Comment> emitPatchEvents(
    Comment previous,
    Comment newComment,
    AuthenticatedUser user,
    CommentChange.UserRole role,
    Room room
  ) {
    return Mono
      .just(newComment)
      .flatMap(c -> {
        if (previous.isAck() != newComment.isAck()) {
          return commentChangeService
            .create(
              new CommentChange(
                newComment.getId(),
                newComment.getRoomId(),
                CommentChange.CommentChangeType.CHANGE_ACK,
                previous.isAck() ? "1" : "0",
                newComment.isAck() ? "1" : "0",
                user.getAccountId(),
                role
              )
            )
            .map(commentChange -> {
              this.sendAmqpMessage(commentChange, c, room);
              return c;
            });
        }
        return Mono.just(c);
      })
      .flatMap(c -> {
        if (previous.isFavorite() != newComment.isFavorite()) {
          return commentChangeService
            .create(
              new CommentChange(
                newComment.getId(),
                newComment.getRoomId(),
                CommentChange.CommentChangeType.CHANGE_FAVORITE,
                previous.isFavorite() ? "1" : "0",
                newComment.isFavorite() ? "1" : "0",
                user.getAccountId(),
                role
              )
            )
            .map(commentChange -> {
              this.sendAmqpMessage(commentChange, c, room);
              return c;
            });
        }
        return Mono.just(c);
      })
      .flatMap(c -> {
        if (previous.getCorrect() != newComment.getCorrect()) {
          return commentChangeService
            .create(
              new CommentChange(
                newComment.getId(),
                newComment.getRoomId(),
                CommentChange.CommentChangeType.CHANGE_CORRECT,
                String.valueOf(previous.getCorrect()),
                String.valueOf(newComment.getCorrect()),
                user.getAccountId(),
                role
              )
            )
            .map(commentChange -> {
              this.sendAmqpMessage(commentChange, c, room);
              return c;
            });
        }
        return Mono.just(c);
      })
      .flatMap(c -> {
        if (!Objects.equals(previous.getTag(), newComment.getTag())) {
          return commentChangeService
            .create(
              new CommentChange(
                newComment.getId(),
                newComment.getRoomId(),
                CommentChange.CommentChangeType.CHANGE_TAG,
                previous.getTag(),
                newComment.getTag(),
                user.getAccountId(),
                role
              )
            )
            .map(commentChange -> {
              this.sendAmqpMessage(commentChange, c, room);
              return c;
            });
        }
        return Mono.just(c);
      });
  }

  public void sendAmqpMessage(
    CommentChange change,
    Comment comment,
    Room room
  ) {
    String additionalId = change.getType() ==
      CommentChange.CommentChangeType.CREATED
      ? ""
      : "." + change.getCommentId();
    CommentChangeEventPayload payload = new CommentChangeEventPayload(
      change,
      comment,
      room
    );
    messagingTemplate.convertAndSend(
      "amq.topic",
      change.getRoomId() + ".comment-change" + additionalId + ".stream",
      new CommentChangeEvent(payload)
    );
  }

  public Mono<CommentChange.UserRole> getUserInformation(
    UUID roomId,
    AuthenticatedUser user
  ) {
    for (RoomAccess access : user.getRoomAccesses()) {
      if (access.getRoomId().equals(roomId)) {
        return Mono.just(CommentChange.UserRole.fromRole(access.getRole()));
      }
    }
    return roomRepository
      .findById(roomId)
      .switchIfEmpty(Mono.error(NotFoundException::new))
      .map(room ->
        authorizationHelper.checkCreatorOfRoom(user, room)
          ? CommentChange.UserRole.CREATOR
          : CommentChange.UserRole.PARTICIPANT
      );
  }
}
