package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Table
public class BrainstormingVote implements Persistable<UUID> {

    @Id
    private UUID id;
    private UUID accountId;
    private UUID wordId;
    private boolean isUpvote;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    public BrainstormingVote() {

    }

    public BrainstormingVote(UUID accountId, UUID wordId, boolean isUpvote) {
        this.accountId = accountId;
        this.wordId = wordId;
        this.isUpvote = isUpvote;
    }

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public UUID getWordId() {
        return wordId;
    }

    public void setWordId(UUID wordId) {
        this.wordId = wordId;
    }

    public boolean isUpvote() {
        return isUpvote;
    }

    public void setUpvote(boolean upvote) {
        isUpvote = upvote;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "BrainstormingVote{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", wordId=" + wordId +
                ", isUpvote=" + isUpvote +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainstormingVote that = (BrainstormingVote) o;
        return isUpvote == that.isUpvote &&
                Objects.equals(id, that.id) &&
                Objects.equals(accountId, that.accountId) &&
                Objects.equals(wordId, that.wordId) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(updatedAt, that.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountId, wordId, isUpvote, createdAt, updatedAt);
    }
}
