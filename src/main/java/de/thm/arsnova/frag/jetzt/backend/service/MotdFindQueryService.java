package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.Motd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MotdFindQueryService {

    private static final Logger logger = LoggerFactory.getLogger(MotdFindQueryService.class);

    private final MotdService motdService;

    @Autowired
    public MotdFindQueryService(final MotdService motdService) {
        this.motdService = motdService;
    }

    public Flux<Motd> resolveQuery(final FindQuery<Motd> findQuery) {
        if (findQuery.getExternalFilters().get("before") instanceof Long) {
            return Mono.just(findQuery)
                    .flatMapMany(query -> motdService.getMotdsBefore((Long) query.getExternalFilters().get("before")));
        }
        if (findQuery.getExternalFilters().get("activeAt") instanceof Long) {
            return Mono.just(findQuery)
                    .flatMapMany(query -> motdService.getActiveMotds((Long) query.getExternalFilters().get("activeAt")));
        }
        return Flux.empty();
    }
}
