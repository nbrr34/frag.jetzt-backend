package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.config.SuperAdminConfig;
import de.thm.arsnova.frag.jetzt.backend.model.Room;
import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import de.thm.arsnova.frag.jetzt.backend.security.AuthenticatedUser;
import de.thm.arsnova.frag.jetzt.backend.security.SecurityContextRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class AuthorizationHelper {

    private final SecurityContextRepository securityContextRepository;
    private final SuperAdminConfig adminConfig;

    @Autowired
    public AuthorizationHelper(SecurityContextRepository securityContextRepository,
            SuperAdminConfig adminConfig) {
        this.securityContextRepository = securityContextRepository;
        this.adminConfig = adminConfig;
    }

    public Mono<AuthenticatedUser> getCurrentUser() {
        return securityContextRepository.getAuthenticatedUser(false)
                .switchIfEmpty(Mono.error(new UnauthorizedException()));
    }

    public boolean isSuperAdmin(AuthenticatedUser authenticatedUser) {
        if (authenticatedUser == null || authenticatedUser.getName() == null) {
            return false;
        }
        return adminConfig.getSuperAdminList().contains(authenticatedUser.getName().toLowerCase());
    }

    public boolean isOnlyCreatorOfRoom(AuthenticatedUser authenticatedUser, Room room) {
        return authenticatedUser.getAccountId().equals(room.getOwnerId());
    }

    public boolean checkCreatorOfRoom(AuthenticatedUser authenticatedUser, Room room) {
        return isSuperAdmin(authenticatedUser) || isOnlyCreatorOfRoom(authenticatedUser, room);
    }

    public Mono<Void> removeAccess(UUID roomId, UUID accountId) {
        return getCurrentUser().filter(user -> user.getAccountId().equals(accountId))
                .flatMap(user -> {
                    user.getRoomAccesses().removeIf(access -> access.getRoomId().equals(roomId));
                    return Mono.empty();
                });
    }

    public Mono<RoomAccess> addOrChangeAccess(RoomAccess roomAccess) {
        return getCurrentUser()
                .filter(user -> user.getAccountId().equals(roomAccess.getAccountId())).map(user -> {
                    final int index = user.getRoomAccesses().indexOf(roomAccess);
                    if (index < 0) {
                        user.getRoomAccesses().add(roomAccess);
                    } else {
                        RoomAccess access = user.getRoomAccesses().get(index);
                        access.setRole(roomAccess.getRole());
                        access.setManagedFields(roomAccess);
                    }
                    return roomAccess;
                }).switchIfEmpty(Mono.just(roomAccess));
    }

    public boolean checkOnlyModeratorOfRoom(AuthenticatedUser authenticatedUser, Room room) {
        for (RoomAccess roomAccess : authenticatedUser.getRoomAccesses()) {
            if (roomAccess.getId() != null
                    && roomAccess.getAccountId().equals(authenticatedUser.getAccountId())
                    && roomAccess.getRoomId().equals(room.getId())
                    && roomAccess.getRole().equals(RoomAccess.Role.EXECUTIVE_MODERATOR)) {
                return true;
            }
        }
        return false;

    }

    public boolean checkModeratorOrCreatorOfRoom(AuthenticatedUser authenticatedUser, Room room) {
        if (checkCreatorOfRoom(authenticatedUser, room)) {
            return true;
        }
        return checkOnlyModeratorOfRoom(authenticatedUser, room);
    }
}
