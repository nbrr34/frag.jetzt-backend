package de.thm.arsnova.frag.jetzt.backend.model.event;

import java.util.UUID;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

public class LivepollSessionDeletedPayload implements WebSocketPayload {

  private UUID livepollId;

  public LivepollSessionDeletedPayload() {}

  public LivepollSessionDeletedPayload(UUID livepollId) {
    this.livepollId = livepollId;
  }

  public UUID getLivepollId() {
    return livepollId;
  }

  public void setLivepollId(UUID livepollId) {
    this.livepollId = livepollId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result =
      prime * result + ((livepollId == null) ? 0 : livepollId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    LivepollSessionDeletedPayload other = (LivepollSessionDeletedPayload) obj;
    if (livepollId == null) {
      if (other.livepollId != null) return false;
    } else if (!livepollId.equals(other.livepollId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "LivepollSessionDeletedPayload [livepollId=" + livepollId + "]";
  }
}
