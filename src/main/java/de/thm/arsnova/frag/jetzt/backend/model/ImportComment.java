package de.thm.arsnova.frag.jetzt.backend.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

public class ImportComment {
    private UUID roomId;
    private boolean favorite;
    private boolean bookmark;
    private int correct;
    private boolean ack;
    private int score;
    private int downvotes;
    private int upvotes;
    private UUID creatorId;
    private String questionerName;
    private String keywordsFromQuestioner;
    private String tag;
    private String body;
    private Timestamp createdAt;
    private String number;

    public ImportComment() {

    }

    public ImportComment(
            UUID roomId,
            boolean favorite,
            boolean bookmark,
            int correct,
            boolean ack,
            int score,
            int downvotes,
            int upvotes,
            UUID creatorId,
            String questionerName,
            String keywordsFromQuestioner,
            String tag,
            String body,
            Timestamp createdAt,
            String number
    ) {
        this.roomId = roomId;
        this.favorite = favorite;
        this.bookmark = bookmark;
        this.correct = correct;
        this.ack = ack;
        this.score = score;
        this.downvotes = downvotes;
        this.upvotes = upvotes;
        this.creatorId = creatorId;
        this.questionerName = questionerName;
        this.keywordsFromQuestioner = keywordsFromQuestioner;
        this.tag = tag;
        this.body = body;
        this.createdAt = createdAt;
        this.number = number;
    }

    public UUID getRoomId() {
        return roomId;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public boolean isBookmark() {
        return bookmark;
    }

    public int getCorrect() {
        return correct;
    }

    public boolean isAck() {
        return ack;
    }

    public int getScore() {
        return score;
    }

    public int getDownvotes() {
        return downvotes;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public UUID getCreatorId() {
        return creatorId;
    }

    public String getQuestionerName() {
        return questionerName;
    }

    public String getKeywordsFromQuestioner() {
        return keywordsFromQuestioner;
    }

    public String getTag() {
        return tag;
    }

    public String getBody() {
        return body;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public String getNumber() {
        return number;
    }

    public static Comment toComment(ImportComment comment, UUID reference) {
        int depth = comment.number.split("/").length - 1;
        if (depth > 0 && reference == null) {
            return null;
        }
        Comment c = new Comment();
        c.id = null;
        c.roomId = comment.roomId;
        c.creatorId = comment.creatorId;
        c.number = comment.number;
        c.body = comment.body;
        c.read = false;
        c.favorite = comment.favorite;
        c.correct = comment.correct;
        c.ack = comment.ack;
        c.bookmark = comment.bookmark;
        c.keywordsFromSpacy = "[]";
        c.keywordsFromQuestioner = comment.keywordsFromQuestioner;
        c.upvotes = comment.upvotes;
        c.downvotes = comment.downvotes;
        c.score = comment.score;
        c.tag = comment.tag;
        c.questionerName = comment.questionerName;
        c.language = Comment.Language.AUTO;
        c.commentReference = reference;
        c.commentDepth = depth;
        c.createdAt = comment.createdAt;
        c.updatedAt = null;
        c.deletedAt = null;
        return c;
    }

    @Override
    public String toString() {
        return "ImportComment{" +
                "roomId=" + roomId +
                ", favorite=" + favorite +
                ", bookmark=" + bookmark +
                ", correct=" + correct +
                ", ack=" + ack +
                ", score=" + score +
                ", downvotes=" + downvotes +
                ", upvotes=" + upvotes +
                ", creatorId=" + creatorId +
                ", questionerName='" + questionerName + '\'' +
                ", keywordsFromQuestioner='" + keywordsFromQuestioner + '\'' +
                ", tag='" + tag + '\'' +
                ", body='" + body + '\'' +
                ", createdAt=" + createdAt +
                ", number='" + number + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImportComment that = (ImportComment) o;
        return favorite == that.favorite && bookmark == that.bookmark && correct == that.correct && ack == that.ack &&
                score == that.score && downvotes == that.downvotes && upvotes == that.upvotes &&
                Objects.equals(roomId, that.roomId) && Objects.equals(creatorId, that.creatorId) &&
                Objects.equals(questionerName, that.questionerName) &&
                Objects.equals(keywordsFromQuestioner, that.keywordsFromQuestioner) &&
                Objects.equals(tag, that.tag) && Objects.equals(body, that.body) &&
                Objects.equals(createdAt, that.createdAt) && Objects.equals(number, that.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roomId, favorite, bookmark, correct, ack, score, downvotes, upvotes, creatorId,
                questionerName, keywordsFromQuestioner, tag, body, createdAt, number
        );
    }
}
