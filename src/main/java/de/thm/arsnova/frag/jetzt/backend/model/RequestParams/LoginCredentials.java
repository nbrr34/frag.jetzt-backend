package de.thm.arsnova.frag.jetzt.backend.model.RequestParams;

import org.springframework.core.style.ToStringCreator;

public class LoginCredentials {
	private String loginId;
	private String password;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(final String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return new ToStringCreator(this)
				.append("loginId", loginId)
				.append("password", password)
				.toString();
	}
}
