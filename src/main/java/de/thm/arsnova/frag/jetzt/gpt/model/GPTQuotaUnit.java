package de.thm.arsnova.frag.jetzt.gpt.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GPTQuotaUnit {

  private final Long value;
  private final int exponent;

  /**
   * Represents the unit US-Dollar ($) with more decimal places<br>
   * The current value in US-Dollar $ is represented with: value * 10^-exponent
   */
  @JsonCreator
  public GPTQuotaUnit(
    @JsonProperty("value") Number value,
    @JsonProperty("exponent") int exponent
  ) {
    this.value = value != null ? value.longValue() : null;
    this.exponent = exponent;
  }

  public Long getValue() {
    return value;
  }

  public GPTQuotaUnit add(long l) {
    return new GPTQuotaUnit(value != null ? value + l : l, exponent);
  }

  public GPTQuotaUnit add(GPTQuotaUnit u) {
    return add(u.toPlain(exponent));
  }

  public int getExponent() {
    return exponent;
  }

  public boolean isLowerThan(GPTQuotaUnit unit) {
    if (unit.exponent > exponent) {
      return toPlain(unit.exponent) < unit.value;
    }
    return value < unit.toPlain(exponent);
  }

  public GPTQuotaUnit minus(GPTQuotaUnit unit) {
    if (unit.exponent > exponent) {
      return new GPTQuotaUnit(
        toPlain(unit.exponent) - unit.value,
        unit.exponent
      );
    }
    return new GPTQuotaUnit(value - unit.toPlain(exponent), exponent);
  }

  public long divide(GPTQuotaUnit unit) {
    return (long) (
      value *
      Math.pow(10, unit.exponent) /
      (unit.value * Math.pow(10, exponent))
    );
  }

  public GPTQuotaUnit multiply(Number n) {
    return new GPTQuotaUnit(n.longValue() * value, exponent);
  }

  public GPTQuotaUnit multiply(GPTQuotaUnit u) {
    return new GPTQuotaUnit(u.value * value, u.exponent + exponent);
  }

  public Long toPlain(int exponent) {
    if (value == null) {
      return null;
    }
    return (long) (value * Math.pow(10, exponent - this.exponent));
  }

  public GPTQuotaUnit zero() {
    return new GPTQuotaUnit(0, exponent);
  }
}
