package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Rating;
import de.thm.arsnova.frag.jetzt.backend.model.RatingResult;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface RatingRepository extends ReactiveCrudRepository<Rating, UUID> {

    @Query("SELECT AVG(rating) AS rating, " +
            "COUNT(account_id) AS people, " +
            "ROUND(COUNT(CASE WHEN rating <= 1 THEN 1 END) * 100 / COALESCE(NULLIF(COUNT(account_id), 0), 1)::float) AS one_star_percent, " +
            "ROUND(COUNT(CASE WHEN rating <= 2 AND rating > 1 THEN 1 END) * 100 / COALESCE(NULLIF(COUNT(account_id), 0), 1)::float) AS two_star_percent, " +
            "ROUND(COUNT(CASE WHEN rating <= 3 AND rating > 2 THEN 1 END) * 100 / COALESCE(NULLIF(COUNT(account_id), 0), 1)::float) AS three_star_percent, " +
            "ROUND(COUNT(CASE WHEN rating <= 4 AND rating > 3 THEN 1 END) * 100 / COALESCE(NULLIF(COUNT(account_id), 0), 1)::float) AS four_star_percent, " +
            "ROUND(COUNT(CASE WHEN rating <= 5 AND rating > 4 THEN 1 END) * 100 / COALESCE(NULLIF(COUNT(account_id), 0), 1)::float) AS five_star_percent " +
            "FROM rating")
    Mono<RatingResult> getRatings();

    Mono<Rating> findByAccountId(UUID accountId);

    Mono<Void> deleteByAccountId(UUID accountId);
}
