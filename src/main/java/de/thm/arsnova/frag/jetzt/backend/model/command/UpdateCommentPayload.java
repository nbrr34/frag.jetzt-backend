package de.thm.arsnova.frag.jetzt.backend.model.command;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thm.arsnova.frag.jetzt.backend.model.Comment;
import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class UpdateCommentPayload implements WebSocketPayload {
    private UUID id;
    private String body;
    private boolean read;
    private boolean favorite;
    private boolean bookmark;
    private int correct;
    private String tag;
    private String keywordsFromSpacy;
    private String keywordsFromQuestioner;
    private Comment.Language language;

    public UpdateCommentPayload() {
    }

    public UpdateCommentPayload(Comment c) {
        this.id = c.getId();
        this.body = c.getBody();
        this.read = c.isRead();
        this.favorite = c.isFavorite();
        this.bookmark = c.isBookmark();
        this.correct = c.getCorrect();
        this.keywordsFromSpacy = c.getKeywordsFromSpacy();
        this.keywordsFromQuestioner = c.getKeywordsFromQuestioner();
        this.language = c.getLanguage();
    }

    @JsonProperty("roomId")
    public UUID getId() {
        return id;
    }

    @JsonProperty("roomId")
    public void setId(UUID id) {
        this.id = id;
    }

    @JsonProperty("body")
    public String getBody() {
        return body;
    }

    @JsonProperty("body")
    public void setBody(String body) {
        this.body = body;
    }

    @JsonProperty("read")
    public boolean isRead() {
        return read;
    }

    @JsonProperty("read")
    public void setRead(boolean read) {
        this.read = read;
    }

    @JsonProperty("favorite")
    public boolean isFavorite() {
        return favorite;
    }

    @JsonProperty("favorite")
    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @JsonProperty("bookmark")
    public boolean isBookmark() {
        return bookmark;
    }

    @JsonProperty("bookmark")
    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }

    @JsonProperty("correct")
    public int getCorrect() {
        return correct;
    }

    @JsonProperty("correct")
    public void setCorrect(int correct) {
        this.correct = correct;
    }

    @JsonProperty("tag")
    public String getTag() {
        return tag;
    }

    @JsonProperty("tag")
    public void setTag(String tag) {
        this.tag = tag;
    }

    @JsonProperty("keywordsFromSpacy")
    public String getKeywordsFromSpacy() {
        return keywordsFromSpacy;
    }

    @JsonProperty("keywordsFromSpacy")
    public void setKeywordsFromSpacy(String keywordsFromSpacy) {
        this.keywordsFromSpacy = keywordsFromSpacy;
    }

    @JsonProperty("keywordsFromQuestioner")
    public String getKeywordsFromQuestioner() {
        return keywordsFromQuestioner;
    }

    @JsonProperty("keywordsFromQuestioner")
    public void setKeywordsFromQuestioner(String keywordsFromQuestioner) {
        this.keywordsFromQuestioner = keywordsFromQuestioner;
    }

    @JsonProperty("language")
    public Comment.Language getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(Comment.Language language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "UpdateCommentPayload{" +
                "id=" + id +
                ", body='" + body + '\'' +
                ", read=" + read +
                ", favorite=" + favorite +
                ", bookmark=" + bookmark +
                ", correct=" + correct +
                ", tag='" + tag + '\'' +
                ", keywordsFromSpacy='" + keywordsFromSpacy + '\'' +
                ", keywordsFromQuestioner='" + keywordsFromQuestioner + '\'' +
                ", language=" + language +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateCommentPayload that = (UpdateCommentPayload) o;
        return read == that.read &&
                favorite == that.favorite &&
                bookmark == that.bookmark &&
                correct == that.correct &&
                Objects.equals(id, that.id) &&
                Objects.equals(body, that.body) &&
                Objects.equals(tag, that.tag) &&
                Objects.equals(keywordsFromSpacy, that.keywordsFromSpacy) &&
                Objects.equals(keywordsFromQuestioner, that.keywordsFromQuestioner) &&
                language == that.language;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, body, read, favorite, bookmark, correct, tag, keywordsFromSpacy, keywordsFromQuestioner, language);
    }
}
