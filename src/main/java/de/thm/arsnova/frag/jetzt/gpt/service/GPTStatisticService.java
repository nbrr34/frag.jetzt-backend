package de.thm.arsnova.frag.jetzt.gpt.service;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTCounters;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTStatistics;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRequestStatistic;
import de.thm.arsnova.frag.jetzt.gpt.model.requestStatistic.GPTRequestStatisticResult;
import de.thm.arsnova.frag.jetzt.gpt.service.persistence.GPTRequestStatisticRepository;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class GPTStatisticService {

  private final GPTCounters counters;
  private final GPTRequestStatisticRepository requestStatisticRepository;
  private final AuthorizationHelper helper;

  @Autowired
  public GPTStatisticService(
    GPTCounters counters,
    GPTRequestStatisticRepository requestStatisticRepository,
    AuthorizationHelper helper
  ) {
    this.counters = counters;
    this.requestStatisticRepository = requestStatisticRepository;
    this.helper = helper;
  }

  public Mono<GPTRequestStatistic> save(GPTRequestStatistic statistic) {
    return requestStatisticRepository.save(statistic);
  }

  public Mono<GPTRequestStatisticResult> getResultByRoomId(UUID roomId) {
    return helper
      .getCurrentUser()
      .filter(user -> helper.isSuperAdmin(user))
      .switchIfEmpty(
        Mono.error(
          new ForbiddenException("Results may only be for administrators!")
        )
      )
      .flatMap(ignore ->
        requestStatisticRepository
          .getAllByRoomIdOrderByCreatedAtAsc(roomId)
          .reduce(
            new GPTRequestStatisticResult(),
            (acc, entry) -> acc.accept(entry)
          )
      );
  }

  public Mono<GPTStatistics> getStats() {
    return Mono.just(
      new GPTStatistics(
        counters.getLastUpdate(),
        counters.getDailyCounter(),
        counters.getWeeklyCounter(),
        counters.getMonthlyCounter(),
        counters.getAccumulatedCounter()
      )
    );
  }
}
