package de.thm.arsnova.frag.jetzt.gpt.model.rest;

import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonIgnore;

public interface IGPTRequest {
  @JsonIgnore
  UUID getRoomId();

  @JsonIgnore
  String getApiKey();

  @JsonIgnore
  String getApiOrganization();
}
