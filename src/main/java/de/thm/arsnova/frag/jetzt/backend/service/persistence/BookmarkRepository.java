package de.thm.arsnova.frag.jetzt.backend.service.persistence;

import de.thm.arsnova.frag.jetzt.backend.model.Bookmark;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface BookmarkRepository extends ReactiveCrudRepository<Bookmark, UUID> {

    Flux<Bookmark> findByAccountIdAndRoomId(UUID accountId, UUID roomId);

    Mono<Void> deleteByAccountIdAndCommentId(UUID accountId, UUID commentId);

}
