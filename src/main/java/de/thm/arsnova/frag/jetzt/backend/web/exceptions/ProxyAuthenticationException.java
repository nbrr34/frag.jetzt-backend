package de.thm.arsnova.frag.jetzt.backend.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Proxy authentication means status code 407.
 */
@ResponseStatus(code = HttpStatus.PROXY_AUTHENTICATION_REQUIRED)
public class ProxyAuthenticationException extends RuntimeException {
    private static final long serialVersionUID = 1937L;

    public ProxyAuthenticationException() {
        super();
    }

    public ProxyAuthenticationException(final String message) {
        super(message);
    }

    public ProxyAuthenticationException(final Throwable cause) {
        super(cause);
    }

    public ProxyAuthenticationException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
