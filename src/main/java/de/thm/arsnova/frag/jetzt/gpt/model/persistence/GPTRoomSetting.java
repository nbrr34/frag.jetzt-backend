package de.thm.arsnova.frag.jetzt.gpt.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.thm.arsnova.frag.jetzt.gpt.config.GPTConfig.GPTActivationCode;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTMultiQuotaLimiter;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTMultiQuotaLimiter.MultiQuotaEntry;
import de.thm.arsnova.frag.jetzt.gpt.model.GPTQuotaUnit;
import de.thm.arsnova.frag.jetzt.gpt.util.GPTQuotaType;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.lang.Nullable;

@Table
public class GPTRoomSetting implements Persistable<UUID> {

  public enum GPTRoomSettingRight {
    CHANGE_PARTICIPANT_QUOTA(0, true),
    CHANGE_MODERATOR_QUOTA(1, true),
    CHANGE_ROOM_QUOTA(2, true),
    CHANGE_ROOM_PRESETS(3, true),
    CHANGE_USAGE_TIMES(4, true),
    CHANGE_API_KEY(5, true),
    ALLOW_UNREGISTERED_USERS(6, true),
    DISABLE_ENHANCED_PROMPT(7, false),
    DISABLE_FORWARD_MESSAGE(8, false);

    private final int bitValue;
    private final int defaultValue;

    GPTRoomSettingRight(int bit, boolean defaultEnabled) {
      if (bit < 0 || bit > 31) {
        throw new IllegalArgumentException("bit must be between [0,31]!");
      }
      bitValue = 1 << bit;
      defaultValue = defaultEnabled ? bitValue : 0;
    }

    public boolean isRightSet(int bitset) {
      return (bitset & bitValue) != 0;
    }
  }

  @Id
  private UUID id;

  private UUID roomId;
  private String apiKey;
  private String apiOrganization;
  private boolean trialEnabled;
  /** Max. Quota is modelled in 10^-2 US-$ (Dollar Cent) */
  private Integer maxDailyRoomCost;
  private Integer maxMonthlyRoomCost;
  private Integer maxMonthlyFlowingRoomCost;
  private Long maxAccumulatedRoomCost;
  private Integer maxDailyParticipantCost;
  private Integer maxMonthlyParticipantCost;
  private Integer maxMonthlyFlowingParticipantCost;
  private Long maxAccumulatedParticipantCost;
  private Integer maxDailyModeratorCost;
  private Integer maxMonthlyModeratorCost;
  private Integer maxMonthlyFlowingModeratorCost;
  private Long maxAccumulatedModeratorCost;
  // stats
  private long dailyRoomCostCounter;
  private long monthlyRoomCostCounter;
  private long accumulatedRoomCostCounter;
  private long dailyParticipantCostCounter;
  private long monthlyParticipantCostCounter;
  private long accumulatedParticipantCostCounter;
  private long dailyModeratorCostCounter;
  private long monthlyModeratorCostCounter;
  private long accumulatedModeratorCostCounter;
  private Timestamp lastUpdate;
  // rights
  private int rightsBitset;
  // payment
  private long paymentCounter;
  // meta information
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;
  // presets
  private String presetContext;
  private String presetLength;
  private String roleInstruction;

  // additional
  @Transient
  private List<GPTRoomUsageTime> usageTimes;

  @Transient
  private GPTActivationCode trialCode;

  @Transient
  private int globalAccumulatedQuota;

  public GPTRoomSetting() {}

  public GPTRoomSetting(UUID roomId) {
    this.roomId = roomId;
    int i = 0;
    for (GPTRoomSettingRight r : GPTRoomSettingRight.values()) {
      i |= r.defaultValue;
    }
    this.rightsBitset = i;
  }

  @JsonIgnore
  public GPTMultiQuotaLimiter makeLimiter(
    boolean isCreator,
    boolean isModerator
  ) {
    final LocalDate d = LocalDate.now();
    final int day = d.getDayOfMonth();
    final int days = d.getMonth().length(d.isLeapYear());
    ArrayList<MultiQuotaEntry> list = new ArrayList<>();
    list.add(
      new MultiQuotaEntry(
        GPTQuotaType.DAILY,
        new GPTQuotaUnit(maxDailyRoomCost, 2),
        new GPTQuotaUnit(dailyRoomCostCounter, 8)
      )
    );
    list.add(
      new MultiQuotaEntry(
        GPTQuotaType.MONTHLY,
        new GPTQuotaUnit(maxMonthlyRoomCost, 2),
        new GPTQuotaUnit(monthlyRoomCostCounter, 8)
      )
    );
    list.add(
      new MultiQuotaEntry(
        GPTQuotaType.GLOBAL,
        new GPTQuotaUnit(maxAccumulatedRoomCost, 2),
        new GPTQuotaUnit(accumulatedRoomCostCounter, 8)
      )
    );
    list.add(
      new MultiQuotaEntry(
        GPTQuotaType.MONTHLY,
        new GPTQuotaUnit(
          maxMonthlyFlowingRoomCost != null
            ? maxMonthlyFlowingRoomCost * day / days
            : null,
          2
        ),
        new GPTQuotaUnit(monthlyRoomCostCounter, 8)
      )
    );
    if (isCreator) {
      return new GPTMultiQuotaLimiter(lastUpdate.toLocalDateTime(), list);
    }
    if (isModerator) {
      list.add(
        new MultiQuotaEntry(
          GPTQuotaType.DAILY,
          new GPTQuotaUnit(maxDailyModeratorCost, 2),
          new GPTQuotaUnit(dailyModeratorCostCounter, 8)
        )
      );
      list.add(
        new MultiQuotaEntry(
          GPTQuotaType.MONTHLY,
          new GPTQuotaUnit(maxMonthlyModeratorCost, 2),
          new GPTQuotaUnit(monthlyModeratorCostCounter, 8)
        )
      );
      list.add(
        new MultiQuotaEntry(
          GPTQuotaType.GLOBAL,
          new GPTQuotaUnit(maxAccumulatedModeratorCost, 2),
          new GPTQuotaUnit(accumulatedModeratorCostCounter, 8)
        )
      );
      list.add(
        new MultiQuotaEntry(
          GPTQuotaType.MONTHLY,
          new GPTQuotaUnit(
            maxMonthlyFlowingModeratorCost != null
              ? maxMonthlyFlowingModeratorCost * day / days
              : null,
            2
          ),
          new GPTQuotaUnit(monthlyModeratorCostCounter, 8)
        )
      );
      return new GPTMultiQuotaLimiter(lastUpdate.toLocalDateTime(), list);
    }
    list.add(
      new MultiQuotaEntry(
        GPTQuotaType.DAILY,
        new GPTQuotaUnit(maxDailyParticipantCost, 2),
        new GPTQuotaUnit(dailyParticipantCostCounter, 8)
      )
    );
    list.add(
      new MultiQuotaEntry(
        GPTQuotaType.MONTHLY,
        new GPTQuotaUnit(maxMonthlyParticipantCost, 2),
        new GPTQuotaUnit(monthlyParticipantCostCounter, 8)
      )
    );
    list.add(
      new MultiQuotaEntry(
        GPTQuotaType.GLOBAL,
        new GPTQuotaUnit(maxAccumulatedParticipantCost, 2),
        new GPTQuotaUnit(accumulatedParticipantCostCounter, 8)
      )
    );
    list.add(
      new MultiQuotaEntry(
        GPTQuotaType.MONTHLY,
        new GPTQuotaUnit(
          maxMonthlyFlowingParticipantCost != null
            ? maxMonthlyFlowingParticipantCost * day / days
            : null,
          2
        ),
        new GPTQuotaUnit(monthlyParticipantCostCounter, 8)
      )
    );
    return new GPTMultiQuotaLimiter(lastUpdate.toLocalDateTime(), list);
  }

  @JsonIgnore
  public void reapplyLimiter(
    GPTMultiQuotaLimiter limiter,
    boolean isCreator,
    boolean isModerator
  ) {
    lastUpdate = Timestamp.valueOf(limiter.getLastUpdate());
    List<MultiQuotaEntry> entries = limiter.getLimits();
    dailyRoomCostCounter = entries.get(0).getCounter().toPlain(8);
    monthlyRoomCostCounter = entries.get(1).getCounter().toPlain(8);
    accumulatedRoomCostCounter = entries.get(2).getCounter().toPlain(8);
    if (isCreator) {
      return;
    }
    if (isModerator) {
      dailyModeratorCostCounter = entries.get(4).getCounter().toPlain(8);
      monthlyModeratorCostCounter = entries.get(5).getCounter().toPlain(8);
      accumulatedModeratorCostCounter = entries.get(6).getCounter().toPlain(8);
      return;
    }
    dailyParticipantCostCounter = entries.get(4).getCounter().toPlain(8);
    monthlyParticipantCostCounter = entries.get(5).getCounter().toPlain(8);
    accumulatedParticipantCostCounter = entries.get(6).getCounter().toPlain(8);
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  @Nullable
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public String getApiKey() {
    return apiKey;
  }

  @JsonIgnore
  public String getMaskedApiKey() {
    return apiKey != null
      ? "***************************************************"
      : null;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public String getApiOrganization() {
    return apiOrganization;
  }

  public void setApiOrganization(String apiOrganization) {
    this.apiOrganization = apiOrganization;
  }

  public boolean isTrialEnabled() {
    return trialEnabled;
  }

  public void setTrialEnabled(boolean trialEnabled) {
    this.trialEnabled = trialEnabled;
  }

  public Integer getMaxDailyRoomCost() {
    return maxDailyRoomCost;
  }

  public void setMaxDailyRoomCost(Integer maxDailyRoomCost) {
    this.maxDailyRoomCost = maxDailyRoomCost;
  }

  public Integer getMaxMonthlyRoomCost() {
    return maxMonthlyRoomCost;
  }

  public void setMaxMonthlyRoomCost(Integer maxMonthlyRoomCost) {
    this.maxMonthlyRoomCost = maxMonthlyRoomCost;
  }

  public Long getMaxAccumulatedRoomCost() {
    return maxAccumulatedRoomCost;
  }

  public void setMaxAccumulatedRoomCost(Long maxAccumulatedRoomCost) {
    this.maxAccumulatedRoomCost = maxAccumulatedRoomCost;
  }

  public Integer getMaxDailyParticipantCost() {
    return maxDailyParticipantCost;
  }

  public void setMaxDailyParticipantCost(Integer maxDailyParticipantCost) {
    this.maxDailyParticipantCost = maxDailyParticipantCost;
  }

  public Integer getMaxMonthlyParticipantCost() {
    return maxMonthlyParticipantCost;
  }

  public void setMaxMonthlyParticipantCost(Integer maxMonthlyParticipantCost) {
    this.maxMonthlyParticipantCost = maxMonthlyParticipantCost;
  }

  public Long getMaxAccumulatedParticipantCost() {
    return maxAccumulatedParticipantCost;
  }

  public void setMaxAccumulatedParticipantCost(
    Long maxAccumulatedParticipantCost
  ) {
    this.maxAccumulatedParticipantCost = maxAccumulatedParticipantCost;
  }

  public Integer getMaxDailyModeratorCost() {
    return maxDailyModeratorCost;
  }

  public void setMaxDailyModeratorCost(Integer maxDailyModeratorCost) {
    this.maxDailyModeratorCost = maxDailyModeratorCost;
  }

  public Integer getMaxMonthlyModeratorCost() {
    return maxMonthlyModeratorCost;
  }

  public void setMaxMonthlyModeratorCost(Integer maxMonthlyModeratorCost) {
    this.maxMonthlyModeratorCost = maxMonthlyModeratorCost;
  }

  public Long getMaxAccumulatedModeratorCost() {
    return maxAccumulatedModeratorCost;
  }

  public void setMaxAccumulatedModeratorCost(Long maxAccumulatedModeratorCost) {
    this.maxAccumulatedModeratorCost = maxAccumulatedModeratorCost;
  }

  public Timestamp getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(Timestamp lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public int getRightsBitset() {
    return rightsBitset;
  }

  public void setRightsBitset(int rightsBitset) {
    this.rightsBitset = rightsBitset;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public List<GPTRoomUsageTime> getUsageTimes() {
    return usageTimes;
  }

  public void setUsageTimes(List<GPTRoomUsageTime> usageTimes) {
    this.usageTimes = usageTimes;
  }

  public long getPaymentCounter() {
    return paymentCounter;
  }

  public void setPaymentCounter(long paymentCounter) {
    this.paymentCounter = paymentCounter;
  }

  public String getPresetContext() {
    return presetContext;
  }

  public void setPresetContext(String presetContext) {
    this.presetContext = presetContext;
  }

  public String getPresetLength() {
    return presetLength;
  }

  public void setPresetLength(String presetLength) {
    this.presetLength = presetLength;
  }

  public long getDailyRoomCostCounter() {
    return dailyRoomCostCounter;
  }

  public void setDailyRoomCostCounter(long dailyRoomCostCounter) {
    this.dailyRoomCostCounter = dailyRoomCostCounter;
  }

  public long getMonthlyRoomCostCounter() {
    return monthlyRoomCostCounter;
  }

  public void setMonthlyRoomCostCounter(long monthlyRoomCostCounter) {
    this.monthlyRoomCostCounter = monthlyRoomCostCounter;
  }

  public long getAccumulatedRoomCostCounter() {
    return accumulatedRoomCostCounter;
  }

  public void setAccumulatedRoomCostCounter(long accumulatedRoomCostCounter) {
    this.accumulatedRoomCostCounter = accumulatedRoomCostCounter;
  }

  public long getDailyParticipantCostCounter() {
    return dailyParticipantCostCounter;
  }

  public void setDailyParticipantCostCounter(long dailyParticipantCostCounter) {
    this.dailyParticipantCostCounter = dailyParticipantCostCounter;
  }

  public long getMonthlyParticipantCostCounter() {
    return monthlyParticipantCostCounter;
  }

  public void setMonthlyParticipantCostCounter(
    long monthlyParticipantCostCounter
  ) {
    this.monthlyParticipantCostCounter = monthlyParticipantCostCounter;
  }

  public long getAccumulatedParticipantCostCounter() {
    return accumulatedParticipantCostCounter;
  }

  public void setAccumulatedParticipantCostCounter(
    long accumulatedParticipantCostCounter
  ) {
    this.accumulatedParticipantCostCounter = accumulatedParticipantCostCounter;
  }

  public long getDailyModeratorCostCounter() {
    return dailyModeratorCostCounter;
  }

  public void setDailyModeratorCostCounter(long dailyModeratorCostCounter) {
    this.dailyModeratorCostCounter = dailyModeratorCostCounter;
  }

  public long getMonthlyModeratorCostCounter() {
    return monthlyModeratorCostCounter;
  }

  public void setMonthlyModeratorCostCounter(long monthlyModeratorCostCounter) {
    this.monthlyModeratorCostCounter = monthlyModeratorCostCounter;
  }

  public long getAccumulatedModeratorCostCounter() {
    return accumulatedModeratorCostCounter;
  }

  public void setAccumulatedModeratorCostCounter(
    long accumulatedModeratorCostCounter
  ) {
    this.accumulatedModeratorCostCounter = accumulatedModeratorCostCounter;
  }

  public GPTActivationCode getTrialCode() {
    return trialCode;
  }

  public void setTrialCode(GPTActivationCode trialCode) {
    this.trialCode = trialCode;
  }

  public int getGlobalAccumulatedQuota() {
    return this.globalAccumulatedQuota;
  }

  public void setGlobalAccumulatedQuota(int quota) {
    this.globalAccumulatedQuota = quota;
  }

  public Integer getMaxMonthlyFlowingRoomCost() {
    return maxMonthlyFlowingRoomCost;
  }

  public void setMaxMonthlyFlowingRoomCost(Integer maxMonthlyFlowingRoomCost) {
    this.maxMonthlyFlowingRoomCost = maxMonthlyFlowingRoomCost;
  }

  public Integer getMaxMonthlyFlowingParticipantCost() {
    return maxMonthlyFlowingParticipantCost;
  }

  public void setMaxMonthlyFlowingParticipantCost(
    Integer maxMonthlyFlowingParticipantCost
  ) {
    this.maxMonthlyFlowingParticipantCost = maxMonthlyFlowingParticipantCost;
  }

  public Integer getMaxMonthlyFlowingModeratorCost() {
    return maxMonthlyFlowingModeratorCost;
  }

  public void setMaxMonthlyFlowingModeratorCost(
    Integer maxMonthlyFlowingModeratorCost
  ) {
    this.maxMonthlyFlowingModeratorCost = maxMonthlyFlowingModeratorCost;
  }

  public String getRoleInstruction() {
    return roleInstruction;
  }

  public void setRoleInstruction(String roleInstruction) {
    this.roleInstruction = roleInstruction;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result = prime * result + ((apiKey == null) ? 0 : apiKey.hashCode());
    result =
      prime *
      result +
      ((apiOrganization == null) ? 0 : apiOrganization.hashCode());
    result = prime * result + (trialEnabled ? 1231 : 1237);
    result =
      prime *
      result +
      ((maxDailyRoomCost == null) ? 0 : maxDailyRoomCost.hashCode());
    result =
      prime *
      result +
      ((maxMonthlyRoomCost == null) ? 0 : maxMonthlyRoomCost.hashCode());
    result =
      prime *
      result +
      (
        (maxMonthlyFlowingRoomCost == null)
          ? 0
          : maxMonthlyFlowingRoomCost.hashCode()
      );
    result =
      prime *
      result +
      (
        (maxAccumulatedRoomCost == null) ? 0 : maxAccumulatedRoomCost.hashCode()
      );
    result =
      prime *
      result +
      (
        (maxDailyParticipantCost == null)
          ? 0
          : maxDailyParticipantCost.hashCode()
      );
    result =
      prime *
      result +
      (
        (maxMonthlyParticipantCost == null)
          ? 0
          : maxMonthlyParticipantCost.hashCode()
      );
    result =
      prime *
      result +
      (
        (maxMonthlyFlowingParticipantCost == null)
          ? 0
          : maxMonthlyFlowingParticipantCost.hashCode()
      );
    result =
      prime *
      result +
      (
        (maxAccumulatedParticipantCost == null)
          ? 0
          : maxAccumulatedParticipantCost.hashCode()
      );
    result =
      prime *
      result +
      ((maxDailyModeratorCost == null) ? 0 : maxDailyModeratorCost.hashCode());
    result =
      prime *
      result +
      (
        (maxMonthlyModeratorCost == null)
          ? 0
          : maxMonthlyModeratorCost.hashCode()
      );
    result =
      prime *
      result +
      (
        (maxMonthlyFlowingModeratorCost == null)
          ? 0
          : maxMonthlyFlowingModeratorCost.hashCode()
      );
    result =
      prime *
      result +
      (
        (maxAccumulatedModeratorCost == null)
          ? 0
          : maxAccumulatedModeratorCost.hashCode()
      );
    result =
      prime *
      result +
      (int) (dailyRoomCostCounter ^ (dailyRoomCostCounter >>> 32));
    result =
      prime *
      result +
      (int) (monthlyRoomCostCounter ^ (monthlyRoomCostCounter >>> 32));
    result =
      prime *
      result +
      (int) (accumulatedRoomCostCounter ^ (accumulatedRoomCostCounter >>> 32));
    result =
      prime *
      result +
      (int) (
        dailyParticipantCostCounter ^ (dailyParticipantCostCounter >>> 32)
      );
    result =
      prime *
      result +
      (int) (
        monthlyParticipantCostCounter ^ (monthlyParticipantCostCounter >>> 32)
      );
    result =
      prime *
      result +
      (int) (
        accumulatedParticipantCostCounter ^
        (accumulatedParticipantCostCounter >>> 32)
      );
    result =
      prime *
      result +
      (int) (dailyModeratorCostCounter ^ (dailyModeratorCostCounter >>> 32));
    result =
      prime *
      result +
      (int) (
        monthlyModeratorCostCounter ^ (monthlyModeratorCostCounter >>> 32)
      );
    result =
      prime *
      result +
      (int) (
        accumulatedModeratorCostCounter ^
        (accumulatedModeratorCostCounter >>> 32)
      );
    result =
      prime * result + ((lastUpdate == null) ? 0 : lastUpdate.hashCode());
    result = prime * result + rightsBitset;
    result = prime * result + (int) (paymentCounter ^ (paymentCounter >>> 32));
    result =
      prime * result + ((presetContext == null) ? 0 : presetContext.hashCode());
    result =
      prime * result + ((presetLength == null) ? 0 : presetLength.hashCode());
    result =
      prime *
      result +
      ((roleInstruction == null) ? 0 : roleInstruction.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GPTRoomSetting other = (GPTRoomSetting) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (apiKey == null) {
      if (other.apiKey != null) return false;
    } else if (!apiKey.equals(other.apiKey)) return false;
    if (apiOrganization == null) {
      if (other.apiOrganization != null) return false;
    } else if (!apiOrganization.equals(other.apiOrganization)) return false;
    if (trialEnabled != other.trialEnabled) return false;
    if (maxDailyRoomCost == null) {
      if (other.maxDailyRoomCost != null) return false;
    } else if (!maxDailyRoomCost.equals(other.maxDailyRoomCost)) return false;
    if (maxMonthlyRoomCost == null) {
      if (other.maxMonthlyRoomCost != null) return false;
    } else if (
      !maxMonthlyRoomCost.equals(other.maxMonthlyRoomCost)
    ) return false;
    if (maxMonthlyFlowingRoomCost == null) {
      if (other.maxMonthlyFlowingRoomCost != null) return false;
    } else if (
      !maxMonthlyFlowingRoomCost.equals(other.maxMonthlyFlowingRoomCost)
    ) return false;
    if (maxAccumulatedRoomCost == null) {
      if (other.maxAccumulatedRoomCost != null) return false;
    } else if (
      !maxAccumulatedRoomCost.equals(other.maxAccumulatedRoomCost)
    ) return false;
    if (maxDailyParticipantCost == null) {
      if (other.maxDailyParticipantCost != null) return false;
    } else if (
      !maxDailyParticipantCost.equals(other.maxDailyParticipantCost)
    ) return false;
    if (maxMonthlyParticipantCost == null) {
      if (other.maxMonthlyParticipantCost != null) return false;
    } else if (
      !maxMonthlyParticipantCost.equals(other.maxMonthlyParticipantCost)
    ) return false;
    if (maxMonthlyFlowingParticipantCost == null) {
      if (other.maxMonthlyFlowingParticipantCost != null) return false;
    } else if (
      !maxMonthlyFlowingParticipantCost.equals(
        other.maxMonthlyFlowingParticipantCost
      )
    ) return false;
    if (maxAccumulatedParticipantCost == null) {
      if (other.maxAccumulatedParticipantCost != null) return false;
    } else if (
      !maxAccumulatedParticipantCost.equals(other.maxAccumulatedParticipantCost)
    ) return false;
    if (maxDailyModeratorCost == null) {
      if (other.maxDailyModeratorCost != null) return false;
    } else if (
      !maxDailyModeratorCost.equals(other.maxDailyModeratorCost)
    ) return false;
    if (maxMonthlyModeratorCost == null) {
      if (other.maxMonthlyModeratorCost != null) return false;
    } else if (
      !maxMonthlyModeratorCost.equals(other.maxMonthlyModeratorCost)
    ) return false;
    if (maxMonthlyFlowingModeratorCost == null) {
      if (other.maxMonthlyFlowingModeratorCost != null) return false;
    } else if (
      !maxMonthlyFlowingModeratorCost.equals(
        other.maxMonthlyFlowingModeratorCost
      )
    ) return false;
    if (maxAccumulatedModeratorCost == null) {
      if (other.maxAccumulatedModeratorCost != null) return false;
    } else if (
      !maxAccumulatedModeratorCost.equals(other.maxAccumulatedModeratorCost)
    ) return false;
    if (dailyRoomCostCounter != other.dailyRoomCostCounter) return false;
    if (monthlyRoomCostCounter != other.monthlyRoomCostCounter) return false;
    if (
      accumulatedRoomCostCounter != other.accumulatedRoomCostCounter
    ) return false;
    if (
      dailyParticipantCostCounter != other.dailyParticipantCostCounter
    ) return false;
    if (
      monthlyParticipantCostCounter != other.monthlyParticipantCostCounter
    ) return false;
    if (
      accumulatedParticipantCostCounter !=
      other.accumulatedParticipantCostCounter
    ) return false;
    if (
      dailyModeratorCostCounter != other.dailyModeratorCostCounter
    ) return false;
    if (
      monthlyModeratorCostCounter != other.monthlyModeratorCostCounter
    ) return false;
    if (
      accumulatedModeratorCostCounter != other.accumulatedModeratorCostCounter
    ) return false;
    if (lastUpdate == null) {
      if (other.lastUpdate != null) return false;
    } else if (!lastUpdate.equals(other.lastUpdate)) return false;
    if (rightsBitset != other.rightsBitset) return false;
    if (paymentCounter != other.paymentCounter) return false;
    if (presetContext == null) {
      if (other.presetContext != null) return false;
    } else if (!presetContext.equals(other.presetContext)) return false;
    if (presetLength == null) {
      if (other.presetLength != null) return false;
    } else if (!presetLength.equals(other.presetLength)) return false;
    if (roleInstruction == null) {
      if (other.roleInstruction != null) return false;
    } else if (!roleInstruction.equals(other.roleInstruction)) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "GPTRoomSetting [id=" +
      id +
      ", roomId=" +
      roomId +
      ", apiKey=" +
      apiKey +
      ", globalAccumulatedQuota=" +
      globalAccumulatedQuota +
      ", apiOrganization=" +
      apiOrganization +
      ", trialEnabled=" +
      trialEnabled +
      ", maxDailyRoomCost=" +
      maxDailyRoomCost +
      ", maxMonthlyRoomCost=" +
      maxMonthlyRoomCost +
      ", maxMonthlyFlowingRoomCost=" +
      maxMonthlyFlowingRoomCost +
      ", maxAccumulatedRoomCost=" +
      maxAccumulatedRoomCost +
      ", maxDailyParticipantCost=" +
      maxDailyParticipantCost +
      ", maxMonthlyParticipantCost=" +
      maxMonthlyParticipantCost +
      ", maxMonthlyFlowingParticipantCost=" +
      maxMonthlyFlowingParticipantCost +
      ", maxAccumulatedParticipantCost=" +
      maxAccumulatedParticipantCost +
      ", maxDailyModeratorCost=" +
      maxDailyModeratorCost +
      ", maxMonthlyModeratorCost=" +
      maxMonthlyModeratorCost +
      ", maxMonthlyFlowingModeratorCost=" +
      maxMonthlyFlowingModeratorCost +
      ", maxAccumulatedModeratorCost=" +
      maxAccumulatedModeratorCost +
      ", dailyRoomCostCounter=" +
      dailyRoomCostCounter +
      ", monthlyRoomCostCounter=" +
      monthlyRoomCostCounter +
      ", accumulatedRoomCostCounter=" +
      accumulatedRoomCostCounter +
      ", dailyParticipantCostCounter=" +
      dailyParticipantCostCounter +
      ", monthlyParticipantCostCounter=" +
      monthlyParticipantCostCounter +
      ", accumulatedParticipantCostCounter=" +
      accumulatedParticipantCostCounter +
      ", dailyModeratorCostCounter=" +
      dailyModeratorCostCounter +
      ", monthlyModeratorCostCounter=" +
      monthlyModeratorCostCounter +
      ", accumulatedModeratorCostCounter=" +
      accumulatedModeratorCostCounter +
      ", lastUpdate=" +
      lastUpdate +
      ", rightsBitset=" +
      rightsBitset +
      ", paymentCounter=" +
      paymentCounter +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", presetContext=" +
      presetContext +
      ", presetLength=" +
      presetLength +
      ", roleInstruction=" +
      roleInstruction +
      ", usageTimes=" +
      usageTimes +
      ", trialCode=" +
      trialCode +
      "]"
    );
  }
}
