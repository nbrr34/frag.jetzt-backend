package de.thm.arsnova.frag.jetzt.backend.service;

import de.thm.arsnova.frag.jetzt.backend.model.BonusToken;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.BonusTokenRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.util.RandomUtils;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Service
public class BonusTokenService {

    @Value("${app.bonustoken.length}")
    private int tokenLength;

    private static final Logger logger = LoggerFactory.getLogger(BonusTokenService.class);

    private final BonusTokenRepository repository;
    private final CommentService commentService;
    private final AuthorizationHelper authorizationHelper;
    private final RoomRepository roomRepository;

    @Autowired
    public BonusTokenService(
            BonusTokenRepository repository, CommentService commentService, AuthorizationHelper authorizationHelper, RoomRepository roomRepository) {
        this.repository = repository;
        this.commentService = commentService;
        this.authorizationHelper = authorizationHelper;
        this.roomRepository = roomRepository;
    }

    public Flux<BonusToken> get(List<UUID> ids) {
        return repository.findAllById(ids);
    }

    public Mono<BonusToken> create(BonusToken b) {
        b.setToken(RandomUtils.generateNumberToken(tokenLength));
        return Mono.zip(authorizationHelper.getCurrentUser(), roomRepository.findById(b.getRoomId()))
                .filter(tuple2 -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple2.getT1(), tuple2.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(roomAccesses -> repository.save(b));
    }

    public Mono<Void> delete(BonusToken b) {
        return Mono.zip(authorizationHelper.getCurrentUser(), roomRepository.findById(b.getRoomId()))
                .filter(tuple2 -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple2.getT1(), tuple2.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(roomAccesses -> repository.delete(b));
    }

    public Mono<Void> deleteByRoomId(UUID roomId) {
        return Mono.zip(authorizationHelper.getCurrentUser(), roomRepository.findById(roomId))
                .filter(tuple2 -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple2.getT1(), tuple2.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(roomAccesses -> repository.deleteAll(getByRoomId(roomId)));
    }

    public Mono<Void> deleteByCommentIdAndAccountId(UUID commentId, UUID accountId) {
        return commentService.get(commentId)
                .switchIfEmpty(Mono.error(new NotFoundException("Comment not found")))
                .flatMap(comment ->
                        Mono.zip(roomRepository.findById(comment.getRoomId()), authorizationHelper.getCurrentUser())
                                .filter(tuple2 -> authorizationHelper.checkModeratorOrCreatorOfRoom(tuple2.getT2(), tuple2.getT1()))
                                .switchIfEmpty(Mono.error(ForbiddenException::new))
                                .flatMap(authenticatedUser -> repository.deleteByCommentIdAndAccountId(commentId, accountId))
                );
    }

    public Flux<BonusToken> getByRoomId(UUID roomId) {
        return repository.findByRoomId(roomId);
    }

    public Flux<BonusToken> getByUserId(UUID userId) {
        return repository.findByAccountId(userId);
    }

}
