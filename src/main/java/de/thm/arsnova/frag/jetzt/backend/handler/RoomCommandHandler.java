package de.thm.arsnova.frag.jetzt.backend.handler;

import de.thm.arsnova.frag.jetzt.backend.RoomEventSource;
import de.thm.arsnova.frag.jetzt.backend.model.Account;
import de.thm.arsnova.frag.jetzt.backend.model.ModeratorAccessCode;
import de.thm.arsnova.frag.jetzt.backend.model.Room;
import de.thm.arsnova.frag.jetzt.backend.model.RoomAccess;
import de.thm.arsnova.frag.jetzt.backend.model.command.*;
import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.CommentService;
import de.thm.arsnova.frag.jetzt.backend.service.RoomAccessService;
import de.thm.arsnova.frag.jetzt.backend.service.RoomService;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.AccountRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomAccessRepository;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.BadRequestException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.ForbiddenException;
import de.thm.arsnova.frag.jetzt.backend.web.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

@Component
public class RoomCommandHandler {

    private static final Logger logger = LoggerFactory.getLogger(RoomCommandHandler.class);

    private final RoomService service;
    private final RoomEventSource eventer;
    private final AuthorizationHelper authorizationHelper;
    private final AccountRepository accountRepository;
    private final CommentService commentService;
    private final RoomAccessService roomAccessService;
    private final RoomAccessRepository roomAccessRepository;

    @Autowired
    public RoomCommandHandler(
            RoomService service,
            RoomEventSource eventer,
            AuthorizationHelper authorizationHelper,
            AccountRepository accountRepository,
            CommentService commentService,
            RoomAccessService roomAccessService,
            RoomAccessRepository roomAccessRepository
    ) {
        this.service = service;
        this.eventer = eventer;
        this.authorizationHelper = authorizationHelper;
        this.accountRepository = accountRepository;
        this.commentService = commentService;
        this.roomAccessService = roomAccessService;
        this.roomAccessRepository = roomAccessRepository;
    }

    public Mono<Room> handle(PatchRoom command) {
        logger.trace("got new command: " + command.toString());

        PatchRoomPayload payload = command.getPayload();
        return service.get(payload.getId())
                .switchIfEmpty(Mono.error(new NotFoundException("Room not found")))
                .flatMap(room -> service.patch(room, payload.getChanges()))
                .doOnSuccess(current -> this.eventer.roomChanged(payload.getId(), payload.getChanges()));
    }

    public Mono<Room> handle(UpdateRoom command) {
        logger.trace("got new command: " + command.toString());

        UpdateRoomPayload payload = command.getPayload();
        return service.get(payload.getRoom().getId())
                .flatMap(current -> {
                    Map<String, Object> changes = service.calculateChanges(current, payload.getRoom());
                    return Mono.zip(service.patch(current, changes), Mono.just(changes));
                })
                .doOnSuccess(tuple2 -> this.eventer.roomChanged(tuple2.getT1().getId(), tuple2.getT2()))
                .map(Tuple2::getT1);
    }

    public Flux<UUID> handle(ImportRoom command) {
        logger.trace("got new command: " + command.toString());

        final int count = command.getPayload().getRequiredGuestCount();
        if (count < 1) {
            throw new BadRequestException("Count is less than 1");
        }
        return Mono.zip(
                        authorizationHelper.getCurrentUser(),
                        service.get(command.getPayload().getId())
                )
                .switchIfEmpty(Mono.error(new BadRequestException("Not logged in or no room present.")))
                .filter(tuple -> authorizationHelper.checkCreatorOfRoom(tuple.getT1(), tuple.getT2()))
                .switchIfEmpty(Mono.error(ForbiddenException::new))
                .flatMap(tuple -> commentService.getByRoomId(tuple.getT2().getId())
                        .filter(c -> c.getDeletedAt() == null).count())
                .filter(commentCount -> commentCount == 0)
                .switchIfEmpty(Mono.error(new BadRequestException("Room has already questions.")))
                .flux()
                .flatMap(c -> {
                    ArrayList<Account> list = new ArrayList<>(count);
                    for (int i = 0; i < count; i++) {
                        list.add(new Account());
                    }
                    return Flux.fromIterable(list);
                })
                .flatMap(accountRepository::save)
                .map(Account::getId);
    }

    public Mono<ModeratorAccessCode> handle(RecreateModeratorCode command) {
        logger.trace("got new command: " + command.toString());

        RecreateModeratorCodePayload payload = command.getPayload();
        return roomAccessService.getByRoomId(payload.getId())
                .flatMap(r -> Mono.zip(Mono.just(r), accountRepository.findById(r.getAccountId())))
                .filter(tuple2 -> tuple2.getT1().getRole() == RoomAccess.Role.EXECUTIVE_MODERATOR &&
                        (tuple2.getT2().getEmail() == null || tuple2.getT2().getEmail().length() == 0))
                .map(Tuple2::getT1)
                .flatMap(roomAccess -> {
                    roomAccess.setRole(RoomAccess.Role.PARTICIPANT);
                    return roomAccessRepository.save(roomAccess);
                })
                .collectList()
                .flatMap(list -> service.recreateModeratorJoinCode(payload.getId()));
    }

}
