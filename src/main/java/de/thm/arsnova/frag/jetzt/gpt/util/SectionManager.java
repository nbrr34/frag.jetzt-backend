package de.thm.arsnova.frag.jetzt.gpt.util;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscription;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class SectionManager<T> {

  private final String ERROR_STRING =
    "Cannot start a new request if the old one is not completed!";
  private final HashMap<T, Subscription> registeredSubscriptions = new HashMap<>();

  public <K> Mono<K> restrictedSection(
    Mono<K> source,
    T identifier,
    boolean endPrevious
  ) {
    AtomicReference<Subscription> ref = new AtomicReference<>();
    return source
      .doOnSubscribe(sub -> {
        onSubscribe(identifier, sub, endPrevious);
        ref.set(sub);
      })
      .doFinally(signal -> leaveSection(identifier, ref.get()));
  }

  public <K> Flux<K> restrictedSection(
    Flux<K> source,
    T identifier,
    boolean endPrevious
  ) {
    AtomicReference<Subscription> ref = new AtomicReference<>();
    return source
      .doOnSubscribe(sub -> {
        onSubscribe(identifier, sub, endPrevious);
        ref.set(sub);
      })
      .doFinally(signal -> leaveSection(identifier, ref.get()));
  }

  public boolean interruptSection(T identifier) {
    Subscription s;
    synchronized (registeredSubscriptions) {
      s = registeredSubscriptions.remove(identifier);
    }
    if (s == null) {
      return false;
    }
    s.cancel();
    return true;
  }

  private void onSubscribe(
    T identifier,
    Subscription sub,
    boolean endPrevious
  ) {
    Subscription old;
    synchronized (registeredSubscriptions) {
      old = registeredSubscriptions.get(identifier);
      if (old == null || endPrevious) {
        registeredSubscriptions.put(identifier, sub);
      }
    }
    if (old == null) {
      return;
    }
    if (!endPrevious) {
      throw new ResponseStatusException(HttpStatus.LOCKED, ERROR_STRING);
    }
    old.cancel();
  }

  private void leaveSection(T identifier, Subscription sub) {
    synchronized (registeredSubscriptions) {
      registeredSubscriptions.remove(identifier, sub);
    }
  }
}
