package de.thm.arsnova.frag.jetzt.backend.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;

public class QuillUtils {

    public static boolean isValid(String s, boolean allowNullable) {
        if (allowNullable && (s == null || s.equals(""))) {
            return true;
        }
        try {
            return new ObjectMapper().readTree(s).getNodeType() == JsonNodeType.ARRAY;
        } catch (JsonProcessingException e) {
            return false;
        }
    }
}
