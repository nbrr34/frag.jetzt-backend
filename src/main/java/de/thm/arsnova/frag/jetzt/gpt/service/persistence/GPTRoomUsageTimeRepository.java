package de.thm.arsnova.frag.jetzt.gpt.service.persistence;

import java.util.UUID;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import de.thm.arsnova.frag.jetzt.gpt.model.persistence.GPTRoomUsageTime;
import reactor.core.publisher.Flux;

@Repository
public interface GPTRoomUsageTimeRepository extends ReactiveCrudRepository<GPTRoomUsageTime, UUID> {

    Flux<GPTRoomUsageTime> findAllBySettingId(UUID settingId);
    
}
