package de.thm.arsnova.frag.jetzt.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Table
public class MotdMessage implements Persistable<UUID> {

    public enum Language {
        de,
        en,
        fr,
    }

    @Id
    private UUID id;
    private UUID motdId;
    private Language language;
    private String message;
    private Timestamp createdAt = Timestamp.from(Instant.now());
    private Timestamp updatedAt;

    public MotdMessage() {

    }

    public MotdMessage(UUID motdId, Language language, String message) {
        this.motdId = motdId;
        this.language = language;
        this.message = message;
    }

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getMotdId() {
        return motdId;
    }

    public void setMotdId(UUID motdId) {
        this.motdId = motdId;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return "MotdMessage{" +
                "id=" + id +
                ", motdId=" + motdId +
                ", language=" + language +
                ", message='" + message + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MotdMessage that = (MotdMessage) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(motdId, that.motdId) &&
                language == that.language &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, motdId, language, message);
    }
}
