package de.thm.arsnova.frag.jetzt.backend.model.livepoll;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class LivepollSession implements Persistable<UUID> {

  @Id
  private UUID id;

  private UUID roomId;
  private boolean active;
  private boolean paused;
  private String template;
  private String title;
  private boolean resultVisible;
  private boolean viewsVisible;
  private int answerCount;
  // Meta information
  private Timestamp createdAt = Timestamp.from(Instant.now());
  private Timestamp updatedAt;

  // Transient
  @Transient
  private List<LivepollCustomTemplateEntry> customEntries;

  public LivepollSession() {}

  public LivepollSession(
    UUID roomId,
    String template,
    String title,
    boolean resultVisible,
    boolean viewsVisible,
    int answerCount
  ) {
    this.roomId = roomId;
    this.active = true;
    this.template = template;
    this.title = title;
    this.resultVisible = resultVisible;
    this.viewsVisible = viewsVisible;
    this.answerCount = answerCount;
  }

  @Override
  public boolean isNew() {
    return id == null;
  }

  @Override
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getRoomId() {
    return roomId;
  }

  public void setRoomId(UUID roomId) {
    this.roomId = roomId;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public String getTemplate() {
    return template;
  }

  public void setTemplate(String template) {
    this.template = template;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean isResultVisible() {
    return resultVisible;
  }

  public void setResultVisible(boolean resultVisible) {
    this.resultVisible = resultVisible;
  }

  public boolean isViewsVisible() {
    return viewsVisible;
  }

  public void setViewsVisible(boolean viewsVisible) {
    this.viewsVisible = viewsVisible;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public List<LivepollCustomTemplateEntry> getCustomEntries() {
    return customEntries;
  }

  public void setCustomEntries(
    List<LivepollCustomTemplateEntry> customEntries
  ) {
    this.customEntries = customEntries;
  }

  public boolean isPaused() {
    return paused;
  }

  public void setPaused(boolean paused) {
    this.paused = paused;
  }

  public int getAnswerCount() {
    return answerCount;
  }

  public void setAnswerCount(int answerCount) {
    this.answerCount = answerCount;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
    result = prime * result + (active ? 1231 : 1237);
    result = prime * result + (paused ? 1231 : 1237);
    result = prime * result + ((template == null) ? 0 : template.hashCode());
    result = prime * result + ((title == null) ? 0 : title.hashCode());
    result = prime * result + (resultVisible ? 1231 : 1237);
    result = prime * result + (viewsVisible ? 1231 : 1237);
    result = prime * result + answerCount;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    LivepollSession other = (LivepollSession) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (roomId == null) {
      if (other.roomId != null) return false;
    } else if (!roomId.equals(other.roomId)) return false;
    if (active != other.active) return false;
    if (paused != other.paused) return false;
    if (template == null) {
      if (other.template != null) return false;
    } else if (!template.equals(other.template)) return false;
    if (title == null) {
      if (other.title != null) return false;
    } else if (!title.equals(other.title)) return false;
    if (resultVisible != other.resultVisible) return false;
    if (viewsVisible != other.viewsVisible) return false;
    if (answerCount != other.answerCount) return false;
    return true;
  }

  @Override
  public String toString() {
    return (
      "LivepollSession [id=" +
      id +
      ", roomId=" +
      roomId +
      ", active=" +
      active +
      ", paused=" +
      paused +
      ", template=" +
      template +
      ", title=" +
      title +
      ", resultVisible=" +
      resultVisible +
      ", viewsVisible=" +
      viewsVisible +
      ", answerCount=" +
      answerCount +
      ", createdAt=" +
      createdAt +
      ", updatedAt=" +
      updatedAt +
      ", customEntries=" +
      customEntries +
      "]"
    );
  }
}
