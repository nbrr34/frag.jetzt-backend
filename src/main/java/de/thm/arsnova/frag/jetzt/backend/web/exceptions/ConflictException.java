package de.thm.arsnova.frag.jetzt.backend.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Conflict means status code 409.
 */
public class ConflictException extends ResponseStatusException {

    public ConflictException() {
        super(HttpStatus.CONFLICT);
    }

    public ConflictException(final String message) {
        super(HttpStatus.CONFLICT, message);
    }

    public ConflictException(final String message, final Throwable e) {
        super(HttpStatus.CONFLICT, message, e);
    }
}