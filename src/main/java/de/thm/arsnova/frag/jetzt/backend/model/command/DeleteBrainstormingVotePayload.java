package de.thm.arsnova.frag.jetzt.backend.model.command;

import de.thm.arsnova.frag.jetzt.backend.model.WebSocketPayload;

import java.util.Objects;
import java.util.UUID;

public class DeleteBrainstormingVotePayload implements WebSocketPayload {

    private UUID wordId;

    public DeleteBrainstormingVotePayload() {

    }

    public DeleteBrainstormingVotePayload(UUID wordId) {
        this.wordId = wordId;
    }

    public UUID getWordId() {
        return wordId;
    }

    public void setWordId(UUID wordId) {
        this.wordId = wordId;
    }

    @Override
    public String toString() {
        return "DeleteBrainstormingVotePayload{" +
                "wordId=" + wordId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeleteBrainstormingVotePayload that = (DeleteBrainstormingVotePayload) o;
        return Objects.equals(wordId, that.wordId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wordId);
    }
}
