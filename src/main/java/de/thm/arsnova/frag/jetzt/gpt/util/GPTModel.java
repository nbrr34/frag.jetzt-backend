package de.thm.arsnova.frag.jetzt.gpt.util;

import de.thm.arsnova.frag.jetzt.gpt.model.GPTQuotaUnit;
import de.thm.arsnova.frag.jetzt.gpt.util.encoding.GPTDefaultEncodings;
import de.thm.arsnova.frag.jetzt.gpt.util.encoding.GPTEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GPTModel {

  private static final Map<String, GPTModel> allowedModels;

  static {
    HashMap<String, GPTModel> map = new HashMap<>();
    map.put(
      "gpt-4-32k",
      new GPTModel(
        new GPTQuotaUnit(6, 5),
        new GPTQuotaUnit(12, 5),
        32768,
        8,
        GPTDefaultEncodings.getCl100kBaseEncoder()
      )
    );
    map.put(
      "gpt-4",
      new GPTModel(
        new GPTQuotaUnit(3, 5),
        new GPTQuotaUnit(6, 5),
        8192,
        8,
        GPTDefaultEncodings.getCl100kBaseEncoder()
      )
    );
    map.put(
      "gpt-3.5-turbo-16k",
      new GPTModel(
        new GPTQuotaUnit(3, 6),
        new GPTQuotaUnit(4, 6),
        16384,
        8,
        GPTDefaultEncodings.getCl100kBaseEncoder()
      )
    );
    map.put(
      "gpt-3.5-turbo",
      new GPTModel(
        new GPTQuotaUnit(15, 7),
        new GPTQuotaUnit(2, 6),
        4096,
        8,
        GPTDefaultEncodings.getCl100kBaseEncoder()
      )
    );
    map.put(
      "text-davinci-003",
      new GPTModel(
        new GPTQuotaUnit(2, 5),
        new GPTQuotaUnit(2, 5),
        4097,
        5,
        GPTDefaultEncodings.getP50kBaseEncoder()
      )
    );
    map.put(
      "text-davinci-002",
      new GPTModel(
        new GPTQuotaUnit(2, 5),
        new GPTQuotaUnit(2, 5),
        4097,
        5,
        GPTDefaultEncodings.getP50kBaseEncoder()
      )
    );
    map.put(
      "code-davinci-002",
      new GPTModel(
        new GPTQuotaUnit(2, 5),
        new GPTQuotaUnit(2, 5),
        8001,
        5,
        GPTDefaultEncodings.getP50kBaseEncoder()
      )
    );
    allowedModels = Collections.unmodifiableMap(map);
  }

  public static Map<String, GPTModel> getAllowedModels() {
    return allowedModels;
  }

  private final GPTQuotaUnit costPerPromptToken;
  private final GPTQuotaUnit costPerCompletionToken;
  private final int maxTokens;
  /**
   * When using the streaming endpoint, it is not possible to decide how many tokens are generated when an error occurs.
   * Also, when a request is interrupted (for both streaming and non-streaming endpoints) due to a network connection error
   * or premature closure, the text is not sent in full, so no additional tokens can be calculated.<br/>
   * <br/>
   * If a non-streaming request is closed or an error occurs, the full maxTokens are charged as the cost of the request.<br/>
   * <br/>
   * When a streaming request is closed or an error occurs, the currently received completion text and
   * an additional offset (not exceeding maxTokens) are charged as the request cost.
   */
  private final int interruptTokens;
  private final GPTEncoder encoder;

  public GPTModel(
    GPTQuotaUnit costPerPromptToken,
    GPTQuotaUnit costPerCompletionToken,
    int maxTokens,
    int interruptTokens,
    GPTEncoder encoder
  ) {
    this.costPerPromptToken = costPerPromptToken;
    this.costPerCompletionToken = costPerCompletionToken;
    this.maxTokens = maxTokens;
    this.interruptTokens = interruptTokens;
    this.encoder = encoder;
  }

  public GPTQuotaUnit getCostPerPromptToken() {
    return costPerPromptToken;
  }

  public GPTQuotaUnit getCostPerCompletionToken() {
    return costPerCompletionToken;
  }

  public int getMaxTokens() {
    return maxTokens;
  }

  public int getInterruptTokens() {
    return interruptTokens;
  }

  public GPTEncoder getEncoder() {
    return encoder;
  }
}
