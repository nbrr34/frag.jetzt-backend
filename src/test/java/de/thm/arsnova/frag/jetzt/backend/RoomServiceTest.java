package de.thm.arsnova.frag.jetzt.backend;

import de.thm.arsnova.frag.jetzt.backend.service.AuthorizationHelper;
import de.thm.arsnova.frag.jetzt.backend.service.BrainstormingSessionService;
import de.thm.arsnova.frag.jetzt.backend.service.LivepollSessionService;
import de.thm.arsnova.frag.jetzt.backend.service.RoomService;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.RoomRepository;
import de.thm.arsnova.frag.jetzt.backend.service.persistence.TagRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;

@WebFluxTest()
@RunWith(MockitoJUnitRunner.class)
public class RoomServiceTest {

    @Mock
    RoomRepository repository;

    @Mock
    AuthorizationHelper authorizationHelper;

    @Mock
    TagRepository tagRepository;

    @Mock
    BrainstormingSessionService sessionService;

    @Mock
    LivepollSessionService livepollService;

    private RoomService service;

    @Before
    public void setup() {
        service = new RoomService(authorizationHelper, repository, tagRepository, sessionService, livepollService);
    }

    @Test
    public void testShouldGetRoomById() {
        /*UUID id = UUID.fromString("032f8beb-e089-46b0-811c-533c8d3e72fd");
        UUID ownerId = UUID.fromString("2d9782fd-2cc8-4bec-b85e-dfd3828668b6");
        String roomName = "testRoom";
        Room r = new Room();
        r.setId(id);
        r.setOwnerId(ownerId);
        r.setName(roomName);

        when(repository.findById(id)).thenReturn(Mono.just(r));

        Mono<Room> comment = service.get(id);

        comment.subscribe(result -> {
            assertEquals(id, result.getId());
            assertEquals(ownerId, result.getOwnerId());
            assertEquals(roomName, result.getName());
        });*/
    }
}
